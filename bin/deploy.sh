#!/bin/bash
# ./bin/deploy.sh gene first_deploy/gene

GIT_COMMIT=$(git rev-parse HEAD)
BRANCH_NAME=$(git rev-parse --abbrev-ref HEAD)
DEPLOYMENT_GROUP=$1
JOB_NAME=$2

## Configuration
APPLICATION_NAME='FirstDeploy'
AWS_BUCKET='l4-demo-deployments'

# echo "trying touch"
# echo $DEPLOYMENT_GROUP
# touch "/usr/l4c2/test/test.txt"

RESULTS=$(aws deploy create-deployment --application-name ''$APPLICATION_NAME'' --s3-location bucket=''$AWS_BUCKET'',key=''$JOB_NAME''/''$BRANCH_NAME''/''$GIT_COMMIT''.zip,bundleType=zip --deployment-group-name ''$DEPLOYMENT_GROUP'' --description 'Deploy to '$DEPLOYMENT_GROUP' Group' --region=us-east-1)
echo $RESULTS


if [ -z "$RESULTS" ]; then
    exit 1;
else
    DEPLOYMENT_ID=$(egrep -o 'd\-[A-Z0-9]+' <<< $RESULTS)
    t=0
    while [ $t -lt 5 ]; do
        result=$(aws deploy get-deployment --deployment-id $DEPLOYMENT_ID --region=us-east-1);

        if grep -q "\"status\": \"Failed\"" <<< $result; then
            exit 1;
        fi

        if grep -q "\"status\": \"Succeeded\"" <<< $result; then
            break
        else
            echo $result
            t=$(($t+1))
            echo $t" attempt(s)"
            sleep 2
        fi
    done
fi