var utils = require('./utils')
var cl = (m)=>{
  let pos = 1 + __filename.lastIndexOf('/');
  let ln = utils.callStack()[1].getLineNumber()
  if (typeof m === "object"){ m = JSON.stringify(m); }
  console.log(__filename.substr(pos) + " " + ln + ": " + m);
}

var bodyParser = require('body-parser');
var bcrypt = require('bcrypt');
var crypto = require('crypto');
var base64url = require('base64url');


// const baseUrl = "http://" + httpHost + ":3374";

var checkPassword = (password, passhash)=>{

}

var doLogin = (req, res)=>{
// { username: 'test', password: 'pass', siteid: 0 }
// { "_id" : ObjectId("5d6c33ab7f0f5c0f51b6d7ca"), "username" : "user2", "passhash" : "$2b$12$q55YIi2XNndqrLP5Jg/buOrguQdWutHHbR9GJF/OVIoOQ.eGoYrWm", "userid" : "TZQUESjAM9wCIx-jn6mBsT1F-w-vKRlw" }
  utils.addCorsHeaders(res);
  let ret = {result: "error"};
  cl(ret)
  g.log.findOne({username: req.body.username}).then(u=>{
    if ((u != null) && bcrypt.compareSync(req.body.password, u.passhash)){
//       cl(u);`
      let payload = {
//         username: req.body.username,
        id: u.userid,
        privs: u.privs,
      }
//       cl(payload);
      let token = utils.makeJwtToken(payload, 86400);
//       cl(token)
//       utils.getJwtToken(token).then(r=>{
//         cl(r);
//       });
      ret.result = "ok";
      ret.token = token;
      ret.wsHost = utils.wsUrl;
    }
    res.status(200).send(ret);
  })
}

// { "_id" : ObjectId("5d6fa21940b86e570a9ae8bf"), "username" : "test", "passhash" : "$2b$12$4fzF717BNZNBi1Xm3zEf7OdUiSJRcUsgZjxOfD19Zl852o62C8hxy", "userid" : "DDmcGqE1wqKkp5r9OpP3soQKTMVxn2HP" }


var createUser = (req, res)=>{
//   user = req.body.user
//   cl(user);
//   crypto.randomBytes(24, (e, b)=>{
//     cl(b.toString('hex'));
//   });
  user = req.body.user
  crypto.randomBytes(24, (e, b)=>{
    let userid = base64url(b)
    cl(userid);
    cl(b.toString('hex'));
    user.userid = userid;
    createUpdateUser(res, user, true)
//     g.log.updateOne({userid: user.userid}, {$set: user}, {upsert: upsert});
  });


//   utils.addCorsHeaders(res);
//   let ret = {result: "ok"};
//     res.status(200).send(ret);
}

var retrieveUsers = (req, res)=>{
  utils.addCorsHeaders(res);
  g.log.find().toArray((e, r)=>{
    let ret = {result: "ok", u: r}
    res.status(200).send(ret);
  })
//     let ret = {result: "ok"};
//   res.status(200).send(ret);
}

var retrieveSites = (req, res)=>{
  cl("retrieve sites");
  utils.addCorsHeaders(res);
  g.sites.find().toArray((e, r)=>{
    retArr = [];
    let i = 0;
    r.forEach(row=>{
      let shortName = row.username.substring(0, 8);
//       cl(row.username.substring(0, 8));
//       cl(r["_id"]);
//       cl(r[0]);
      var name;
      if(row.name !== undefined) {
        name = row.name;
      }else{
        name = "Site " + i++;
      }
      var flags;
      if(row.flags !== undefined){
        flags=row.flags;
      }else{
        flags=0;
      }
      retArr.push({id: row["_id"], siteid: row.siteid, name: name, flags: flags, shortName: shortName});
    });
//     cl(retArr);
    let ret = {result: "ok", s: retArr}
    res.status(200).send(ret);
  })
}

var updateOneSite=(req, res)=>{// sets flags and name
  utils.addCorsHeaders(res);
  let site = req.body.site;
  cl(site);
  g.sites.updateOne({_id: utils.makeObjectID(site.id)}, {$set: {name: site.name, flags: site.flags}}, {upsert: false});
  res.status(200).send({result: "ok"});
}

bcrypt.hash('myPassword', 10, function(err, hash) {
  // Store hash in database
});

var updateOneUser=(user, upsert)=>{
  // cl(user, upsert);
  // cl(upsert);
  g.log.updateOne({userid: user.userid}, {$set: user}, {upsert: upsert});
}

var createUpdateUser = (res, user, upsert)=>{
/* if no password is sent, then don't change it. if there is, then create a new password hash
 if upsert is set, then create a new one if not found*/
  delete user._id;
  cl(user);
  utils.addCorsHeaders(res);
  if (user.password != ""){
      cl("bcrypt");
      bcrypt.hash(user.password, 10, (e, h)=>{
        user.passhash = h;
        delete user.password;
        cl(user);
        updateOneUser(user, upsert);
//         g.log.updateOne({userid: user.userid}, {$set: user}, {upsert: upsert});
      });
  } else{
    updateOneUser(user, upsert);
//     g.log.updateOne({userid: user.userid}, {$set: user}, {upsert: upsert});

  }
  res.status(200).send({result: "ok"});
}

var updateUser = (req, res)=>{
// authorize.js 65: {"_id":"5d6f991b3b67ac100e5923b5","username":"none","passhash":"$2b$12$SLRwBmez3NJHAcaoDXMaIO8ZxVxUL0xu/8YzdPQCH.RDww29xBrKW","userid":"4bMgiyBm3ZyON7iiL1R-OpnGk3Et9Wy4","privs":[{"s":0,"p":1},{"s":1,"p":1}]}

//   cl(req.body);
  let user = req.body.user
  createUpdateUser(res, user, false)
//   delete user._id;
//   cl(user);
//   utils.addCorsHeaders(res);
//   g.log.updateOne({userid: user.userid}, {$set: user}, {upsert: false});
//   let ret = {result: "ok"};
//   res.status(200).send(ret);
}

// {name: "ZoneStages",
// controls: [
// {name: "Zone Name",
// type: "text",
// pid: {z: 0, c: 255, i: 5022}
// }
// ]}


var getPages=(req, res)=>{
  g.pages.find().toArray((e, r)=>{
    let ret = {result: "ok", pages: r}
    res.status(200).send(ret);
  })
}

var putPage=(req, res)=>{
  cl("put page");
  let page = req.body;
  cl(page);
  g.pages.updateOne({name: page.name}, {$set: page}, {upsert: true});
  res.status(200).send({result: "ok"});
}

var getFuiPages=(req, res)=>{
  utils.addCorsHeaders(res);
  // cl("get fui")
  g.fuipages.find().toArray((e, r)=>{
    r.forEach(p=>{
      delete p._id;
    })
    let ret = {result: "ok", pages: r}
    // cl(ret)
    res.status(200).send(ret);
  })
}

var putFuiPage=(req, res)=>{
  utils.addCorsHeaders(res);
//   let body = req.body;
//   cl(body);
//   let page = body.page;
//   cl("put page");
// //   let page = req.body.page;
//   cl(body.key);
  g.fuipages.updateOne({type: req.body.key}, {$set: req.body.page}, {upsert: true});
  res.status(200).send({result: "ok"});
}

var deleteFuiPage=(req, res)=>{
  utils.addCorsHeaders(res);
//   cl(req.params.key);
  g.fuipages.deleteOne({type: req.params.key});
  res.status(200).send({result: "ok"});
}

var getTemplates=(req, res)=>{
  // cl("get" + x)
  utils.addCorsHeaders(res);
  g.templates.find().toArray((e, r)=>{
    r.forEach(p=>{
      delete p._id;// so we don't have to send it
    })
    let ret = {result: "ok", templates: r}
    res.status(200).send(ret);
  })
}

var putTemplate=(req, res)=>{
/*this assumes that there's a "key" field in the posted
json, that matches the name of the template*/
  utils.addCorsHeaders(res);
  cl("put Template");
  g.templates.updateOne({name: req.body.key}, {$set: req.body.template}, {upsert: true});
  res.status(200).send({result: "ok"});
}

var deleteTemplate=(req, res)=>{
  utils.addCorsHeaders(res);
  g.templates.deleteOne({name: req.params.key});
  res.status(200).send({result: "ok"});
}

var deleteUser = (req, res)=>{
//   user = req.body.user
//   cl(user)
  cl(req.params);
  utils.addCorsHeaders(res);
  g.log.deleteOne({userid: req.params.userid});
  res.status(200).send({result: "ok"});
}

// @app.route('/siteconfig', methods=['GET', 'OPTIONS'])
// @login_required
// def get_site_config(payload):# payload from token
//     configs = mongo.db.siteconfigs
//     userid = payload['id']
//     # lla(userid)
//     c = configs.find_one({'userid': userid})
//     lla(c)
//     # lla(c['configs'])
//     resp = jsonify({'result': 'ok', 'configs': c['configs']})
//     add_cors_headers(resp)
//     return resp
//

// { "userid" : "DDmcGqE1wqKkp5r9OpP3soQKTMVxn2HP",
//   "configs" : { "siteViewColumns" : [ "Zone" ], "zoneViewColumns" : [ [ "HumSP" ] ] } }


var getSiteConfig = (req, res)=>{
  g.configs.findOne({userid: req.payload['id']}).then((r)=>{
//     cl(r.configs);
    if (r == undefined){
      r = {configs: {"siteViewColumns": ["Zone"], "zoneViewColumns": [[ "HumSP" ]]}};
    }
    res.status(200).send({result: 'ok', configs: r.configs});
  }, e=>{
    res.status(200).send({result: "ok", configs: {}});
  })
}

var checkAuth = (req, res)=>{
  res.status(200).send({result: "ok"});
}

var saveSiteConfig = (req, res)=>{
// { "_id" : ObjectId("5d6fb902ce207b090bc259b6"),
//   "userid" : "DDmcGqE1wqKkp5r9OpP3soQKTMVxn2HP",
//   "configs" : { "siteViewColumns" : [ "Zone", "Name", "BaroPres", "InTemp", "InHum" ],
//     "zoneViewColumns" : [ [ "HumSP", "InHum", "DeHumSP", "HeatSP" ] ]
//
//   } }

// {
//   "siteViewColumns":["Zone","Name","BaroPres","InTemp","InHum"],
//   "zoneViewColumns":[["HumSP","InHum","DeHumSP","HeatSP"]]
//
// }

// authorize.js 186: {"siteViewColumns":["Zone","Name","InTemp"],"zoneViewColumns":["HumSP"]}
// authorize.js 187: {"id":"5lmYH3sq8Jfx2XG2nuoZzfPu3xC_DL3e","privs":[{"s":1,"p":1}],"iat":1571055763,"exp":1571142163}

//   cl(req.body);
//   cl(req.payload);
  g.configs.findOne({userid: req.payload['id']}).then((r)=>{

//     let configs = Object.assign({}, r.configs);
//     cl(r.configs);
//
//     configs.siteViewColumns = req.body.siteViewColumns;
    g.configs.updateOne({userid: req.payload['id']}, {$set: {configs: req.body}}, {upsert: true});
    res.status(200).send({result: 'ok'});//, configs: r.configs
  }, e=>{
    res.status(200).send({result: "ok", configs: {}});
  })
}

var addCors = (req, res)=>{
  utils.addCorsHeaders(res);
  res.status(200).send("ok");
}

var checkToken = (req, res, next)=>{
  utils.addCorsHeaders(res);
  let auth = req.header("Authorization") ;
//   cl(auth);
  if (auth !== undefined && auth.indexOf("Bearer ") === 0){
    utils.getJwtToken(auth.substr(7)).then(r=>{
      req.payload = r;
      next();
    }, e=>{
      res.status(401).send({result: "unauth"});
    });
  } else {
    return res.status(401).send({result: "unauth"});
  }
}

var setRoutes = (r)=>{
  g.app.options('/*', addCors);
  g.app.post('/open/logins', doLogin);
  g.app.get('/users', retrieveUsers);
  g.app.post('/users', createUser);
  g.app.delete('/users/:userid', deleteUser);
  g.app.put('/users', updateUser);
  g.app.get('/sites', retrieveSites);
  g.app.put('/sites', updateOneSite);
  g.app.get('/logshow', (req, res)=>{
    res.sendFile(__dirname + '/showLog.html');
  });
  g.app.use('/auth/*', checkToken);
  g.app.get('/auth/siteconfig', getSiteConfig);
  g.app.post('/auth/siteconfig', saveSiteConfig);
  g.app.get('/auth/check', checkAuth);

  g.app.get('/auth/pages', getPages);
  g.app.put('/auth/pages', putPage);
  g.app.get('/fuipages', getFuiPages);
  g.app.put('/fuipages', putFuiPage);
  g.app.delete('/fuipages/:key', deleteFuiPage);
  g.app.get('/templates', getTemplates);
  g.app.put('/templates', putTemplate);
  g.app.delete('/templates/:key', deleteTemplate);
}


var g = {


  init: ()=>{
//     cl("auth init");
    utils.openMongoDB('authorize', utils.authorizeUrl).then(db=>{
      g.db = db;
      g.log = utils.openMongoColl(db, 'logins');
      g.sites = utils.openMongoColl(db, 'sites');
      g.configs = utils.openMongoColl(db, 'siteconfigs');
      g.pages = utils.openMongoColl(db, 'pages');
      g.fuipages = utils.openMongoColl(db, 'fui_pages');
      g.templates = utils.openMongoColl(db, 'templates');
      cl("mongo open for authorize: logins, siteconfigs, and pages on " + utils.authorizeUrl);
//       g.log.find().toArray((e, r)=>{
//         cl(r)
//       })
    });
    utils.openExpressServer(3374, g.onListen).then(r=>{
      r.use(bodyParser.json());
      g.app = r;
      setRoutes(r);
    });
  },

//   },

}

g.init();

module.exports = g
