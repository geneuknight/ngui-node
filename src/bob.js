var utils = require('./utils');
var l2c=require('./l2c2l');

var cl = (m)=>{
  let pos = 1 + __filename.lastIndexOf('/');
  let ln = utils.callStack()[1].getLineNumber()
  if (typeof m === "object"){ m = JSON.stringify(m); }
  console.log(__filename.substr(pos) + " " + ln + ": " + m);
}

var bodyParser = require('body-parser');
var bcrypt = require('bcrypt');
var crypto = require('crypto');
var base64url = require('base64url');

var addCors = (req, res)=>{
  utils.addCorsHeaders(res);
  res.status(200).send("ok");
}

var streamIds={};

var readMqtt=(req, res)=>{
  /* assume that the companyName is unique, also, the user email
   * activating a user is not easy!
   * if there's a company name, then create the company account, and get the accountID
   * then, create the user and get the userID
   */
  utils.addCorsHeaders(res);
//   var url_parts = url.parse(req.url, true);  
//   cl(req.query.clientId);
  let clientId = req.query.clientId;
  let streamId = req.query.streamId;
  if(!streamId)streamId="1";
  streamIds[streamId] = 1;
  if(!clientId) return;
//   cl(clientId);
//   cl(req.params.activation);
//   req.params.activation="vTh4XR5h87I25J0UD2sIog";
//   let query = {activation: req.params.activation}
//   cl(query);
//   g.pendingUsers.findOne(query).then(r=>{
//     cl(r);
//   });
//   cl(clientId);
//   cl(g.bobReceived[clientId]);
//   cl(g.bobReceived);
//   cl(g.bobReceived);
  if(!g.bobReceived[clientId])g.bobReceived[clientId]={};
  let tx = g.bobReceived[clientId][streamId];
  if(!tx) tx = "";
  if(tx != "") tx += "]";
  let ret = {result: "ok", text: tx};
  g.bobReceived[clientId][streamId] = "" ;
  res.status(200).send(ret);
}

var writeMqtt=(req, res)=>{
  utils.addCorsHeaders(res);
//   cl(req.body);
//   cl(req.body);
  if (req.body.mqttMsg != ""){
    g.msgToMqtt(req.body);
  }
  let ret = {result: "ok"};
  res.status(200).send(ret);
}

var sendBobFile=(req, res)=>{
  l2c.makeLNames();
//   cl(l2c.lNames);
  res.sendFile('/usr/l4c2/no00/src/bobIndex.html');
//   let ret = {result: "ok"};
//   res.status(200).send(ret);
}

var initSite=(req, res)=>{
  utils.addCorsHeaders(res);
//   cl(req.params);
  let obj={command: "siteinit01", siteid: req.params.siteid}
  g.chanBob.publish('Current', '', Buffer.from(JSON.stringify(obj)));
  
  let ret = {result: "ok"};
  res.status(200).send(ret);
}

var dataPacks={
  "irr":{"type": "eq", "p": [143, 144, 199, 202, 
    [203, 1, [0, 1, 2, 3, 4, 5]], 
    [204, 1, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]],
    205, 206, 207, 208, 209, 210, 211, 212, 215, 216, 217, 218, 219]},
  "chn":{"type": "eq", "p": [142, 143, 144, 147, 445, 446, 447, 448, 449, 450, 451, 452, 453, 454, 455]},
  "vent":{"type": "eq", "p": [143, 144, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178]},
  "pump":{"type": "eq", "p": [143, 144, 284, 285, 286, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296, 297, 298, 300, 301, 302, 303, 304, 305, 306, 307, 308, 309, 310, 311, 312, 313, 314, 317, 318, 319, 323, 324]},
  "fValve":{"type": "eq", "p": [143, 144, 327, 328, 329, 330, 331, 332, 333, 334, 335, 336]},
  "curt":{"type": "eq", "p": [143, 144, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198]},
  "sensors":{"type": "eq", "p": [2037, 2038, 2039, 2040, 2041, 2042, 2043, 2044, 2045, 2046, 2047, 2048, 2049, 2051, 2052, 2053, 2054, 2055, 2056, 2057, 2058]},
  "setup":{"type": "eq", "p": [3, 4, 5, 8, 10, 11, 12, 13, 14, 18, 19, 20, 21, 22, 23, 25, 26, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 46]},
  "co2":{"type": "eq", "p": [143, 144, 220, 221, 222, 223, 224, 225, 226, 227]},
  "ecphConfig":{"type": "eq", "p": [477, 478, 479, 480, 481, 482, 483, 484, 485, 486, 487, 488, 489, 490, 491, 492, 493, 494, 495, 496, 497, 498, 499, 500, 501, 502, 503, 504, 505, 506]},
  "ecphStatus":{"type": "eq", "p": [2062, 2063, 2064]},
  
  
  
  
  
  
  
  
  
  
  
  
}

var getPack=(req, res)=>{
//   cl(req.params);
  dp=dataPacks[req.params.packId];
//   cl(l2c.lNames);
  retDP = [];
  dp.p.forEach(id=>{
    if(typeof id == "number"){
      let name = l2c.lNames[id];
      retDP.push({
        name: l2c.lNames[id],
        id: id,
        ix: [0],// array of one index
      });
    }else{
//       let name = l2c.lNames[id[0]];
      retDP.push({
        name: l2c.lNames[id[0]],
        id: id[0],
        ix: id[2],// array of indexes
      });
    }
//     cl(name);
  });
//   cl(arr);
  
  let ret = {result: "ok", d: {type: dp.type, p: retDP}};
  cl(ret);
  res.status(200).send(ret);
}

var setRoutes = ()=>{
  g.app.options('/*', addCors);
  g.app.get('/bob/mqtt', readMqtt);
  g.app.get('/bobIndex.html', sendBobFile);
  g.app.post('/bob/mqtt', writeMqtt);
  g.app.post('/maint/sites/init/:siteid', initSite);
  g.app.get('/bob/packs/:packId/:ch/:ix', getPack);
  //   g.app.post('/usa/o/register', register);
//   g.app.use('/s/*', checkToken);
  //   g.app.get('/s/siteconfig', getSiteConfig);
  
}

var doLogin = (req, res)=>{
  utils.addCorsHeaders(res);
  let ret = {result: "error"};
  res.status(200).send(ret);
}

// var getSiteConfig = (req, res)=>{
//   utils.addCorsHeaders(res);
//   let ret = {result: "error"};
//   res.status(200).send(ret);
// }

var msgBob = (msg)=>{// received from mqtt
  //   cl("got manager message");
//   cmds = {siteinit00: siteinit00, wsinit00: wsinit00, getmqttqueue00: getmqttqueue00}
  let o = JSON.parse(msg.content);
//   cl(o.msg)
  let o2 = JSON.parse(o.msg);
  if(!o2.p.length) return;
//   cl(o2.p);
//   cl(o2.p);
//   cl(msg.content.toString());
//   g.bobReceived += msg.content.toString();
  let recv = JSON.parse(msg.content.toString());
//   cl(recv.clientId);
//   cl(recv.msg);
  if(!g.bobReceived[recv.clientId])g.bobReceived[recv.clientId] = {};
  let keys = Object.keys(streamIds);
  keys.forEach(k=>{
    let tx = g.bobReceived[recv.clientId][k];
    if(tx && (tx.length > 10000)) g.bobReceived[recv.clientId][k]="";
    if(!g.bobReceived[recv.clientId][k]) {
      g.bobReceived[recv.clientId][k] = "[";
      //     cl(g.bobReceived[recv.clientId]);
    }else{
      g.bobReceived[recv.clientId][k] += ", ";
      //     cl(g.bobReceived[recv.clientId]);
    }
    g.bobReceived[recv.clientId][k] += recv.msg;
  })
  
//   cl(g.bobReceived);
//   cmds[o.command](o);
//   cl("bob got " + o.msg);
//   cl(o);
  //   cl(msg.content.toString());
}

var g = {
  
  msgToMqtt: (mqttMsg)=>{
//     mqttMsg={msg: "this is the other one"};
    g.chanBob.publish('mqttBob', '', Buffer.from(JSON.stringify(mqttMsg)));
//     cl("sent bob");
  },
  
  chanBob: null,
  bobReceived: {},
  init: ()=>{
//     setInterval(g.msgToMqtt, 10000);
    cl("init bob");
    
    utils.openExpressServer(3379, g.onListen).then(r=>{// 3101 is the front end
      r.use(bodyParser.json());
      g.app = r;
      setRoutes(r);
      cl("bob routes are set on 3379");
    });

    utils.openAMQP(utils.amqpUrl).then(conn=>{
      utils.openAMQPChannel00(conn, "bob", 'fanout', msgBob).then((r)=>{
        cl("bob connected to AMQP " + "bob" + " on " + utils.amqpUrl);
        g.chanBob = r.chan;
      });
    })


//     utils.openMongoDB("usa", "127.0.0.1").then(db=>{
//       g.db = db;
//       g.pendingUsers = utils.openMongoColl(db, 'pendingUsers');
//       //       g.userIDs = utils.openMongoColl(db, 'userIDs');
//       //       g.userAccounts = utils.openMongoColl(db, 'userAccounts');
//       //       g.userSites = utils.openMongoColl(db, 'userSites');
//       g.users = utils.openMongoColl(db, 'users');
//       //       g.sites = utils.openMongoColl(db, 'sites');
//       //       g.accounts = utils.openMongoColl(db, 'accounts');
//       cl("mongo open for usa: userIDs, userAccounts, and userSites on " + "127.0.0.1");
//     })
  },
}

g.init();

module.exports = g
