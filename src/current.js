var utils = require('./utils')
var fs = require('fs');
var cl = (m)=>{
  let pos = 1 + __filename.lastIndexOf('/');
  let ln = utils.callStack()[1].getLineNumber()
  if (typeof m === "object"){ m = JSON.stringify(m); }
  console.log(__filename.substr(pos) + " " + ln + ": " + m);
}


var dbl = (m)=>{
  let pos = 1 + __filename.lastIndexOf('/');
  let ln = utils.callStack()[1].getLineNumber()
  if (typeof m === "object"){ m = JSON.stringify(m); }
  utils.dbl(__filename.substr(pos) + " " + ln + ": " + m);
}

// var msgControlStream = (msg)=>{
//   cl("control stream");
//   g.chanControlStream.ack(msg);
//   let obj = JSON.parse(msg.content);
// //   cl(obj);
//   if (obj.command === 'getcursite00')getCurrent(obj);
// }

var logLine=(line)=>{
  return;
  let d = new Date();
  let sd = d.getFullYear() + az(d.getMonth() + 1, 2) + az(d.getDate(), 2) + az(d.getHours(), 2) +
    az(d.getMinutes(), 2) + az(d.getSeconds(), 2);
  fs.open("/home/gknight/dirs/dev/no00/src/server.log", "a", (e, f)=>{
    if (e) throw e;
    fs.write(f, sd + ": " + line + '\n', (e)=>{
      fs.close(f, (e)=>{});
    });
  });}

var az = (n, cnt)=>{
  let str = ("0000" + n)
  return str.substring(str.length-cnt, str.length);
}

var logParam=(o, p)=>{

  let str = o.site + "-" + p.z + "-" + p.c + "-" + p.i + "-" + p.d;
  logLine(str);
}

var writeToCurrent = (o)=>{// updates Current image for site
/* current doesn't use either the time or the user fields*/
//   cl("write to current");
//   if(o.h != undefined){cl(o)}
//   cl(o);
//   dbl("write to current");
  o.params.forEach(p=>{
    logParam(o, p);
//     if(p.z != o.zone) cl("new zone error!");
    if(p.h !== 1){// don't overwrite with history!
      let insO = {
        c: p.c,
        i: p.i,
        s: o.site,
        z: p.z,
        d: p.d,
      };
      let queryO = {
        c: p.c,
        i: p.i,
        s: o.site,
        z: p.z,
      };
      if (p.i === 4642){
        cl("writing to current: " + o.command);
//         cl(insO);
      }
      try{
//         dbl(insO);
//         dbl(queryO);
//         cl(insO);
        g.currentCollection.updateOne(queryO, {$set: insO}, {upsert: true});
      }catch{
        dbl("current error");
      }
    } else {
//       cl("hist")
    }
  });
}

var data00 = (o)=>{
//   cl("received data00");
//   cl(o);
  writeToCurrent(o)
}

var data01 = (o)=>{
// {"command":"data01","site":0,"user":0,"zone":0,"token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IkREbWNHcUUxd3FLa3A1cjlPcFAzc29RS1RNVnhuMkhQIiwicHJpdnMiOnsicyI6MH0sImlhdCI6MTU3MDA5ODgzOCwiZXhwIjoxNTcwMTg1MjM4fQ.RiJO4ju0Losi-dPOM7B2cnMF5y4Dp3lanbZm_8jxZBk",
// "params":[{"c":255,"i":4643,"t":1570185105,"d":22.4}]}
//   cl("received data01");
//   cl(o);
  writeToCurrent(o);

//   cl("received data01");
}

var msgPodSiteStream = (msg)=>{// saves current updates received from controllers
/* this is whats received from the CurrentHistory exchange*/
//   cl("in current: msgPodSiteStream");
//   cl("pod stream");
let cmds = {data00: data00, data01: data01, data02: data01, 
  demo_get_status: data01,
  demo_get_config: data01,
};
  let obj = JSON.parse(decodeURI(msg.content));
  cmds[obj.command](obj);
//   if (obj.command === 'data00'){
// //     writeToCurrent(obj)
// //     cl(obj);
//   }
  g.chanPodSiteStream.ack(msg);
}

var getCurrent = (obj)=>{// responds to request for current image for a site
/*and returns it to the CurrentReturn exchange, to the 'returnQueue' queue*/
//   cl(obj.site)
  let cur1 = g.currentCollection.find({s:obj.site});// returns a cursor!
//   cl("getting site: " + obj.site);
  let packs = [];
//   cl("get current");
//   utils.start("getCurrent");
  cur1.on('data', r=>{
    packs.push(r);
  });
//   cl(packs.length);
  cur1.on('end', r=>{
    let cpack = {
      command: "gotcursite00",
      socketId: obj.socketId,
      key: obj.key,
      cursite: packs,
    };
//     cl(utils.stop("getCurrent"));
    cl("sending packs: " + packs.length);
    g.chanCurrent.publish("ws", obj.returnQueue, Buffer.from(JSON.stringify(cpack)));
  });
}

var loadWholeSite=(siteid)=>{
  return new Promise((res, rej)=>{
    let cur1 = g.currentCollection.find({s:siteid});// returns a cursor!
    let packs=[];
    cur1.on('data', r=>{packs.push(r)});
    cur1.on('end', r=>{
      res(packs);
//       let keys = Object.keys(packs);
//       cl(keys);
//       cl(packs[0]);
      //     cl(packs);
    });
  });
}

var siteinit01=(o)=>{
  /*
   * the idea here is to completely copy zones from a known good site to the given one, so that all the zones*
   * from the new site will load safely
   * this is temporary, to get Landru up and running
   * site: R9Pw8WOdJbnuL8Fv
   * for now, we're just going to copy the whole site, id by id. Later, we should be able to copy individual zones.
   * 
   */
  let siteToCopy = "R9Pw8WOdJbnuL8Fv";
  let os = loadWholeSite(siteToCopy);
  let newSite = o.siteid;
  let ns = loadWholeSite(newSite);
  let once = true;
  cl(o);
  Promise.all([os, ns]).then(r=>{
    let [osv, nsv] = r;
    cl("return");
    nsv2 = {};
    let nk = Object.keys(nsv);
    nk.forEach(k=>{
      let n = nsv[k];
      if(!nsv2[n.z]) nsv2[n.z]=[];
      if(!nsv2[n.z][n.c]) nsv2[n.z][n.c] = [];
      nsv2[n.z][n.c][n.i] = n;
//       cl([n.z, n.c, n.i, nsv2[n.z][n.c][n.i]]);
    });
    let ok = Object.keys(osv);
    ok.forEach(k=>{
      let o = osv[k];
      
      if(nsv2[o.z] && nsv2[o.z][o.c] && nsv2[o.z][o.c][o.i]){
//         cl(o);
      } else{
//{"_id":"5f58d229892916f15f4edbda","c":240,"i":33,"s":"R9Pw8WOdJbnuL8Fv","z":0,"d":"789"}
        // {"c":240,"i":1346,"s":"R9Pw8WOdJbnuL8Fv","z":0,"d":"0"}
        
        // mx ngdb db00c
        if(once){
          let insObj = {c: o.c, i: o.i, s: newSite, z: o.z, d: o.d}
//           cl(insObj);
          g.currentCollection.insertOne(insObj);
//           once = false;
        }
      }
    });
    cl([ok.length, nk.length]);
//     cl(nsv[0]);
    //     let osv = r[0];
//     let nsv = r[1];
//     cl(r[0]);
//     let keys=Object.keys(nsv);
//     keys.forEach(k=>{
//       if(k in osv){
//         cl(nsv[k]);
//       }
//     });
//     ns.forEach(e=>{cl(e);});
    cl("all loaded");
//     cl(ns);
  });

//   let cur1 = g.currentCollection.find({s:siteToCopy});// returns a cursor!
//   let packs=[];
//   cur1.on('data', r=>{packs.push(r)});
//   cur1.on('end', r=>{
//     let keys = Object.keys(packs);
//     cl(keys);
//     cl(packs[0]);
// //     cl(packs);
//   });
  
  cl(o);
  
}

var msgControlStream = (msg)=>{// commands sent to control
  let cmds = {getcursite00: getCurrent, siteinit01: siteinit01}
//   cl("msg 2");
  g.chanCurrent.ack(msg);
//   cl("current stream!!!");
  let obj = JSON.parse(msg.content);
  cmds[obj.command](obj);

//   cl(obj);
//   if (obj.command === 'getcursite00')getCurrent(obj);
}

var g = {

  currentCollection: null,// mongo collection
  chanPodSiteStream: null,// AMQP PodSiteStream
  chanControlStream: null,// AMQP ControlStream
  currentExchange: "Current",
  currentExchange: "Current",
  currentReturnExchange: "CurrentReturn",

  init: ()=>{
    utils.openMongoDB('ngdb', utils.currentUrl).then(db=>{
      g.currentCollection = utils.openMongoColl(db, 'db00c');
      cl("mongo open for db00c on " + utils.currentUrl);
    });
    utils.openAMQP(utils.amqpUrl).then(conn=>{
      utils.openChannelAMQP(conn).then(chan=>{
        g.chanCurrent = chan;
        let exch = 'Current';
        utils.exchAMQP(chan, exch, 'fanout');
        cl("current connected to AMQP at " + utils.amqpUrl);
        utils.anonQueueAMQP(chan, exch, msgControlStream).then(queue=>{
          cl("current connected to Current AMQP stream");
          g.queueChannelStream = queue;// not actually used
//           cl("cur queue: " + g.queueChannelStream);
        });
      });
      utils.openChannelAMQP(conn).then(chan=>{
        g.chanPodSiteStream = chan;
        let exch = 'CurrentHistory';
        utils.exchAMQP(chan, exch, 'fanout');
        utils.anonQueueAMQP(chan, exch, msgPodSiteStream).then(queue=>{
          g.queuePodSiteStream = queue;// not actually used
        });
        cl("current connected to CurrentHistory AMQP stream");
      });
    });

//     utils.openAMQP00().then(chan=>{
//       g.chanPodSiteStream = chan;
//       utils.exchAMQP(chan, exch, 'fanout');
//       utils.anonQueueAMQP(chan, exch, msgPodSiteStream).then(queue=>{
//         g.queuePodSiteStream = queue;// not actually used
//       });
//     });

//     utils.openAMQP().then(conn=>{
//       g.podQueue = "PodSiteStream";
//       utils.openAQMPConsumerChannel(conn, g.podQueue, msgPodSiteStream).then(chan=>{
//         g.chanPodSiteStream = chan;
//       });
//       utils.openAMQPChannel00(conn, g.currentExchange, 'fanout', msgControlStream).then((r)=>{
//         g.chanCurrent = r.chan;
//       });
//     })


  }
}

g.init();

module.exports = g;
