var utils = require('./utils')
var cl = console.log

var g = {
  currentCollection: null,// mongo collection
  chanPodSiteStream: null,// AMQP PodSiteStream
  chanControlStream: null,// AMQP ControlStream
//   mqttClient: null,
//   queue: 'ControlStream',
//   chan1: null, // test
}



var writeToCurrent = (o)=>{// updates Current image for site
  o.params.forEach(p=>{
    let insO = {
      c: p.c,
      i: p.i,
      s: o.site,
      z: o.zone,
      d: p.p,
    };
    cl(insO);
    g.currentCollection.insertOne(insO);
  });
}

var msgPodSiteStream = (msg)=>{// saves current updates received from controllers
  let obj = JSON.parse(decodeURI(msg.content));
  if (obj.command === 'data00'){
//     writeToCurrent(obj)
    cl(obj);
  }
  g.chanPodSiteStream.ack(msg);
}

var getCurrent = (obj)=>{// responds to request for current image for a site
//   cl("from control stream");
//   cl(obj);
  let cur1 = g.currentCollection.find({s:obj.site});// returns a cursor!
  let packs = [];
  cur1.on('data', r=>{
    packs.push(r);
  });
  cur1.on('end', r=>{
    let cpack = {
      command: "gotcursite00",
      socket: obj.socket,
      cursite: packs,
    };
    let strPack = JSON.stringify(cpack);
    cl("len: " + strPack.length);
//     g.chanControlStream.sendToQueue(g.controlQueue, Buffer.from(strPack));
  });
}

var msgControlStream = (msg)=>{
  g.chanControlStream.ack(msg);
  let obj = JSON.parse(msg.content);
//   cl(obj);
  if (obj.command === 'getcursite00')getCurrent(obj);
}

var init = ()=>{
//   initPub();
  utils.openMongoDB('ngdb').then(db=>{
    g.currentCollection = utils.openMongoColl(db, 'db00c');
  });
  utils.openAMQP().then(conn=>{
    g.podQueue = "PodSiteStream";
    utils.openAQMPConsumerChannel(conn, g.podQueue, msgPodSiteStream).then(chan=>{
      g.chanPodSiteStream = chan;
    });
    g.controlQueue = "ControlStream";
    utils.openAQMPConsumerChannel(conn, g.controlQueue, msgControlStream).then(chan=>{
      g.chanControlStream = chan;
    });
  })
}

init();