var bodyParser = require('body-parser');
// import tabs from './iPonicIDs';
var tabs = require('./iPonicIDs');

/* this is going to catch the data coming from the iponic / igrow*/

var utils = require('./utils')
var iPonicUtils = require('./iPonicUtils');
var cnst={
  NUM_EQUIPMENT: 8,
  NUM_SETPOINTS: 3,
  alarmHoldTime: 100,
}

var keyStore = {};
// var rb =
var cl = (m)=>{
  let pos = 1 + __filename.lastIndexOf('/');
  let ln = utils.callStack()[1].getLineNumber()
  if (typeof m === "object"){ m = JSON.stringify(m); }
  console.log(__filename.substr(pos) + " " + ln + ": " + m);
}

var getPackHead=(bPack)=>{
/* wCon, wInfo, wRtd, wScreen, wUserAction, rCon, gToken, rCheck, sTime*/
  let headLen = {wCon: 4, wInf: 5, wRtd: 4, wUse: 11, rCon: 4, gTok: 6, rChe: 6, sTim: 5};
  let headKey = String.fromCharCode.apply(null, bPack.slice(0, 4));
  let keyLen =  3 + headLen[headKey];
  return {len: keyLen, head: String.fromCharCode.apply(null, bPack.slice(0, keyLen))}
}

function toArrayBuffer(buf, start, size) {
    var ab = new ArrayBuffer(size);
    var view = new Uint8Array(ab);
    for (var i = 0; i < size; ++i) {
        view[i] = buf[start + i];
        cl(view[i]);
    }
    return ab;
}

class rawBytes {
// iponic.js 31: 0
// iponic.js 31: 7
// iponic.js 31: 65533
// iponic.js 31: 1

  constructor(addString, headLen){// addString is a buffer
/* the strings we receive look like this:
wRtd\0(serialno-6 bytes)(data)
the items come in an array, not packed one after another
*/
    this.bytes = addString;
//     this.view = new DataView(toArrayBuffer(addString));

    this.headLen = headLen + 7; // skip serial, too and closing 0 from string
    this.pnt = headLen + 7;
    this.getTypes = {
      sbyte: x=>this.bytes.readInt8(x),
      sshort: x=>this.bytes.readInt16B(x),
      sint: x=>this.bytes.readInt32BE(x),
      byte: x=>this.bytes.readUInt8(x),
      short: x=>this.bytes.readUInt16BE(x),
      int: x=>this.bytes.readUInt32BE(x),
      bool: x=>this.readBoolOfs(x),
      string: x=>this.readStringOfs(x),
      float: x=>this.bytes.readFloatBE(x),
    };

  };
  setOffset(ofs){
    this.pnt = this.headLen + ofs;
  }
  showOffset(strShow){
    cl(strShow + ": " + (this.pnt - this.headLen).toString());
  }
  skip(move){
    this.pnt += move;
  }
  adv(move){
    let pos = this.pnt;
    this.pnt += move ;
    return pos;
  }
  readSerial(){
    let outStr = "";
    for (let i = 0 ; i < 6 ; i++){
      let pnt = this.headLen - 6 + i;
      outStr += this.bytes.readInt8().toString(16);
    }
    return outStr;
  }
  readBoolOfs(ofs){
    return (this.bytes.readInt8(ofs) === 0) ? false : true;
  }

  readByte(){
    return this.bytes.readInt8(this.adv(1));
  }
  readShort(){
    return this.bytes.readInt16BE(this.adv(2));
  }
  readInt(){
    return this.bytes.readInt32BE(this.adv(4));
  }
  readUByte(){
    return this.bytes.readUInt8(this.adv(1));
  }
  readUShort(){
    return this.bytes.readUInt16BE(this.adv(2));
  }
  readUInt(){
    return this.bytes.readUInt32BE(this.adv(4));
  }
  readBool(){
    return (this.readByte() === 0) ? false : true;
  }
  readFloat (){
    return this.bytes.readFloatBE(this.adv(4));
  }
  readString (fieldSize){
    let pnt0 = this.pnt;
    while (this.readByte() !== 0) ;
    if(fieldSize !== undefined){
      this.pnt = pnt0 + fieldSize;
    }
    return String.fromCharCode.apply(null, this.bytes.slice(pnt0, this.pnt));
  }
  readData(length){
    let pnt0 = this.adv(length);
    return this.bytes.slice(pnt0, this.pnt);
  }
  readIPonicAlarm(ofs){
    var hilo, active, ticks;
    this.setOffset(ofs);
    hilo = this.readByte();
    active = this.readByte();
    this.skip(1)
    ticks = this.readInt();
    return active && (ticks >= cnst.alarmHoldTime) ? ((hilo) ? -1 : 1) : 0;

  }
  readValType(type, offset){
    return this.getTypes[type](ofs);

  };
  readACVal(zone, channel, type, name, offset){
  /* configuration values, for the iponic
  -1, -1: tabs.config
  -1, i: tabs.config_equipment
  0, -1: tabs.config_zone
the key value store is already setup in this.kv
  */
    var tab;
    if (zone >= 0){
      tab = tabs.config_zone;
    }else{
      if (channel >= 0){
        tab = tabs.config_equipment;
      }else{
        tab = tabs.config
      }
    }
    this.readAVal(tab, zone, channel, type, name, offset);
    // let pid = tab[name];
    // let val = this.getTypes[type](this.headLen + offset);
    // let key = zone.toString() + "-" + channel.toString() + "-" + pid.toString();
    // if (this.kv[key] !== val){
    //   cl("put value " + key + ": " + val.toString());
    // }
  }

  readAVal(tab, zone, channel, type, name, offset){
      let pid = tab[name] + tab.base;
      let z = (zone === -1) ? "x" : zone.toString();
      let c = (channel === -1) ? "x" : channel.toString();
      let key = z + "-" + c + "-" + pid.toString();
      let val = this.getTypes[type](offset + this.headLen);
      if (this.kv[key] !== val){
        this.kv[key] = val;
        cl("put value " + key + " " + name + ": " + val.toString());
      }
    }

  readARVal(zone, channel, type, name, offset){
    var tab = tabs.realtimedata
    this.readAVal(tab, zone, channel, type, name, offset);
    // let pid = tab[name] + tab.base;
    // let z = (zone === -1) ? "x" : zone.toString();
    // let c = (channel === -1) ? "x" : channel.toString();
    // let key = z + "-" + c + "-" + pid.toString();
    // // cl(key);
    // let val = this.getTypes[type](offset + this.headLen);
    // if (this.kv[key] !== val){
    //   this.kv[key] = val;
    //   cl("put value " + key + " " + name + ": " + val.toString());
    // }
  }

  readFloat2(pos){

// 119,82,116,100,73,80,50,
// 0,
//     7,227,1,4,17,124, 6 bytes of serial
// 66,139,246,200,
// 66,139,246,200,2,2,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,2,0,1,0,2,0,0,0,0,255,255,255,255,0,0
// 00 07 , e3, 01, 04, 11 7c
//
//
// 0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,73,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,100,0,0,0,10,0,0,0,0,0,0,0,119,0,0,0,11,0,0,0,21,0,0,0,6,0,0,0,0,0,0,0,51,0,0,0,0,2,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,100,0,0,
65,205,12,167,
65,205,12,167,194,140,0,0,194,140,0,0,0,0,0,0,0,0,0,0,255,255,255,255,255,255,255,255,60,19,116,188,62,24,252,80,0,0,0,0,0,0,0,0,194,140,0,0,194,140,0,0,0,0


// 0,7,227,1,
// 4,17,124,66,
// 139,246,200,66,
//     var buffer = new ArrayBuffer(4);
//     var intView = new Int32Array(buffer);
//     var floatView = new Float32Array(buffer);
    var view = new DataView(new ArrayBuffer(4));
    view.setFloat32(0, 100.1);
//     cl(view.getInt32(0));
//     cl(view.getFloat32(0));

//     floatView = 100.1;
//     intView = 1234567
//     cl(intView);
//     let fArr = [];
    let test = [65,205,12,167]
    for (let i = 0 ; i < 4 ; i++){
      let ch = this.bytes.charCodeAt(pos + 3 - i);
      ch = test[i]
      cl(ch);
      view.setInt8(i, ch);
//       buffer[i] = ch;
//       cl(pos + i);
//       fArr.push(this.bytes.charCodeAt(pos + i));
//       cl(this.bytes.charCodeAt(i));
    }
    cl(view.getFloat32());
//       cl(floatView);
//     cl(fArr);
//     cl(this.bytes.substring(0, 11));
    return this.headLen;
  }
  // this.bytes = "";
};

var procwUserActionIP2 =(rb)=>{
  cl(headLen);
}

var procrCheckIP2=(rb)=>{
  cl(rb.headLen);

}

var procwInfoIP2=(rb)=>{
  let inTemperature=rb.readFloat(7);
  cl(inTemperature);
}

var getPid=(zone, channel, name)=>{

}

var procwRtdIP2=(rb)=>{
  rb.readARVal(0, -1, "float", "zoneInTemperature", 4);
  rb.readARVal(0, -1, "byte", "zoneTemperatureStage", 9);
  rb.readARVal(0, -1, "byte", "zoneHumidityStage", 10);
  rb.readARVal(0, -1, "bool", "zoneHumidifyActive", 12);
  rb.readARVal(0, -1, "byte", "zoneSetpointIndex", 13);
  rb.readARVal(0, -1, "float", "zoneInHumidity", 212);
  rb.readARVal(-1, -1, "float", "outTemperature", 220);
  rb.readARVal(0, -1, "float", "zoneInLight", 244);
  rb.readARVal(-1, -1, "float", "outHumidity", 268);
  rb.readARVal(-1, -1, "bool", "outHumidifyOn", 273);
  rb.readARVal(-1, -1, "byte", "outHumidifyOffset", 274);
  rb.readARVal(-1, -1, "byte", "outDehumidifyOffset", 275);
  rb.readARVal(0, -1, "sint", "zoneInCO2", 280);
  rb.readARVal(0, -1, "int", "zoneErrors", 296);
  for (let i = 0 ; i < cnst.NUM_EQUIPMENT ; i++){
    rb.readARVal(-1, i, "int", "channelStatus", 304 + 4 * i);
  }
  rb.readARVal(0, -1, "bool", "zoneCO2Cycle", 506);
  rb.readARVal(0, -1, "byte", "zoneAlarmTemperatureHighLow", 565);
  rb.readARVal(0, -1, "byte", "zoneAlarmTemperatureActive", 566);
  rb.readARVal(0, -1, "int", "zoneAlarmTemperatureTicks", 568);
  rb.readARVal(0, -1, "byte", "zoneAlarmHumidityHighLow", 585);
  rb.readARVal(0, -1, "byte", "zoneAlarmHumidityActive", 586);
  rb.readARVal(0, -1, "int", "zoneAlarmHumidityTicks", 588);
  rb.readARVal(0, -1, "byte", "zoneAlarmCO2HighLow", 605);
  rb.readARVal(0, -1, "byte", "zoneAlarmCO2Active", 606);
  rb.readARVal(0, -1, "int", "zoneAlarmCO2Ticks", 608);
  rb.readARVal(0, -1, "byte", "zoneFanStatus", 676);
  rb.readARVal(0, -1, "byte", "zoneCO2DumpType", 625);
  rb.readARVal(0, -1, "bool", "zoneCO2DumpOnOff", 626);
  rb.readARVal(1, -1, "byte", "zoneTemperatureStage", 788);
  rb.readARVal(1, -1, "byte", "zoneHumidityStage", 790);
  rb.readARVal(1, -1, "bool", "zoneHumidifyActive", 792);
  rb.readARVal(1, -1, "byte", "zoneSetpointIndex", 816);
  rb.readARVal(1, -1, "float", "zoneInTemperature", 844);
  rb.readARVal(1, -1, "float", "zoneInHumidity", 852);
  rb.readARVal(1, -1, "sint", "zoneInCO2", 860);
  rb.readARVal(1, -1, "float", "zoneInLight", 868);
  rb.readARVal(1, -1, "bool", "zoneCO2Cycle", 910);
  rb.readARVal(1, -1, "byte", "zoneAlarmTemperatureHighLow", 969);
  rb.readARVal(1, -1, "byte", "zoneAlarmTemperatureActive", 970);
  rb.readARVal(1, -1, "int", "zoneAlarmTemperatureTicks", 972);
  rb.readARVal(1, -1, "byte", "zoneAlarmHumidityHighLow", 989);
  rb.readARVal(1, -1, "byte", "zoneAlarmHumidityActive", 990);
  rb.readARVal(1, -1, "int", "zoneAlarmHumidityTicks", 992);
  rb.readARVal(1, -1, "byte", "zoneAlarmCO2HighLow", 1009);
  rb.readARVal(1, -1, "byte", "zoneAlarmCO2Active", 1010);
  rb.readARVal(1, -1, "int", "zoneAlarmCO2Ticks", 1012);
  rb.readARVal(1, -1, "byte", "zoneFanStatus", 1080);
  rb.readARVal(1, -1, "byte", "zoneCO2DumpType", 1029);
  rb.readARVal(1, -1, "bool", "zoneCO2DumpOnOff", 1030);
  rb.readARVal(1, -1, "int", "zoneErrors", 1128);
  for (let i = 0 ; i < cnst.NUM_EQUIPMENT ; i++){
    let chType = rb.readARVal(-1, i, "byte", "channelType", 2288);
    switch(chType){
      case 0:// on/off
      case 4:// fan
      case 5:// ac
      case 6:// light
      case 7:// co2
      case 8:// irrigation
        rb.readARVal(-1, i, "int", "channelPosition", 2320 + 16 * i);// 236
        break;
      case 1:// vent
        rb.readARVal(-1, i, "float", "channelPosition", 2320 + 16 * i);// 240
        break;
      case 2:// curtain
        rb.readARVal(-1, i, "byte", "channelPosition", 2320 + 16 * i);// 245
        break;
      case 3:// alarm
        rb.readARVal(-1, i, "byte", "channelPosition", 2320 + 16 * i);// 249
        break;
      default:
        break;
    }
  }
  rb.readARVal(-1, -1, "sint", "controllerTime", 2560);
  for (let i = 0 ; i < cnst.NUM_EQUIPMENT; i++){
    rb.readARVal(-1, i, "byte", "channelRelay", 2564 + 1 * i);// 272
  }
  for (let i = 0 ; i < cnst.NUM_EQUIPMENT; i++){
    rb.readARVal(-1, i, "byte", "channelOneOverride", 2572 + 1 * i);// 278
  }
  for (let i = 0 ; i < cnst.NUM_EQUIPMENT / 2 ; i++){
    rb.readARVal(-1, 2 * i, "byte", "channelTwoOverride", 2580 + i);// 284
    rb.readARVal(-1, 2 * i, "byte", "channelTwoOverridePosition", 2584 + i);// 285
  }
  rb.readARVal(-1, -1, "int", "growWeek", 2588);
  rb.readARVal(-1, -1, "bool", "growthCurveEnable", 2592);
  rb.readARVal(-1, -1, "bool", "outHumidityOverrideEnable", 2593);
  rb.readARVal(0, -1, "int", "zoneLightTickNum", 2596);
  rb.readARVal(0, -1, "float", "zoneHeatSetpoint", 2600);
  rb.readARVal(0, -1, "float", "zoneCoolSetpoint", 2604);
  rb.readARVal(0, -1, "float", "zoneHumidifySetpoint", 2608);
  rb.readARVal(0, -1, "float", "zoneDehumidifySetpoint", 2612);
  rb.readARVal(0, -1, "float", "zoneCO2Setpoint", 2616);
  rb.readARVal(0, -1, "short", "zoneLightCountdown0", 2620);
  rb.readARVal(0, -1, "short", "zoneLightCountdown1", 2622);
  rb.readARVal(1, -1, "int", "zoneLightTickNum", 2624);
  rb.readARVal(1, -1, "float", "zoneHeatSetpoint", 2628);
  rb.readARVal(1, -1, "float", "zoneCoolSetpoint", 2632);
  rb.readARVal(1, -1, "float", "zoneHumidifySetpoint", 2636);
  rb.readARVal(1, -1, "float", "zoneDehumidifySetpoint", 2640);
  rb.readARVal(1, -1, "float", "zoneCO2Setpoint", 2644);
  rb.readARVal(1, -1, "short", "zoneLightCountdown0", 2648);
  rb.readARVal(1, -1, "short", "zoneLightCountdown1", 2650);

/*  let inTemperature=rb.readFloat(4);// skip the first 4
  let ret={z0:{}, z1:{}, eq:[]};
  rb.setOffset(4);
  rb.showOffset("before temp");
  ret.z0.inTemperature =rb.readFloat();
  rb.skip(1);
  ret.z0.temperatureStage =rb.readByte();
  ret.z0.humidityStage =rb.readByte();
  rb.skip(1);
  rb.showOffset("before humidifyActive");
  ret.z0.humidifyActive =rb.readByte();
  ret.z0.setpointIndex =rb.readByte();
  rb.setOffset(0x00D4);
  ret.z0.inHumidity =rb.readFloat();
  rb.skip(4);
  ret.outTemperature =rb.readFloat();
  rb.skip(20);
  ret.z0.inLight =rb.readFloat();
  rb.skip(20);
  ret.outHumidity =rb.readFloat();
  rb.skip(1);
  rb.showOffset("before outHumidifyOn");
  ret.outHumidifyOn =rb.readBool();
  ret.outHumidifyOffset =rb.readByte();
  ret.outDehumidifyOffset =rb.readByte();
  rb.skip(4);
  ret.z0.CO2 =rb.readInt();
  rb.skip(12);
  rb.showOffset("before errors");
  ret.z0.errors =rb.readInt();

  rb.skip(4);
  for(let i = 0 ; i < cnst.NUM_EQUIPMENT ; i++){
    ret.eq.push({status: rb.readInt()});
  }
  rb.showOffset("before status");
  rb.setOffset(0x01FA);
  ret.z0.CO2Cycle = rb.readBool();
  ret.z0.tempAlarm = rb.readIPonicAlarm(0x0235);
  ret.z0.humAlarm = rb.readIPonicAlarm(0x0249);
  ret.z0.co2Alarm = rb.readIPonicAlarm(0x025D);
  rb.setOffset(0x02A4);
  ret.z0.fanStatus = rb.readByte();
  rb.setOffset(0x0271);
  ret.z0.CO2DumpType = rb.readByte();
  ret.z0.CO2Dump = rb.readBool();

  rb.setOffset(0x0314)
  ret.z1.temperatureStage =rb.readByte();
  rb.skip(1);
  ret.z1.humidityStage =rb.readByte();
  rb.skip(1);
  ret.z1.humidifyActive =rb.readByte();
  rb.showOffset("before setpointIndex");
  rb.setOffset(0x0330);
  ret.z1.setpointIndex =rb.readByte();
  rb.setOffset(0x034C);
  ret.z1.inTemperature = rb.readFloat();
  rb.skip(4);

  ret.z1.inHumidity =rb.readFloat();
  rb.skip(4);
  ret.z1.CO2 =rb.readInt();
  rb.skip(4);
  ret.z1.inLight =rb.readFloat();
  rb.skip(8);
  rb.setOffset(0x038E);
  ret.z1.CO2Cycle = rb.readBool();
  ret.z1.tempAlarm = rb.readIPonicAlarm(0x03C9);
  ret.z1.humAlarm = rb.readIPonicAlarm(0x03DD);
  ret.z1.co2Alarm = rb.readIPonicAlarm(0x03F1);
  rb.setOffset(0x0438);
  ret.z1.fanStatus = rb.readByte();
  rb.setOffset(0x0405);
  ret.z1.CO2DumpType = rb.readByte();
  ret.z1.CO2Dump = rb.readBool();
  rb.setOffset(0x0468);
  ret.z1.errors =rb.readInt();

  rb.setOffset(0x08F0);
  for (let i = 0 ; i < cnst.NUM_EQUIPMENT ; i++){
    let type = rb.readByte();
    rb.skip(3);
    switch(type){
      case 0:// OnOff
      case 4:// Fan
      case 5:// AC
      case 6:// Light
      case 7:// CO2
      case 8:// Irrigation
        ret.eq[i].position = rb.readInt();
        rb.skip(12);
        break;
      case 1:// Vent
        ret.eq[i].position = rb.readFloat();
        rb.skip(12);
        break;
      case 2:// Curtain
        rb.skip(1);
        ret.eq[i].position = rb.readByte();
        rb.skip(14);
        break;
      case 3:// Alarm
        ret.eq[i].position = (rb.readByte() === 1) ? 1 : 0;
        rb.skip(15);
        break;
      default:
        ret.eq[i].position = -1;
        rb.skip(16);
        break;
    }
  }
  rb.showOffset("before position");
  rb.setOffset(0x0A00);
  ret.controllerTime = rb.readInt();
  for (let i = 0 ; i < cnst.NUM_EQUIPMENT ; i++){
    ret.eq[i].relay = rb.readByte();
  }
  for (let i = 0 ; i < cnst.NUM_EQUIPMENT ; i++){
    ret.eq[i].oneOverride = rb.readByte();
  }
  for (let i = 0 ; i < (cnst.NUM_EQUIPMENT / 2) ; i++){
    let tom = rb.readByte();
    let top = rb.readByte();
    for (let j = 0 ; j < 2 ; j++){
      ret.eq[i * 2 + j].twoOverrideMode = tom;
      ret.eq[i * 2 + j].twoOverridePosition = top;
    }
  }
  rb.showOffset("growWeek");
  ret.growWeek = rb.readInt();
  ret.growthCurveEnable = rb.readBool();
  ret.outHumidifyValid = rb.readBool();
  rb.skip(2);
  ret.z0.lightTickNum = rb.readInt();
  ret.z0.heatSetpoint = rb.readFloat();
  ret.z0.coolSetpoint = rb.readFloat();
  ret.z0.humidifySetpoint = rb.readFloat();
  ret.z0.dehumidifySetpoint = rb.readFloat();
  ret.z0.CO2Setpoint = rb.readFloat();
  rb.showOffset("CO2Setpoint");

  ret.z0.lightCountdown = [rb.readShort(), rb.readShort()];
  ret.z1.lightTickNum = rb.readInt();
  ret.z1.heatSetpoint = rb.readFloat();
  ret.z1.coolSetpoint = rb.readFloat();
  ret.z1.humidifySetpoint = rb.readFloat();
  ret.z1.dehumidifySetpoint = rb.readFloat();
  ret.z1.CO2Setpoint = rb.readFloat();
  ret.z1.lightCountdown = [rb.readShort(), rb.readShort()];
  rb.showOffset("done");
*/
}

var procwConIP2=(rb)=>{
  let ret={z0:{}, z1:{}, eq:[]};
  var hr, mn, sc;
  rb.setOffset(0); // 0
  ret.configVersion = rb.readInt(); // 4
  for(let i = 0 ; i < cnst.NUM_EQUIPMENT ; i++){ // 4
    ret.eq.push({oneChannelOverride: rb.readByte()}); // 12
  } // 12
  for(let i = 0 ; i < cnst.NUM_EQUIPMENT / 2 ; i++){ // 12
    let om = rb.readByte(); // 16
    let op = rb.readByte(); // 20
    ret.eq[i * 2 ].twoChannelOverrideMode = om; // 20
    ret.eq[i * 2 + 1].twoChannelOverrideMode = om; // 20
    ret.eq[i * 2 ].twoChannelOverridePosition = op; // 20
    ret.eq[i * 2 + 1].twoChannelOverridePosition = op; // 20
  } // 20
  rb.showOffset("temp_stage_cfg"); // 20
  rb.skip(4); // 24
  ret.z0.tempStage = {}; // 24
  ret.z0.tempStage.stageWidth = rb.readFloat(); // 28
  ret.z0.tempStage.deadband = rb.readFloat(); // 32
  ret.z0.tempStage.delayHoldTime = rb.readInt(); // 36
  rb.skip(16); // 52
  rb.skip(4); // 56
  ret.z0.humDehumStage = {}; // 56
  ret.z0.humDehumStage.stageWidth = rb.readFloat(); // 60
  ret.z0.humDehumStage.deadband = rb.readFloat(); // 64
  ret.z0.humDehumStage.delayHoldTime = rb.readInt(); // 68
  ret.z0.humDehumStage.onTime = rb.readShort(); // 70
  ret.z0.humDehumStage.offTime = rb.readShort(); // 72
  ret.z0.humDehumStage.lowTemperature = rb.readFloat(); // 76
  rb.showOffset("setpoint_Cfg"); // 76
  ret.z0.setpoint = []; // 76
  for(let i = 0 ; i < cnst.NUM_SETPOINTS ; i++){ // 76
    rb.skip(1); // 79
    ret.z0.setpoint.push({ // 79
      enabled: rb.readBool(), // 82
      rampTimeMinutes: rb.readShort(), // 88
      startTimeHourMinute: rb.readShort(), // 94
      startTimeMode: rb.readByte(), // 97
    }); // 97
    rb.skip(1); // 100
  } // 100
  for (let i = 0 ; i < cnst.NUM_SETPOINTS; i++){ // 100
    rb.skip(4); // 112
    ret.z0.setpoint[i].heatSetpoint = rb.readFloat(); // 124
    ret.z0.setpoint[i].coolSetpoint = rb.readFloat(); // 136
    rb.skip(20); // 196
  } // 196
  for (let i = 0 ; i < cnst.NUM_SETPOINTS; i++){ // 196
    ret.z0.setpoint[i].humidifySetpoint = rb.readFloat(); // 208
    ret.z0.setpoint[i].dehumidifySetpoint = rb.readFloat(); // 220
    rb.skip(24); // 292
  } // 292
  rb.showOffset("ipaddress"); // 292
  rb.skip(16); // 308
  for(let i = 0 ; i < cnst.NUM_EQUIPMENT; i++){ // 308
    hr=rb.readByte(); // 316
    mn=rb.readByte(); // 324
    ret.eq[i].timedOverrideStartTimeHourMinute = 60 * hr + mn; // 324
    hr=rb.readByte(); // 332
    mn=rb.readByte(); // 340
    sc=rb.readByte(); // 348
    ret.eq[i].timedOverrideDurationSeconds = 3600 * hr + 60 * mn + sc; // 348
    ret.eq[i].timedOverrideMode = rb.readByte(); // 356
    rb.skip(2); // 372
    let top = rb.readFloat(); // 404
    if (top < 0) top = 0; // 404
    ret.eq[i].timedOverridePosition = top; // 404
  } // 404
  rb.showOffset("device_Cfg"); // 404
  for(let i = 0 ; i < cnst.NUM_EQUIPMENT; i++){ // 404
    ret.eq[i].type = rb.readByte(); // 412
    rb.skip(1); // 420
    ret.eq[i].name = rb.readString(11); // 508
    rb.skip(3); // 532
    switch(ret.eq[i].type){ // 532
      case 0: // 532
      case 4: // 532
      case 5: // 532
        rb.showOffset("AC"); // 532
        ret.eq[i].heat2=rb.readBool(); // 532
        ret.eq[i].heat1=rb.readBool(); // 532
        ret.eq[i].normal=rb.readBool(); // 532
        ret.eq[i].cool1=rb.readBool(); // 532
        ret.eq[i].cool2=rb.readBool(); // 532
        ret.eq[i].cool3=rb.readBool(); // 532
        ret.eq[i].cool4=rb.readBool(); // 532
        ret.eq[i].cool5=rb.readBool(); // 532
        ret.eq[i].cool6=rb.readBool(); // 532
        ret.eq[i].humidify=rb.readByte(); // 532
        rb.skip(1); // 532
        ret.eq[i].dehumidify=rb.readByte(); // 532
        ret.eq[i].coldDehum=rb.readByte(); // 532
        if(ret.eq[i].type === 0){ // 532
          rb.skip(1); // 532
          ret.eq[i].onOffType=rb.readByte(); // 532
          rb.skip(21); // 532
        } else { // 532
          rb.skip(23); // 532
        } // 532
        if(ret.eq[i].humidify === 2) ret.eq[i].humidify = -1; // 532
        if(ret.eq[i].dehumidify === 2) ret.eq[i].dehumidify = -1; // 532
        if(ret.eq[i].coldDehum === 2) ret.eq[i].coldDehum = -1; // 532
        break; // 532
      case 8: // 532
        rb.showOffset("Irrigation"); // 532
        ret.eq[i].irrigationEnable=rb.readBool(); // 532
        rb.skip(13); // 532
        ret.eq[i].irrigationStartTimeHourMinute=rb.readShort(); // 532
        ret.eq[i].irrigationEndTimeHourMinute=rb.readShort(); // 532
        rb.skip(2); // 532
        ret.eq[i].irrigationOnTimeSeconds=rb.readInt(); // 532
        ret.eq[i].irrigationOffTimeSeconds=rb.readInt(); // 532
        rb.skip(8); // 532
        break; // 532
      case 3: // 532
        rb.showOffset("Alarm"); // 532
        rb.skip(13); // 532
        let temp = rb.readByte(); // 532
        let hum = rb.readByte(); // 532
        let co2 = rb.readByte(); // 532
        ret.eq[i].alarmTemperatureHigh = (temp & 0x01 !== 0) ? true : false; // 532
        ret.eq[i].alarmTemperatureLow = (temp & 0x02 !== 0) ? true : false; // 532
        ret.eq[i].alarmHumidityHigh = (hum & 0x01 !== 0) ? true : false; // 532
        ret.eq[i].alarmHumidityLow = (hum & 0x02 !== 0) ? true : false; // 532
        ret.eq[i].alarmCO2High = (co2 & 0x01 !== 0) ? true : false; // 532
        ret.eq[i].alarmCO2Low = (co2 & 0x02 !== 0) ? true : false; // 532
        rb.skip(20); // 532
      default: // 532
        rb.skip(36); // 820
    } // 820
  } // 820
  rb.showOffset("longitude"); // 820
  ret.longitude=rb.readShort(); // 822
  ret.latitude=rb.readShort(); // 824
  rb.skip(11); // 835
  ret.timeFormat=rb.readByte(); // 836
  ret.temperatureUnits=rb.readByte(); // 837
  ret.windSpeedUnits=rb.readByte(); // 838
  ret.lightUnits=rb.readByte(); // 839
  rb.showOffset("0x0354"); // 839
  rb.setOffset(0x0354); // 852
  ret.z0.calibration={ // 852
    inTemp: rb.readFloat(), // 856
    inHum: rb.readFloat(), // 860
    outTemp: rb.readFloat(), // 864
    light: rb.readFloat(), // 868
    x1:  rb.readFloat(), // 872
    backup: rb.readFloat(), // 876
    x2:  rb.readFloat(),// just skipping // 880
    outHum: rb.readFloat(), // 884
    CO2: rb.readFloat(), // 888
  } // 888
  rb.showOffset("0x037F"); // 888
  rb.setOffset(0x037F); // 895
  ret.z0.mapping={ // 895
    inTemp: rb.readByte(), // 896
    inHum: rb.readByte(), // 897
    light: rb.readByte(), // 898
    outTemp: rb.readByte(), // 899
    x1: rb.readShort(),// skip 2 // 901
    backup: rb.readByte(), // 902
    outHum: rb.readByte(), // 903
    CO2: rb.readByte(), // 904
  } // 904
  rb.skip(4); // 908
  ret.selfTest = { // 908
    enable: rb.readByte(), // 909
    x1: rb.readByte(), // 910
    timeMinute: rb.readByte(), // 911
    timeHour: rb.readByte(), // 912
    retestMinute: rb.readByte(), // 913
    x2: rb.skip(3), // 916
    temperatureDifference: rb.readFloat(), // 920
  }; // 920
  rb.showOffset("0x0407"); // 920
  rb.setOffset(0x0407); // 1031
  ret.usbLog={ // 1031
    enable: rb.readBool(), // 1032
    fileFormat: rb.readByte(), // 1033
    durationHour: rb.readByte(), // 1034
    durationMinute: rb.readByte(), // 1035
    durationSecond: rb.readByte(), // 1036
  }; // 1036
  rb.showOffset("0x0417"); // 1036
  rb.setOffset(0x0417); // 1047
  ret.password=rb.readString(12); // 1059
  ret.name=rb.readString(16); // 1075
  rb.skip(5); // 1080
  ret.z0.heatStages=rb.readByte(); // 1081
  ret.z0.coolStages=rb.readByte(); // 1082
  rb.showOffset("0x05F8"); // 1082
  rb.setOffset(0x05F8); // 1528
  ret.backLightOnTime=rb.readInt()/1000; // 1532
  ret.ledLightOnTime=rb.readInt(); // 1536
  ret.outHumidityEnable=rb.readBool(); // 1537
  rb.skip(3); // 1540
  ret.outHumidifyOffset=rb.readFloat(); // 1544
  ret.outDehumidifyOffset=rb.readFloat(); // 1548
  ret.z0.CO2LSB=rb.readFloat(); // 1552
  for (let i = 0 ; i < cnst.NUM_SETPOINTS; i++){ // 1552
    ret.z0.setpoint[i].CO2Setpoint=rb.readFloat(); // 1564
    rb.skip(28); // 1648
  } // 1648
  rb.showOffset("CO2_Stage_Cfg"); // 1648
  rb.skip(4); // 1652
  ret.z0.CO2Stage={ // 1652
    stageWidth: rb.readFloat(), // 1656
    deadband: rb.readFloat(), // 1660
    delayHoldTime: rb.readFloat(), // 1664
  }; // 1664
  ret.z0.setpointO={ // 1664
    lightSwitchByTime: rb.readByte(), // 1665
    syncBySetpoint: rb.readBool(), // 1666
    lightOnHourMinute: rb.readShort(), // 1668
    lightOffHourMinute: rb.readShort(), // 1670
  }; // 1670
  rb.skip(2); // 1672
  ret.z0.temperatureHighAlarm=rb.readFloat(), // 1676
  ret.z0.temperatureLowAlarm=rb.readFloat(), // 1680
  ret.z0.humidityHighAlarm=rb.readFloat(), // 1684
  ret.z0.humidityLowAlarm=rb.readFloat(), // 1688
  ret.z0.CO2HighAlarm=rb.readFloat(), // 1692
  ret.z0.CO2LowAlarm=rb.readFloat(), // 1696
  rb.skip(3); // 1699
  ret.z0.airDumpEnable=rb.readBool(); // 1700
  ret.z0.airDumpOffTime=rb.readInt(); // 1704
  ret.z0.airDumpEnable=rb.readInt(); // 1708
  ret.$dateFormat=rb.readByte(); // 1709
  rb.skip(1); // 1710
  rb.showOffset("miscellaneous"); // 1710
  ret.outTemperatureOverride=rb.readShort()/10.0; // 1712
  ret.z0.lightSwitchBankDelay=rb.readShort(); // 1714
  ret.z0.alarmHoldTime=rb.readShort(); // 1716
  ret.z0.temperatureAlarmLightOffDelay=rb.readShort(); // 1718
  ret.z0.lightPowerCooldownTime=rb.readShort(); // 1720
  ret.z0.co2LightCutoffMode=rb.readShort(); // 1722
  ret.z0.co2LightDeadband=rb.readShort(); // 1724
  ret.z0.co2LightThreshold=rb.readShort(); // 1726
  rb.skip(2); // 1728
  ret.$enableStaging=rb.readShort(); // 1730
  ret.z0.enableRamping=rb.readShort(); // 1732
  ret.z0.enableDIFSetpoint=rb.readShort(); // 1734
  ret.z0.enableNightlyCO2=rb.readShort(); // 1736
  ret.z0.enableNoSyncLight=rb.readShort(); // 1738
  ret.z0.enableExternalLight=rb.readShort(); // 1740
  rb.skip(12); // 1752
  ret.z0.CO2Enable=rb.readBool(); // 1753
  rb.skip(3); // 1756
  ret.z0.CO2OnTime=rb.readInt(); // 1760
  ret.z0.CO2OffTime=rb.readInt(); // 1764
  rb.showOffset("CO2cycle_adj"); // 1764
  ret.z0.co2Fuzzy=[]; // 1764
  for(let i = 0 ; i < 5 ; i++){ // 1764
    ret.z0.co2Fuzzy.push({ // 1764
      deviation: rb.readShort(), // 1774
      x1: rb.readShort(), // 1784
      delayLevel: rb.readFloat(), // 1804
    }); // 1804
  } // 1804
  rb.showOffset("co2FuzzyEnable"); // 1804
  ret.z0.co2FuzzyEnable = rb.readBool(); // 1805
  rb.skip(3); // 1808
  ret.z0.lightSetpointTiming = rb.readInt(); // 1812
  ret.z0.dayLightThreshold = rb.readShort(); // 1814
  ret.z0.dayLightDeadband = rb.readShort(); // 1816
  rb.skip(8); // 1824
  ret.z0.airExchangeStartTime=rb.readShort(); // 1826
  ret.z0.airExchangeEndTime=rb.readShort(); // 1828
  rb.showOffset("CO2RampCfg"); // 1828
  ret.$enableCO2Ramping=rb.readByte(); // 1829
  rb.skip(1); // 1830
  ret.$co2DayRampMinutes=rb.readShort(); // 1832
  ret.$co2DIFRampMinutes=rb.readShort(); // 1834
  ret.$enableSyncTime=rb.readByte(); // 1835
  rb.skip(1); // 1836
  ret.$timeOffset=rb.readInt(); // 1840
  ret.$timeCountry=rb.readByte(); // 1841
  rb.skip(3); // 1844
  rb.showOffset("temp_stage_Cfg"); // 1844
  rb.skip(4); // 1848
  ret.z1.tempStage={ // 1848
    stageWidth: rb.readFloat(), // 1852
    deadband: rb.readFloat(), // 1856
    delayHoldTime: rb.readInt(), // 1860
  }; // 1860
  rb.skip(4); // 1864
  ret.z1.humDehumStage={ // 1864
    stageWidth: rb.readFloat(), // 1868
    deadband: rb.readFloat(), // 1872
    delayHoldTime: rb.readInt(), // 1876
    onTime: rb.readShort(), // 1878
    offTime: rb.readShort(), // 1880
    lowTemperature: rb.readFloat(), // 1884
  } // 1884
  rb.skip(4); // 1888
  ret.z1.CO2Stage={ // 1888
    stageWidth: rb.readFloat(), // 1892
    deadband: rb.readFloat(), // 1896
    delayHoldTime: rb.readInt(), // 1900
  } // 1900
  rb.showOffset("temp_stagevals"); // 1900
  ret.z1.setpoint = []; // 1900
  for(let i = 0 ; i < cnst.NUM_SETPOINTS; i++){ // 1900
    rb.skip(4); // 1912
    ret.z1.setpoint.push({ // 1912
      heatSetpoint: rb.readFloat(), // 1924
      coolSetpoint: rb.readFloat(), // 1936
    }); // 1936
    rb.skip(20); // 1996
  } // 1996
  for(let i = 0 ; i < cnst.NUM_SETPOINTS; i++){ // 1996
      ret.z1.setpoint[i].humidifySetpoint = rb.readFloat(); // 2008
      ret.z1.setpoint[i].dehumidifySetpoint = rb.readFloat(); // 2020
      rb.skip(24); // 2092
  } // 2092
  rb.showOffset("CO2StageVal"); // 2092
  for(let i = 0 ; i < cnst.NUM_SETPOINTS; i++){ // 2092
      ret.z1.setpoint[i].CO2Setpoint = rb.readFloat(); // 2104
      rb.skip(28); // 2188
  } // 2188
  ret.z1.heatStages=rb.readByte(); // 2189
  ret.z1.coolStages=rb.readByte(); // 2190
  for(let i = 0 ; i < cnst.NUM_SETPOINTS; i++){ // 2190
      rb.skip(1); // 2193
      ret.z1.setpoint[i].enabled = rb.readBool(); // 2196
      ret.z1.setpoint[i].rampTimeMinutes = rb.readShort(); // 2202
      ret.z1.setpoint[i].startTimeHourMinute = rb.readShort(); // 2208
      ret.z1.setpoint[i].startTimeMode = rb.readByte(); // 2211
      rb.skip(1); // 2214
  } // 2214
  rb.skip(2); // 2216
  ret.z1.lightSetpointTiming=rb.readInt(); // 2220
  ret.z1.dayLightThreshold=rb.readShort(); // 2222
  ret.z1.dayLightDeadband=rb.readShort(); // 2224
  ret.z1.setpointO={ // 2224
    lightSwitchByTime: rb.readByte(), // 2225
    syncBySetpoint: rb.readBool(), // 2226
    lightOnHourMinute: rb.readShort(), // 2228
    lightOffHourMinute: rb.readShort(), // 2230
  }; // 2230
  rb.skip(2); // 2232
  ret.z1.CO2Enable=rb.readBool(); // 2233
  rb.skip(3); // 2236
  ret.z1.CO2OnTime=rb.readInt(); // 2240
  ret.z1.CO2OffTime=rb.readInt(); // 2244
  ret.z1.co2Fuzzy = []; // 2244
  for(let i = 0 ; i < 5 ; i++){ // 2244
    ret.z1.co2Fuzzy.push({ // 2244
      deviation: rb.readShort(), // 2254
      x1: rb.skip(2), // 2264
      delayLevel: rb.readFloat(), // 2284
    }); // 2284
  } // 2284
  rb.showOffset("co2FuzzyEnable"); // 2284
  ret.z1.co2FuzzyEnable=rb.readBool(); // 2285
  rb.skip(3); // 2288
  rb.skip(3); // 2291
  ret.z1.airDumpEnable=rb.readBool(); // 2292
  ret.z1.airDumpOnTime=rb.readInt(); // 2296
  ret.z1.airDumpOffTime=rb.readInt(); // 2300
   // 2300
  ret.z1.airExchangeStartTime=rb.readShort(); // 2302
  ret.z1.airExchangeEndTime=rb.readShort(); // 2304
   // 2304
  ret.z1.temperatureHighAlarm=rb.readFloat(); // 2308
  ret.z1.temperatureLowAlarm=rb.readFloat(); // 2312
  ret.z1.humidityHighAlarm=rb.readFloat(); // 2316
  ret.z1.humidityLowAlarm=rb.readFloat(); // 2320
  ret.z1.CO2HighAlarm=rb.readFloat(); // 2324
  ret.z1.CO2LowAlarm=rb.readFloat(); // 2328
   // 2328
  ret.z1.calibration={ // 2328
    inTemp: rb.readFloat(), // 2332
    inHum: rb.readFloat(), // 2336
    outTemp: rb.readFloat(), // 2340
    light: rb.readFloat(), // 2344
    x1: rb.skip(4), // 2348
    backup: rb.readFloat(), // 2352
    x2: rb.skip(4), // 2356
    outHum: rb.readFloat(), // 2360
    outHum: rb.readFloat(), // 2364
  }; // 2364
   // 2364
  rb.showOffset("CO2LSB"); // 2364
  ret.z1.CO2LSB=rb.readFloat(); // 2368
  ret.z1.mapping={ // 2368
    inTemp: rb.readByte(), // 2369
    inHum: rb.readByte(), // 2370
    light: rb.readByte(), // 2371
    outTemp: rb.readByte(), // 2372
    x1: rb.skip(2), // 2374
    backup: rb.readByte(), // 2375
    outHum: rb.readByte(), // 2376
    CO2: rb.readByte(), // 2377
    x2: rb.skip(3), // 2380
  }; // 2380
   // 2380
  rb.showOffset("miscellaneous"); // 2380
  ret.z1.lightSwitchBankDelay=rb.readShort(); // 2382
  ret.z1.alarmHoldTime=rb.readShort(); // 2384
  ret.z1.temperatureAlarmLightOffDelay=rb.readShort(); // 2386
  ret.z1.lightPowerCooldownTime=rb.readShort(); // 2388
  ret.z1.co2LightCutoffMode=rb.readShort(); // 2390
  ret.z1.co2LightDeadband=rb.readShort(); // 2392
  ret.z1.co2LightThreshold=rb.readShort(); // 2394
  rb.skip(4); // 2398
  ret.z1.enableRamping=rb.readShort(); // 2400
  ret.z1.enableDIFSetpoint=rb.readShort(); // 2402
  ret.z1.enableNightlyCO2=rb.readShort(); // 2404
  ret.z1.enableNoSyncLight=rb.readShort(); // 2406
  ret.z1.enableExternalLight=rb.readShort(); // 2408
  rb.showOffset("0x09F4"); // 2408
  rb.setOffset(0x09F4); // 2548
  for(let i = 0 ; i < cnst.NUM_EQUIPMENT; i++){ // 2548
    ret.eq[i].zoneIndex=rb.readByte(); // 2556
  } // 2556
  ret.dualSensorModuleBackup=rb.readBool(); // 2557
  rb.showOffset("done"); // 2557
}

var procrConIP2=(rb)=>{
/*tables:
-1, -1: tabs.config
-1, i: tabs.config_equipment
0, -1: tabs.config_zone

keyval, and db:
each serial number needs an id to make the value keys:
serial-zone-channel-id = // x
need to load and store the whole kv store
for now, do it on the fly:
when a serial number comes in, check if we have it. if not, create
a new entry. then, for each value, use the id for that serial
to look up items in keyStore[id]

no, just use keyStore[SerialNumber] to store the keys

readAVal:
make this a function of rp. look up the serial number, and put its id
in rp.


*/
  for (let i = 0 ; i < cnst.NUM_EQUIPMENT ; i++){
    rb.readACVal(-1, i, "byte", "oneChannelOverride", 4 + 1 * i);
  }
  rb.readACVal(0, -1, "float", "temperatureStageWidth", 24);
  rb.readACVal(0, -1, "float", "temperatureDeadband", 28);
  rb.readACVal(0, -1, "int", "temperatureStageDelaySeconds", 32);
  rb.readACVal(0, -1, "float", "humidifyDeadband", 60);
  rb.readACVal(0, -1, "short", "humidifyOnTimeSeconds", 68);
  rb.readACVal(0, -1, "short", "humidifyOffTimeSeconds", 70);
  rb.readACVal(0, -1, "float", "dehumidifyLowTemperature", 72);
  rb.readACVal(0, -1, "short", "dayRampTimeMinutes", 78);
  rb.readACVal(0, -1, "short", "dayStartTimeHourMinute", 80);
  rb.readACVal(0, -1, "byte", "dayStartMode", 82);
  rb.readACVal(0, -1, "bool", "nightEnable", 85);
  rb.readACVal(0, -1, "short", "nightRampTimeMinutes", 86);
  rb.readACVal(0, -1, "short", "nightStartTimeHourMinute", 88);
  rb.readACVal(0, -1, "byte", "nightStartMode", 90);
  rb.readACVal(0, -1, "bool", "difEnable", 93);
  rb.readACVal(0, -1, "short", "difRampTimeMinutes", 94);
  rb.readACVal(0, -1, "short", "difStartTimeHourMinute", 96);
  rb.readACVal(0, -1, "byte", "difStartMode", 98);
  // rb.readACVal(0, -1, "float", "dayHeatSetpoint", 100);
  rb.readACVal(0, -1, "float", "dayHeatSetpoint", 104);
  rb.readACVal(0, -1, "float", "dayCoolSetpoint", 108);
  // rb.readACVal(0, -1, "float", "dayCoolSetpoint", 112);
  // rb.readACVal(0, -1, "float", "dayCoolSetpoint", 116);
  // rb.readACVal(0, -1, "float", "dayCoolSetpoint", 120);
  // rb.readACVal(0, -1, "float", "dayCoolSetpoint", 124);
  // rb.readACVal(0, -1, "float", "dayCoolSetpoint", 128);
  // rb.readACVal(0, -1, "float", "nightHeatSetpoint", 132);
  rb.readACVal(0, -1, "float", "nightHeatSetpoint", 136);
  rb.readACVal(0, -1, "float", "nightCoolSetpoint", 140);
  // rb.readACVal(0, -1, "float", "nightCoolSetpoint", 144);
  // rb.readACVal(0, -1, "float", "nightCoolSetpoint", 148);
  // rb.readACVal(0, -1, "float", "nightCoolSetpoint", 152);
  // rb.readACVal(0, -1, "float", "nightCoolSetpoint", 156);
  // rb.readACVal(0, -1, "float", "nightCoolSetpoint", 160);
  // rb.readACVal(0, -1, "float", "difHeatSetpoint", 164);
  rb.readACVal(0, -1, "float", "difHeatSetpoint", 168);
  rb.readACVal(0, -1, "float", "difCoolSetpoint", 172);
  // rb.readACVal(0, -1, "float", "difCoolSetpoint", 176);
  // rb.readACVal(0, -1, "float", "difCoolSetpoint", 180);
  // rb.readACVal(0, -1, "float", "difCoolSetpoint", 184);
  // rb.readACVal(0, -1, "float", "difCoolSetpoint", 188);
  // rb.readACVal(0, -1, "float", "difCoolSetpoint", 192);
  rb.readACVal(0, -1, "float", "dayHumidifySetpoint", 196);
  rb.readACVal(0, -1, "float", "dayDehumidifySetpoint", 200);
  // rb.readACVal(0, -1, "float", "dayDehumidifySetpoint", 204);
  // rb.readACVal(0, -1, "float", "dayDehumidifySetpoint", 208);
  // rb.readACVal(0, -1, "float", "dayDehumidifySetpoint", 212);
  // rb.readACVal(0, -1, "float", "dayDehumidifySetpoint", 216);
  // rb.readACVal(0, -1, "float", "dayDehumidifySetpoint", 220);
  // rb.readACVal(0, -1, "float", "dayDehumidifySetpoint", 224);
  rb.readACVal(0, -1, "float", "nightHumidifySetpoint", 228);
  rb.readACVal(0, -1, "float", "nightDehumidifySetpoint", 232);
  // rb.readACVal(0, -1, "float", "nightDehumidifySetpoint", 236);
  // rb.readACVal(0, -1, "float", "nightDehumidifySetpoint", 240);
  // rb.readACVal(0, -1, "float", "nightDehumidifySetpoint", 244);
  // rb.readACVal(0, -1, "float", "nightDehumidifySetpoint", 248);
  // rb.readACVal(0, -1, "float", "nightDehumidifySetpoint", 252);
  // rb.readACVal(0, -1, "float", "nightDehumidifySetpoint", 256);
  rb.readACVal(0, -1, "float", "difHumidifySetpoint", 260);
  rb.readACVal(0, -1, "float", "difDehumidifySetpoint", 264);
  // rb.readACVal(0, -1, "float", "difDehumidifySetpoint", 268);
  // rb.readACVal(0, -1, "float", "difDehumidifySetpoint", 272);
  // rb.readACVal(0, -1, "float", "difDehumidifySetpoint", 276);
  // rb.readACVal(0, -1, "float", "difDehumidifySetpoint", 280);
  // rb.readACVal(0, -1, "float", "difDehumidifySetpoint", 284);
  // rb.readACVal(0, -1, "float", "difDehumidifySetpoint", 288);
  for (i = 0 ; i < cnst.NUM_EQUIPMENT ; i++){
    var hr, mn, sc;
    hr = rb.readACVal(-1, i, "byte", "", 308 + 12 * i);// just returns value
    mn = rb.readACVal(-1, i, "byte", "", 309 + 12 * i);
    // just puts value:
    rb.readACVal(-1, i, "", "timedOverrideStartTimeHourMinute", 60 * hr + mn);
    // rb.readACVal(-1, $i, "byte", "timedOverrideStartTimeHourMinute", 308 + 12 * i);
    // rb.readACVal(-1, $i, "byte", "timedOverrideStartTimeHourMinute", 309 + 12 * i);
    hr = rb.readACVal(-1, $i, "byte", "", 310 + 12 * i);
    mn = rb.readACVal(-1, $i, "byte", "", 311 + 12 * i);
    sc = rb.readACVal(-1, $i, "byte", "", 312 + 12 * i);
    rb.readACVal(-1, i, "byte", "timedOverrideDurationSeconds", 3600 * hr + 60 * mn + sc);
    // rb.readACVal(-1, $i, "byte", "timedOverrideDurationSeconds", 310 + 12 * i);
    // rb.readACVal(-1, $i, "byte", "timedOverrideDurationSeconds", 311 + 12 * i);
    // rb.readACVal(-1, $i, "byte", "timedOverrideDurationSeconds", 312 + 12 * i);
    rb.readACVal(-1, $i, "byte", "timedOverrideMode", 313 + 12 * i);
    rb.readACVal(-1, $i, "float", "channelType", 316 + 12 * i);
  }
  for (let i = 0 ; i < cnst.NUM_EQUIPMENT ; i++){
    let chType = rb.readACVal(-1, i, "byte", "channelType", 404 + 52 * i);
    rb.readACVal(-1, i, "string", "channelName", 406 + 52 * i);
    switch(chType){
    case 0:// on/off
    case 4:// fan
    case 5:// ac
      rb.readACVal(-1, i, "bool", "heat2", 420 + 52 * i);
      rb.readACVal(-1, i, "bool", "heat1", 421 + 52 * i);
      rb.readACVal(-1, i, "bool", "normal", 422 + 52 * i);
      rb.readACVal(-1, i, "bool", "cool1", 423 + 52 * i);
      rb.readACVal(-1, i, "bool", "cool2", 424 + 52 * i);
      rb.readACVal(-1, i, "bool", "cool3", 425 + 52 * i);
      rb.readACVal(-1, i, "bool", "cool4", 426 + 52 * i);
      rb.readACVal(-1, i, "bool", "cool5", 427 + 52 * i);
      rb.readACVal(-1, i, "bool", "cool6", 428 + 52 * i);
      rb.readACVal(-1, i, "bool", "humidify", 429 + 52 * i);
      rb.readACVal(-1, i, "byte", "dehumidify", 431 + 52 * i);
      rb.readACVal(-1, i, "byte", "coldDehum", 432 + 52 * i);
      rb.readACVal(-1, i, "byte", "onOffType", 434 + 52 * i);
      break;
    case 6:// light
    case 7:// co2
      break;
    case 8:// irrigation
      rb.readACVal(-1, $i, "bool", "irrigationEnable", 420 + 52 * i);
      rb.readACVal(-1, $i, "short", "irrigationStartTimeHourMinute", 434 + 52 * i);
      rb.readACVal(-1, $i, "short", "irrigationEndTimeHourMinute", 436 + 52 * i);
      rb.readACVal(-1, $i, "int", "irrigationOnTimeSeconds", 440 + 52 * i);
      rb.readACVal(-1, $i, "int", "irrigationOffTimeSeconds", 444 + 52 * i);
      break;
    case 3:// alarm
      // rb.readACVal(-1, -1, "float", "temperatureUnits", 424);
      rb.readACVal(-1, $i, "byte", "alarmTemperatureHigh", 433 + 52 * i);// these need some work
      rb.readACVal(-1, $i, "byte", "alarmHumidityHigh", 434 + 52 * i);
      rb.readACVal(-1, $i, "byte", "alarmCO2High", 435 + 52 * i);
      break;
    case 1:// vent
    case 2:// alarm ???
    default:
    break;
    }

  }

  rb.readACVal(-1, -1, "short", "longitude", 820);
  rb.readACVal(-1, -1, "short", "latitude", 822);
  rb.readACVal(-1, -1, "byte", "timeFormat", 835);
  rb.readACVal(-1, -1, "byte", "temperatureUnits", 836);
  rb.readACVal(-1, -1, "byte", "windSpeedUnits", 837);
  rb.readACVal(-1, -1, "byte", "lightUnits", 838);
  rb.readACVal(0, -1, "float", "sensorCalibrationInTemperature", 852);
  rb.readACVal(0, -1, "float", "sensorCalibrationInHumidity", 856);
  rb.readACVal(0, -1, "float", "sensorCalibrationOutTemperature", 860);
  rb.readACVal(0, -1, "float", "sensorCalibrationLight", 864);
  rb.readACVal(0, -1, "float", "sensorCalibrationBackupTemperature", 872);
  rb.readACVal(0, -1, "float", "sensorCalibrationOutHumidity", 880);
  rb.readACVal(0, -1, "float", "sensorCalibrationCO2", 884);
  rb.readACVal(0, -1, "byte", "sensorMappingInTemperature", 895);
  rb.readACVal(0, -1, "byte", "sensorMappingInHumidity", 896);
  rb.readACVal(0, -1, "byte", "sensorMappingLight", 897);
  rb.readACVal(0, -1, "byte", "sensorMappingOutTemperature", 898);
  rb.readACVal(0, -1, "byte", "sensorMappingBackupTemperature", 901);
  rb.readACVal(0, -1, "byte", "sensorMappingOutHumidity", 902);
  rb.readACVal(0, -1, "byte", "sensorMappingCO2", 903);
  rb.readACVal(-1, -1, "bool", "selfTestEnable", 908);

  mn = rb.readACVal(-1, -1, "byte", "", 910);
  hr = rb.readACVal(-1, -1, "byte", "", 911);
  rb.readACVal(-1, -1, "", "selfTestTimeHourMinute", 60 * hr + mn);
  // rb.readACVal(-1, -1, "byte", "selfTestTimeHourMinute", 910);
  // rb.readACVal(-1, -1, "byte", "selfTestTimeHourMinute", 911);

  rb.readACVal(-1, -1, "byte", "selfTestRetestMinutes", 912);
  rb.readACVal(-1, -1, "float", "selfTestTemperatureDifference", 916);
  rb.readACVal(-1, -1, "bool", "usbLoggingEnable", 1031);
  rb.readACVal(-1, -1, "byte", "usbLoggingFormat", 1032);

  hr = rb.readACVal(-1, -1, "byte", "", 1033);
  mn = rb.readACVal(-1, -1, "byte", "", 1034);
  sc = rb.readACVal(-1, -1, "byte", "", 1035);
  rb.readACVal(-1, -1, "", "usbLoggingIntervalSeconds", 3600 * hr + 60 * mn + sc);
  // rb.readACVal(-1, -1, "byte", "usbLoggingIntervalSeconds", 1033);
  // rb.readACVal(-1, -1, "byte", "usbLoggingIntervalSeconds", 1034);
  // rb.readACVal(-1, -1, "byte", "usbLoggingIntervalSeconds", 1035);

  rb.readACVal(-1, -1, "string", "screenPassword", 1047);
  rb.readACVal(-1, -1, "string", "controllerName", 1059);

  rb.readACVal(0, -1, "byte", "temperatureHeatStages", 1080);
  rb.readACVal(0, -1, "byte", "temperatureCoolStages", 1081);
  rb.readACVal(-1, -1, "int", "backLightOnTime", 1528);
  rb.readACVal(-1, -1, "int", "ledLightOnTime", 1532);
  rb.readACVal(-1, -1, "bool", "outHumidityOverrideEnable", 1536);
  rb.readACVal(-1, -1, "float", "outHumidifyOffset", 1540);
  rb.readACVal(-1, -1, "float", "outDehumidifyOffset", 1544);
  rb.readACVal(0, -1, "float", "sensorCO2LSB", 1548);
  rb.readACVal(0, -1, "float", "dayCO2Setpoint", 1552);
  rb.readACVal(0, -1, "float", "nightCO2Setpoint", 1584);
  rb.readACVal(0, -1, "float", "difCO2Setpoint", 1616);
  rb.readACVal(0, -1, "float", "co2Deadband", 1656);
  rb.readACVal(0, -1, "byte", "lightSwitchByTime", 1664);
  rb.readACVal(0, -1, "bool", "lightTimeByDaySetpoint", 1665);
  rb.readACVal(0, -1, "short", "lightOnTimeHourMinute", 1666);
  rb.readACVal(0, -1, "short", "lightOffTimeHourMinute", 1668);
  rb.readACVal(0, -1, "float", "alarmHighTemperature", 1672);
  rb.readACVal(0, -1, "float", "alarmLowTemperature", 1676);
  rb.readACVal(0, -1, "float", "alarmHighHumidity", 1680);
  rb.readACVal(0, -1, "float", "alarmLowHumidity", 1684);
  rb.readACVal(0, -1, "float", "alarmHighCO2", 1688);
  rb.readACVal(0, -1, "float", "alarmLowCO2", 1692);
  rb.readACVal(0, -1, "bool", "airDumpEnable", 1699);
  rb.readACVal(0, -1, "int", "airDumpOnTimeSeconds", 1700);
  rb.readACVal(0, -1, "int", "airDumpOffTimeSeconds", 1704);
  rb.readACVal(-1, -1, "byte", "dateFormat", 1708);
  rb.readACVal(-1, -1, "short", "outTemperatureOverride", 1710);
  rb.readACVal(0, -1, "short", "lightSwitchBankDelay", 1712);
  rb.readACVal(0, -1, "short", "alarmHoldTime", 1714);
  rb.readACVal(0, -1, "short", "temperatureAlarmLightOffDelay", 1716);
  rb.readACVal(0, -1, "short", "lightPowerCooldownTime", 1718);
  rb.readACVal(0, -1, "short", "co2LightCutoffMode", 1720);
  rb.readACVal(0, -1, "short", "co2LightDeadband", 1722);
  rb.readACVal(0, -1, "short", "co2LightThreshold", 1724);
  rb.readACVal(-1, -1, "short", "enableStaging", 1728);
  rb.readACVal(0, -1, "short", "enableRamping", 1730);
  rb.readACVal(0, -1, "short", "enableDIFSetpoint", 1732);
  rb.readACVal(0, -1, "short", "enableNightlyCO2", 1734);
  rb.readACVal(0, -1, "short", "enableNoSyncLight", 1736);
  rb.readACVal(0, -1, "short", "enableExternalLight", 1738);
  rb.readACVal(0, -1, "bool", "co2FanCutEnable", 1752);
  rb.readACVal(0, -1, "int", "co2CycleOnSeconds", 1756);
  rb.readACVal(0, -1, "int", "co2CycleOffSeconds", 1760);
  for (let i = 0 ; i < 5 ; i++){
    rb.readACVal(0, -1, "short", "co2FuzzyLevel" + i.toString() + "Error", 1764 + 8 * i);
    rb.readACVal(0, -1, "float", "co2FuzzyLevel" + i.toString() + "Delay", 1768 + 8 * i);
  }
  rb.readACVal(0, -1, "bool", "co2FuzzyEnable", 1804);
  rb.readACVal(0, -1, "int", "timingAdjustMode", 1808);
  rb.readACVal(0, -1, "short", "timingLightThreshold", 1812);
  rb.readACVal(0, -1, "short", "timingLightDeadband", 1814);
  rb.readACVal(0, -1, "short", "airExchangeStartTime", 1824);
  rb.readACVal(0, -1, "short", "airExchangeEndTime", 1826);
  rb.readACVal(-1, -1, "bool", "enableCO2Ramping", 1828);
  rb.readACVal(-1, -1, "short", "co2DayRampMinutes", 1830);
  rb.readACVal(-1, -1, "short", "co2DIFRampMinutes", 1832);
  rb.readACVal(-1, -1, "bool", "enableSyncTime", 1834);
  rb.readACVal(-1, -1, "int", "timeOffset", 1836);
  rb.readACVal(-1, -1, "bool", "timeCountry", 1840);
  rb.readACVal(1, -1, "float", "temperatureStageWidth", 1848);
  rb.readACVal(1, -1, "float", "temperatureDeadband", 1852);
  rb.readACVal(1, -1, "int", "temperatureStageDelaySeconds", 1856);
  rb.readACVal(1, -1, "float", "humidifyDeadband", 1868);
  rb.readACVal(1, -1, "short", "humidifyOnTimeSeconds", 1876);
  rb.readACVal(1, -1, "short", "humidifyOffTimeSeconds", 1878);
  rb.readACVal(1, -1, "float", "dehumidifyLowTemperature", 1880);
  rb.readACVal(1, -1, "float", "co2Deadband", 1892);
  rb.readACVal(1, -1, "float", "dayHeatSetpoint", 1900);
  rb.readACVal(1, -1, "float", "dayHeatSetpoint", 1904);
  rb.readACVal(1, -1, "float", "dayCoolSetpoint", 1908);
  rb.readACVal(1, -1, "float", "dayCoolSetpoint", 1912);
  rb.readACVal(1, -1, "float", "dayCoolSetpoint", 1916);
  rb.readACVal(1, -1, "float", "dayCoolSetpoint", 1920);
  rb.readACVal(1, -1, "float", "dayCoolSetpoint", 1924);
  rb.readACVal(1, -1, "float", "dayCoolSetpoint", 1928);
  rb.readACVal(1, -1, "float", "nightHeatSetpoint", 1932);
  rb.readACVal(1, -1, "float", "nightHeatSetpoint", 1936);
  rb.readACVal(1, -1, "float", "nightCoolSetpoint", 1940);
  rb.readACVal(1, -1, "float", "nightCoolSetpoint", 1944);
  rb.readACVal(1, -1, "float", "nightCoolSetpoint", 1948);
  rb.readACVal(1, -1, "float", "nightCoolSetpoint", 1952);
  rb.readACVal(1, -1, "float", "nightCoolSetpoint", 1956);
  rb.readACVal(1, -1, "float", "nightCoolSetpoint", 1960);
  rb.readACVal(1, -1, "float", "difHeatSetpoint", 1964);
  rb.readACVal(1, -1, "float", "difHeatSetpoint", 1968);
  rb.readACVal(1, -1, "float", "difCoolSetpoint", 1972);
  rb.readACVal(1, -1, "float", "difCoolSetpoint", 1976);
  rb.readACVal(1, -1, "float", "difCoolSetpoint", 1980);
  rb.readACVal(1, -1, "float", "difCoolSetpoint", 1984);
  rb.readACVal(1, -1, "float", "difCoolSetpoint", 1988);
  rb.readACVal(1, -1, "float", "difCoolSetpoint", 1992);
  rb.readACVal(1, -1, "float", "dayHumidifySetpoint", 1996);
  rb.readACVal(1, -1, "float", "dayDehumidifySetpoint", 2000);
  rb.readACVal(1, -1, "float", "dayDehumidifySetpoint", 2004);
  rb.readACVal(1, -1, "float", "dayDehumidifySetpoint", 2008);
  rb.readACVal(1, -1, "float", "dayDehumidifySetpoint", 2012);
  rb.readACVal(1, -1, "float", "dayDehumidifySetpoint", 2016);
  rb.readACVal(1, -1, "float", "dayDehumidifySetpoint", 2020);
  rb.readACVal(1, -1, "float", "dayDehumidifySetpoint", 2024);
  rb.readACVal(1, -1, "float", "nightHumidifySetpoint", 2028);
  rb.readACVal(1, -1, "float", "nightDehumidifySetpoint", 2032);
  rb.readACVal(1, -1, "float", "nightDehumidifySetpoint", 2036);
  rb.readACVal(1, -1, "float", "nightDehumidifySetpoint", 2040);
  rb.readACVal(1, -1, "float", "nightDehumidifySetpoint", 2044);
  rb.readACVal(1, -1, "float", "nightDehumidifySetpoint", 2048);
  rb.readACVal(1, -1, "float", "nightDehumidifySetpoint", 2052);
  rb.readACVal(1, -1, "float", "nightDehumidifySetpoint", 2056);
  rb.readACVal(1, -1, "float", "difHumidifySetpoint", 2060);
  rb.readACVal(1, -1, "float", "difDehumidifySetpoint", 2064);
  rb.readACVal(1, -1, "float", "difDehumidifySetpoint", 2068);
  rb.readACVal(1, -1, "float", "difDehumidifySetpoint", 2072);
  rb.readACVal(1, -1, "float", "difDehumidifySetpoint", 2076);
  rb.readACVal(1, -1, "float", "difDehumidifySetpoint", 2080);
  rb.readACVal(1, -1, "float", "difDehumidifySetpoint", 2084);
  rb.readACVal(1, -1, "float", "difDehumidifySetpoint", 2088);
  rb.readACVal(1, -1, "float", "dayCO2Setpoint", 2092);
  rb.readACVal(1, -1, "float", "nightCO2Setpoint", 2124);
  rb.readACVal(1, -1, "float", "difCO2Setpoint", 2156);
  rb.readACVal(1, -1, "byte", "temperatureHeatStages", 2188);
  rb.readACVal(1, -1, "byte", "temperatureCoolStages", 2189);
  rb.readACVal(1, -1, "short", "dayRampTimeMinutes", 2192);
  rb.readACVal(1, -1, "short", "dayStartTimeHourMinute", 2194);
  rb.readACVal(1, -1, "byte", "dayStartMode", 2196);
  rb.readACVal(1, -1, "bool", "nightEnable", 2199);
  rb.readACVal(1, -1, "short", "nightRampTimeMinutes", 2200);
  rb.readACVal(1, -1, "short", "nightStartTimeHourMinute", 2202);
  rb.readACVal(1, -1, "byte", "nightStartMode", 2204);
  rb.readACVal(1, -1, "bool", "difEnable", 2207);
  rb.readACVal(1, -1, "short", "difRampTimeMinutes", 2208);
  rb.readACVal(1, -1, "short", "difStartTimeHourMinute", 2210);
  rb.readACVal(1, -1, "byte", "difStartMode", 2212);
  rb.readACVal(1, -1, "int", "timingAdjustMode", 2216);
  rb.readACVal(1, -1, "short", "timingLightThreshold", 2220);
  rb.readACVal(1, -1, "short", "timingLightDeadband", 2222);
  rb.readACVal(1, -1, "byte", "lightSwitchByTime", 2224);
  rb.readACVal(1, -1, "bool", "lightTimeByDaySetpoint", 2225);
  rb.readACVal(1, -1, "short", "lightOnTimeHourMinute", 2226);
  rb.readACVal(1, -1, "short", "lightOffTimeHourMinute", 2228);
  rb.readACVal(1, -1, "bool", "co2FanCutEnable", 2232);
  rb.readACVal(1, -1, "int", "co2CycleOnSeconds", 2236);
  rb.readACVal(1, -1, "int", "co2CycleOffSeconds", 2240);

  for (let i = 0 ; i < 5 ; i++){
    rb.readACVal(1, -1, "short", "co2FuzzyLevel" + i.toString() + "Error", 2244 + 8 * i);
    rb.readACVal(1, -1, "float", "co2FuzzyLevel" + i.toString() + "Delay", 2248 + 8 * i);
  }
  // rb.readACVal(1, -1, "short", "co2FuzzyLevel.$i.Error", 2244);
  // rb.readACVal(1, -1, "float", "co2FuzzyLevel.$i.Delay", 2264);

  rb.readACVal(1, -1, "bool", "co2FuzzyEnable", 2284);
  rb.readACVal(1, -1, "bool", "airDumpEnable", 2291);
  rb.readACVal(1, -1, "int", "airDumpOnTimeSeconds", 2292);
  rb.readACVal(1, -1, "int", "airDumpOffTimeSeconds", 2296);
  rb.readACVal(1, -1, "short", "airExchangeStartTime", 2300);
  rb.readACVal(1, -1, "short", "airExchangeEndTime", 2302);
  rb.readACVal(1, -1, "float", "alarmHighTemperature", 2304);
  rb.readACVal(1, -1, "float", "alarmLowTemperature", 2308);
  rb.readACVal(1, -1, "float", "alarmHighHumidity", 2312);
  rb.readACVal(1, -1, "float", "alarmLowHumidity", 2316);
  rb.readACVal(1, -1, "float", "alarmHighCO2", 2320);
  rb.readACVal(1, -1, "float", "alarmLowCO2", 2324);
  rb.readACVal(1, -1, "float", "sensorCalibrationInTemperature", 2328);
  rb.readACVal(1, -1, "float", "sensorCalibrationInHumidity", 2332);
  rb.readACVal(1, -1, "float", "sensorCalibrationOutTemperature", 2336);
  rb.readACVal(1, -1, "float", "sensorCalibrationLight", 2340);
  rb.readACVal(1, -1, "float", "sensorCalibrationBackupTemperature", 2348);
  rb.readACVal(1, -1, "float", "sensorCalibrationOutHumidity", 2356);
  rb.readACVal(1, -1, "float", "sensorCalibrationCO2", 2360);
  rb.readACVal(1, -1, "float", "sensorCO2LSB", 2364);
  rb.readACVal(1, -1, "byte", "sensorMappingInTemperature", 2368);
  rb.readACVal(1, -1, "byte", "sensorMappingInHumidity", 2369);
  rb.readACVal(1, -1, "byte", "sensorMappingLight", 2370);
  rb.readACVal(1, -1, "byte", "sensorMappingOutTemperature", 2371);
  rb.readACVal(1, -1, "byte", "sensorMappingBackupTemperature", 2374);
  rb.readACVal(1, -1, "byte", "sensorMappingOutHumidity", 2375);
  rb.readACVal(1, -1, "byte", "sensorMappingCO2", 2376);
  rb.readACVal(1, -1, "short", "lightSwitchBankDelay", 2380);
  rb.readACVal(1, -1, "short", "alarmHoldTime", 2382);
  rb.readACVal(1, -1, "short", "temperatureAlarmLightOffDelay", 2384);
  rb.readACVal(1, -1, "short", "lightPowerCooldownTime", 2386);
  rb.readACVal(1, -1, "short", "co2LightCutoffMode", 2388);
  rb.readACVal(1, -1, "short", "co2LightDeadband", 2390);
  rb.readACVal(1, -1, "short", "co2LightThreshold", 2392);
  rb.readACVal(1, -1, "short", "enableRamping", 2398);
  rb.readACVal(1, -1, "short", "enableDIFSetpoint", 2400);
  rb.readACVal(1, -1, "short", "enableNightlyCO2", 2402);
  rb.readACVal(1, -1, "short", "enableNoSyncLight", 2404);
  rb.readACVal(1, -1, "short", "enableExternalLight", 2406);
  for (let i = 0 ; i < cnst.NUM_EQUIPMENT ; i++){
    rb.readACVal(-1, i, "byte", "zoneIndex", 2548 + 1 * i);
  }
  rb.readACVal(-1, -1, "bool", "dualSensorModuleBackup", 2556);
/*
  let inTemperature=rb.readFloat(4);// skip the first 4
  let ret={z0:{}, z1:{}, eq:[]};
  rb.setOffset(4); // 4
  for(let i = 0 ; i < cnst.NUM_EQUIPMENT ; i++){ // 4
    ret.eq.push({oneChannelOverride: rb.readByte()}); // 12
  } // 12
  rb.skip(8); // 20
  rb.showOffset("temp_stage_cfg"); // 20
// temp_stage_cfg: 20   // 20
  rb.skip(4); // 24
  ret.z0.temperatureStageWidth=rb.readFloat(); // 28
  ret.z0.temperatureDeadband=rb.readFloat(); // 32
  ret.z0.temperatureStageDelaySeconds=rb.readInt(); // 36
  rb.skip(16); // 52
  rb.skip(8); // 60
  ret.z0.humidifyDeadband=rb.readFloat(); // 64
  rb.skip(4); // 68
  ret.z0.humidifyOnTimeSeconds=rb.readShort(); // 70
  ret.z0.humidifyOffTimeSeconds=rb.readShort(); // 72
  ret.z0.dehumidifyLowTemperature=rb.readFloat(); // 76
  rb.skip(1); // 77
  rb.skip(1); // 78
  rb.showOffset("dayRampTimeMinutes");// dayRampTimeMinutes: 78 // 78
  ret.z0.dayRampTimeMinutes=rb.readShort(); // 80
  ret.z0.dayStartTimeHourMinute=rb.readShort(); // 82
  ret.z0.dayStartMode=rb.readByte(); // 83
  rb.skip(2); // 85
  ret.z0.nightEnable=rb.readBool(); // 86
  ret.z0.nightRampTimeMinutes=rb.readShort(); // 88
  ret.z0.nightStartTimeHourMinute=rb.readShort(); // 90
  ret.z0.nightStartMode=rb.readByte(); // 91
  rb.skip(2); // 93
  ret.z0.difEnable=rb.readBool(); // 94
  ret.z0.difRampTimeMinutes=rb.readShort(); // 96
  ret.z0.difStartTimeHourMinute=rb.readShort(); // 98
  ret.z0.difStartMode=rb.readByte(); // 99
  rb.skip(1); // 100
  rb.showOffset("temp_stagevals"); // 100
// temp_stagevals: 100  // 100
  rb.skip(4); //  // 104
  rb.showOffset("dayHeatSetpoint"); // 104
  ret.z0.dayHeatSetpoint=rb.readFloat() // 108
  ret.z0.dayCoolSetpoint=rb.readFloat() // 112
  rb.skip(24); //  // 136
 // 136
  rb.showOffset("nightHeatSetpoint"); // 136
  ret.z0.nightHeatSetpoint=rb.readFloat() // 140
  ret.z0.nightCoolSetpoint=rb.readFloat() // 144
  rb.skip(24); //  // 168
   // 168
  rb.showOffset("difHeatSetpoint"); // 168
  ret.z0.difHeatSetpoint=rb.readFloat() // 172
  ret.z0.difCoolSetpoint=rb.readFloat() // 176
  rb.skip(20); //  // 196
  rb.showOffset("dayHumidifySetpoint"); // 196
  ret.z0.dayHumidifySetpoint=rb.readFloat() // 200
  ret.z0.dayDehumidifySetpoint=rb.readFloat() // 204
  rb.skip(24); //  // 228
  rb.showOffset("nightHumidifySetpoint"); // 228
  ret.z0.nightHumidifySetpoint=rb.readFloat() // 232
  ret.z0.nightDehumidifySetpoint=rb.readFloat() // 236
  rb.skip(24); //  // 260
  rb.showOffset("difHumidifySetpoint"); // 260
  ret.z0.difHumidifySetpoint=rb.readFloat() // 264
  ret.z0.difDehumidifySetpoint=rb.readFloat() // 268
  rb.skip(24); //  // 292
  rb.showOffset("ipaddress"); // 292
  rb.skip(16); // 308
  for (let i = 0 ; i < cnst.NUM_EQUIPMENT ; i++){ // 308
    var hr, mn, sc; // 308
    hr = rb.readByte(); // 316
    mn = rb.readByte(); // 324
    ret.eq[i].timedOverrideStartTimeHourMinute= 60 * hr + mn; // 324
    hr = rb.readByte(); // 332
    sc = rb.readByte(); // 340
    mn = rb.readByte(); // 348
    ret.eq[i].timedOverrideDurationSeconds = 3600 * hr + 60 * mn + sc; // 348
    ret.eq[i].timedOverrideMode=rb.readByte(); // 356
    rb.skip(2); // 372
    rb.skip(4); // 404
//     $valid &= $configData->writeFloat($config['equipment'][$i]['channelType'] == 1 || $config['equipment'][$i]['channelType'] == 2 ? $config['equipment'][$i]['timedOverridePosition'] : ($config['equipment'][$i]['timedOverridePosition'] ? 1 : -1)); // 404
  } // 404
  rb.showOffset("device_Cfg"); // 404
  for (let i = 0 ; i < cnst.NUM_EQUIPMENT ; i++){ // 404
    ret.eq[i].channelType = rb.readByte(); // 412
//     rb.showOffset("type: " + ret.eq[i].channelType.toString()); // 412
    rb.skip(1); // 420
//     rb.showOffset("str 1"); // 420
    ret.eq[i].channelName = rb.readString(11); // 508
//     rb.showOffset("str 2" + ret.eq[i].channelName); // 508
    rb.skip(3); // 532
    switch(ret.eq[i].channelType){ // 532
      case 0: // 532
      case 4: // 532
      case 5: // 532
        rb.showOffset("AC"); // 532
        ret.eq[i].heat2=rb.readBool(); // 532
        ret.eq[i].heat1=rb.readBool(); // 532
        ret.eq[i].normal=rb.readBool(); // 532
        ret.eq[i].cool1=rb.readBool(); // 532
        ret.eq[i].cool2=rb.readBool(); // 532
        ret.eq[i].cool3=rb.readBool(); // 532
        ret.eq[i].cool4=rb.readBool(); // 532
        ret.eq[i].cool5=rb.readBool(); // 532
        ret.eq[i].cool6=rb.readBool(); // 532
        rb.showOffset("ACb"); // 532
        ret.eq[i].humidify=rb.readByte(); // 532
        rb.skip(1);// $valid &= $configData->writeByte(2); // 532
        ret.eq[i].dehumidify=rb.readByte(); // 532
        ret.eq[i].coldDehum=rb.readByte(); // 532
        rb.showOffset("ACca"); // 532
        if (ret.eq[i].channelType == 0) { // 532
          rb.skip(1);//$valid &= $configData->writeByte(0); // 532
          ret.eq[i].onOffType = rb.readByte(); // 532
          rb.readString(21);// $valid &= $configData->writeString("", 21); // 532
        } else { // 532
          rb.readString(23); // 532
        } // 532
        rb.showOffset("ACc"); // 532
        break; // 532
      case 6: // 532
      case 7: // 532
        rb.showOffset("CO2a"); // 532
        rb.readString(9); // 532
//         rb.showOffset("CO2b"); // 532
        rb.skip(4); // 532
//         rb.showOffset("CO2c"); // 532
        rb.readString(23); // 532
//         rb.showOffset("CO2d"); // 532
        break; // 532
      case 8: // 532
        rb.showOffset("Irrigation"); // 532
        ret.eq[i].irrigationEnable=rb.readBool(); // 532
        rb.readString(9); // 532
        rb.skip(4); // 532
        ret.eq[i].irrigationStartTimeHourMinute=rb.readShort(); // 532
        ret.eq[i].irrigationEndTimeHourMinute=rb.readShort(); // 532
        rb.readString(2); // 532
        ret.eq[i].irrigationOnTimeSeconds=rb.readInt(); // 532
        ret.eq[i].irrigationOffTimeSeconds=rb.readInt(); // 532
        rb.readString(8); // 532
        break; // 532
      case 3: // 532
        rb.showOffset("Alarm"); // 532
        rb.skip(16); // 532
        rb.readString(20); // 532
        break; // 532
      case 1: // 532
      case 2: // 532
        rb.showOffset("Alarm2"); // 532
        rb.skip(36); // 532
        break // 532
      default: // 532
        rb.showOffset("default"); // 532
        rb.skip(36); // 820
         // 820
    } // 820
     // 820
  } // 820
 // 820
  rb.showOffset("longitude"); // 820
  ret.longitude=rb.readShort(); // 822
  ret.latitude=rb.readShort(); // 824
  rb.skip(11); // 835
  ret.timeFormat=rb.readByte(); // 836
  ret.temperatureUnits=rb.readByte(); // 837
  ret.windSpeedUnits=rb.readByte(); // 838
  ret.lightUnits=rb.readByte(); // 839
  rb.showOffset("0x0354"); // 839
  rb.setOffset(0x0354); // 852
  ret.z0.sensorCalibrationInTemperature=rb.readFloat(); // 856
  ret.z0.sensorCalibrationInHumidity=rb.readFloat(); // 860
  ret.z0.sensorCalibrationOutTemperature=rb.readFloat(); // 864
  ret.z0.sensorCalibrationLight=rb.readFloat(); // 868
  rb.skip(4); // 872
  ret.z0.sensorCalibrationBackupTemperature=rb.readFloat(); // 876
  rb.skip(4); // 880
  ret.z0.sensorCalibrationOutHumidity=rb.readFloat(); // 884
  ret.z0.sensorCalibrationCO2=rb.readFloat(); // 888
  rb.showOffset("0x037F"); // 888
  rb.setOffset(0x037F); // 895
  rb.showOffset("sensorMappingInTemperature"); // 895
  ret.z0.sensorMappingInTemperature=rb.readByte(); // 896
  ret.z0.sensorMappingInHumidity=rb.readByte(); // 897
  ret.z0.sensorMappingLight=rb.readByte(); // 898
  ret.z0.sensorMappingOutTemperature=rb.readByte(); // 899
  rb.skip(2); // 901
  ret.z0.sensorMappingBackupTemperature=rb.readByte(); // 902
  ret.z0.sensorMappingOutHumidity=rb.readByte(); // 903
  ret.z0.sensorMappingCO2=rb.readByte(); // 904
  rb.skip(4); // 908
  rb.showOffset("selfTestEnable"); // 908
  ret.selfTestEnable=rb.readBool(); // 909
  rb.skip(1); // 910
  mn = rb.readByte(); // 911
  hr = rb.readByte(); // 912
  ret.selfTestTimeHourMinute= 60 * hr + mn; // 912
  ret.selfTestRetestMinutes=rb.readByte(); // 913
  rb.skip(3); // 916
  ret.selfTestTemperatureDifference=rb.readFloat(); // 920
  rb.showOffset("0x0407"); // 920
  rb.setOffset(0x0407); // 1031
  ret.usbLoggingEnable=rb.readBool(); // 1032
  ret.usbLoggingFormat=rb.readByte(); // 1033
  hr = rb.readByte(); // 1034
  mn = rb.readByte(); // 1035
  sc = rb.readByte(); // 1036
  ret.usbLoggingIntervalSeconds= 3600 * hr + 60 * mn + sc; // 1036
  rb.showOffset("0x0417"); // 1036
  rb.setOffset(0x0417); // 1047
  ret.screenPassword=rb.readString(12); // 1059
  ret.controllerName=rb.readString(16); // 1075
  rb.skip(5); // 1080
  ret.z0.temperatureHeatStages=rb.readByte(); // 1081
  ret.z0.temperatureCoolStages=rb.readByte(); // 1082
  rb.showOffset("0x05F8"); // 1082
  rb.setOffset(0x05F8); // 1528
  ret.backLightOnTime=rb.readInt()/1000; // 1532
  ret.ledLightOnTime=rb.readInt(); // 1536
  ret.outHumidityOverrideEnable=rb.readBool(); // 1537
  rb.skip(3); // 1540
  ret.outHumidifyOffset=rb.readFloat(); // 1544
  ret.outDehumidifyOffset=rb.readFloat(); // 1548
  ret.z0.sensorCO2LSB=rb.readFloat(); // 1552
  ret.z0.dayCO2Setpoint=rb.readFloat(); // 1556
  rb.skip(28); // 1584
  ret.z0.nightCO2Setpoint=rb.readFloat(); // 1588
  rb.skip(28); // 1616
  ret.z0.difCO2Setpoint=rb.readFloat(); // 1620
  rb.skip(36); // 1656
  ret.z0.co2Deadband=rb.readFloat(); // 1660
  rb.skip(4); // 1664
  ret.z0.lightSwitchByTime=rb.readByte(); // 1665
  ret.z0.lightTimeByDaySetpoint=rb.readBool(); // 1666
  ret.z0.lightOnTimeHourMinute=rb.readShort(); // 1668
  ret.z0.lightOffTimeHourMinute=rb.readShort(); // 1670
  rb.skip(2); // 1672
  ret.z0.alarmHighTemperature=rb.readFloat(); // 1676
  ret.z0.alarmLowTemperature=rb.readFloat(); // 1680
  ret.z0.alarmHighHumidity=rb.readFloat(); // 1684
  ret.z0.alarmLowHumidity=rb.readFloat(); // 1688
  ret.z0.alarmHighCO2=rb.readFloat(); // 1692
  ret.z0.alarmLowCO2=rb.readFloat(); // 1696
  rb.showOffset("AirCfg"); // 1696
  rb.skip(3); // 1699
  ret.z0.airDumpEnable=rb.readBool(); // 1700
  ret.z0.airDumpOnTimeSeconds=rb.readInt(); // 1704
  ret.z0.airDumpOffTimeSeconds=rb.readInt(); // 1708
 // 1708
  ret.dateFormat=rb.readByte(); // 1709
  rb.skip(1); // 1710
  ret.outTemperatureOverride=rb.readShort() / 10; // 1712
  ret.z0.lightSwitchBankDelay=rb.readShort(); // 1714
  ret.z0.alarmHoldTime=rb.readShort(); // 1716
  ret.z0.temperatureAlarmLightOffDelay=rb.readShort(); // 1718
  ret.z0.lightPowerCooldownTime=rb.readShort(); // 1720
  ret.z0.co2LightCutoffMode=rb.readShort(); // 1722
  ret.z0.co2LightDeadband=rb.readShort(); // 1724
  ret.z0.co2LightThreshold=rb.readShort(); // 1726
  rb.skip(2); // 1728
  ret.enableStaging=rb.readShort(); // 1730
  ret.z0.enableRamping=rb.readShort(); // 1732
  ret.z0.enableDIFSetpoint=rb.readShort(); // 1734
  ret.z0.enableNightlyCO2=rb.readShort(); // 1736
  ret.z0.enableNoSyncLight=rb.readShort(); // 1738
  ret.z0.enableExternalLight=rb.readShort(); // 1740
  rb.skip(12); // 1752
 // 1752
  ret.z0.co2FanCutEnable=rb.readBool(); // 1753
  rb.skip(3); // 1756
  ret.z0.co2CycleOnSeconds=rb.readInt(); // 1760
  ret.z0.co2CycleOffSeconds=rb.readInt(); // 1764
  rb.showOffset("CO2cycle_adj"); // 1764
  for (let i = 0 ; i < 5 ; i++){ // 1764
    ret.z0['co2FuzzyLevel' + i + 'Error']=rb.readShort(); // 1774
    rb.skip(2); // 1784
    ret.z0['co2FuzzyLevel' + i + 'Delay']=rb.readFloat(); // 1804
  } // 1804
  ret.z0.co2FuzzyEnable=rb.readBool(); // 1805
  rb.skip(3); // 1808
  ret.z0.timingAdjustMode=rb.readInt(); // 1812
  ret.z0.timingLightThreshold=rb.readShort(); // 1814
  ret.z0.timingLightDeadband=rb.readShort(); // 1816
  rb.skip(8); // 1824
  rb.showOffset("AirStartEndCfg"); // 1824
  ret.z0.airExchangeStartTime=rb.readShort(); // 1826
  ret.z0.airExchangeEndTime=rb.readShort(); // 1828
  ret.enableCO2Ramping=rb.readBool(); // 1829
  rb.skip(1); // 1830
  ret.co2DayRampMinutes=rb.readShort(); // 1832
  ret.co2DIFRampMinutes=rb.readShort(); // 1834
   // 1834
  ret.enableSyncTime=rb.readBool(); // 1835
  rb.skip(1); // 1836
  ret.timeOffset=rb.readInt(); // 1840
  ret.timeCountry=rb.readBool(); // 1841
  rb.skip(3); // 1844
   // 1844
  rb.skip(4); // 1848
  rb.showOffset("zone2"); // 1848
   // 1848
  ret.z1.temperatureStageWidth=rb.readFloat(); // 1852
  ret.z1.temperatureDeadband=rb.readFloat(); // 1856
  ret.z1.temperatureStageDelaySeconds=rb.readInt(); // 1860
  rb.skip(8) // 1868
  ret.z1.humidifyDeadband=rb.readFloat(); // 1872
  rb.skip(4) // 1876
  ret.z1.humidifyOnTimeSeconds=rb.readShort(); // 1878
  ret.z1.humidifyOffTimeSeconds=rb.readShort(); // 1880
  ret.z1.dehumidifyLowTemperature=rb.readFloat(); // 1884
  rb.skip(8) // 1892
  ret.z1.co2Deadband=rb.readFloat(); // 1896
  rb.skip(4) // 1900
  rb.skip(4) // 1904
   // 1904
  rb.showOffset("dayHeatSetpoint"); // 1904
  ret.z1.dayHeatSetpoint=rb.readFloat(); // 1908
  ret.z1.dayCoolSetpoint=rb.readFloat(); // 1912
  rb.skip(24); // 1936
  ret.z1.nightHeatSetpoint=rb.readFloat(); // 1940
  ret.z1.nightCoolSetpoint=rb.readFloat(); // 1944
  rb.skip(24); // 1968
  ret.z1.difHeatSetpoint=rb.readFloat(); // 1972
  ret.z1.difCoolSetpoint=rb.readFloat(); // 1976
  rb.skip(20); // 1996
  ret.z1.dayHumidifySetpoint=rb.readFloat(); // 2000
  ret.z1.dayDehumidifySetpoint=rb.readFloat(); // 2004
  rb.skip(24); // 2028
  ret.z1.nightHumidifySetpoint=rb.readFloat(); // 2032
  ret.z1.nightDehumidifySetpoint=rb.readFloat(); // 2036
  rb.skip(24); // 2060
  ret.z1.difHumidifySetpoint=rb.readFloat(); // 2064
  ret.z1.difDehumidifySetpoint=rb.readFloat(); // 2068
  rb.skip(24); // 2092
 // 2092
  rb.showOffset("CO2StageVal"); // 2092
  ret.z1.dayCO2Setpoint=rb.readFloat(); // 2096
  rb.skip(28) // 2124
  ret.z1.nightCO2Setpoint=rb.readFloat(); // 2128
  rb.skip(28) // 2156
  ret.z1.difCO2Setpoint=rb.readFloat(); // 2160
  rb.skip(28) // 2188
   // 2188
  ret.z1.temperatureHeatStages=rb.readByte(); // 2189
  ret.z1.temperatureCoolStages=rb.readByte(); // 2190
  rb.skip(2); // 2192
  ret.z1.dayRampTimeMinutes=rb.readShort(); // 2194
  ret.z1.dayStartTimeHourMinute=rb.readShort(); // 2196
  ret.z1.dayStartMode=rb.readByte(); // 2197
  rb.skip(2) // 2199
   // 2199
  rb.showOffset("nightEnable"); // 2199
  ret.z1.nightEnable=rb.readBool(); // 2200
  ret.z1.nightRampTimeMinutes=rb.readShort(); // 2202
  ret.z1.nightStartTimeHourMinute=rb.readShort(); // 2204
  ret.z1.nightStartMode=rb.readByte(); // 2205
  rb.skip(2) // 2207
   // 2207
  ret.z1.difEnable=rb.readBool(); // 2208
  ret.z1.difRampTimeMinutes=rb.readShort(); // 2210
  ret.z1.difStartTimeHourMinute=rb.readShort(); // 2212
  ret.z1.difStartMode=rb.readByte(); // 2213
  rb.skip(3) // 2216
   // 2216
  rb.showOffset("timingAdjustMode"); // 2216
  ret.z1.timingAdjustMode=rb.readInt(); // 2220
  ret.z1.timingLightThreshold=rb.readShort(); // 2222
  ret.z1.timingLightDeadband=rb.readShort(); // 2224
   // 2224
  ret.z1.lightSwitchByTime=rb.readByte(); // 2225
  ret.z1.lightTimeByDaySetpoint=rb.readBool(); // 2226
  ret.z1.lightOnTimeHourMinute=rb.readShort(); // 2228
  ret.z1.lightOffTimeHourMinute=rb.readShort(); // 2230
  rb.skip(2) // 2232
  rb.showOffset("co2FanCutEnable"); // 2232
   // 2232
  ret.z1.co2FanCutEnable=rb.readBool(); // 2233
  rb.skip(3) // 2236
  ret.z1.co2CycleOnSeconds=rb.readInt(); // 2240
  ret.z1.co2CycleOffSeconds=rb.readInt(); // 2244
  for(let i = 0 ; i < 5 ; i++){ // 2244
    ret.z1["co2FuzzyLevel" + i + "Error"]=rb.readShort(); // 2254
    rb.skip(2) // 2264
    ret.z1["co2FuzzyLevel" + i + "Delay"]=rb.readFloat(); // 2284
  } // 2284
  ret.z1.co2FuzzyEnable=rb.readBool(); // 2285
  rb.skip(3) // 2288
  rb.showOffset("AirCfg"); // 2288
   // 2288
   // 2288
  rb.skip(3) // 2291
  ret.z1.airDumpEnable=rb.readBool(); // 2292
  ret.z1.airDumpOnTimeSeconds=rb.readInt(); // 2296
  ret.z1.airDumpOffTimeSeconds=rb.readInt(); // 2300
   // 2300
  ret.z1.airExchangeStartTime=rb.readShort(); // 2302
  ret.z1.airExchangeEndTime=rb.readShort(); // 2304
   // 2304
  ret.z1.alarmHighTemperature=rb.readFloat(); // 2308
  ret.z1.alarmLowTemperature=rb.readFloat(); // 2312
  ret.z1.alarmHighHumidity=rb.readFloat(); // 2316
  ret.z1.alarmLowHumidity=rb.readFloat(); // 2320
  ret.z1.alarmHighCO2=rb.readFloat(); // 2324
  ret.z1.alarmLowCO2=rb.readFloat(); // 2328
   // 2328
  ret.z1.sensorCalibrationInTemperature=rb.readFloat(); // 2332
  ret.z1.sensorCalibrationInHumidity=rb.readFloat(); // 2336
  ret.z1.sensorCalibrationOutTemperature=rb.readFloat(); // 2340
  ret.z1.sensorCalibrationLight=rb.readFloat(); // 2344
  rb.skip(4) // 2348
   // 2348
  ret.z1.sensorCalibrationBackupTemperature=rb.readFloat(); // 2352
  rb.skip(4) // 2356
   // 2356
  ret.z1.sensorCalibrationOutHumidity=rb.readFloat(); // 2360
  ret.z1.sensorCalibrationCO2=rb.readFloat(); // 2364
   // 2364
  ret.z1.sensorCO2LSB=rb.readFloat(); // 2368
   // 2368
  ret.z1.sensorMappingInTemperature=rb.readByte(); // 2369
  ret.z1.sensorMappingInHumidity=rb.readByte(); // 2370
  ret.z1.sensorMappingLight=rb.readByte(); // 2371
  ret.z1.sensorMappingOutTemperature=rb.readByte(); // 2372
  rb.skip(2) // 2374
   // 2374
  ret.z1.sensorMappingBackupTemperature=rb.readByte(); // 2375
  ret.z1.sensorMappingOutHumidity=rb.readByte(); // 2376
  ret.z1.sensorMappingCO2=rb.readByte(); // 2377
  rb.skip(3) // 2380
   // 2380
  ret.z1.lightSwitchBankDelay=rb.readShort(); // 2382
  ret.z1.alarmHoldTime=rb.readShort(); // 2384
  ret.z1.temperatureAlarmLightOffDelay=rb.readShort(); // 2386
  ret.z1.lightPowerCooldownTime=rb.readShort(); // 2388
  ret.z1.co2LightCutoffMode=rb.readShort(); // 2390
  ret.z1.co2LightDeadband=rb.readShort(); // 2392
  ret.z1.co2LightThreshold=rb.readShort(); // 2394
  rb.skip(4) // 2398
   // 2398
  ret.z1.enableRamping=rb.readShort(); // 2400
  ret.z1.enableDIFSetpoint=rb.readShort(); // 2402
  ret.z1.enableNightlyCO2=rb.readShort(); // 2404
  ret.z1.enableNoSyncLight=rb.readShort(); // 2406
  ret.z1.enableExternalLight=rb.readShort(); // 2408
  rb.skip(12); // 2420
  rb.showOffset("0x09F4"); // 2420
  rb.setOffset(0x09F4); // 2548
  for (let i = 0 ; i < cnst.NUM_EQUIPMENT ; i++){ // 2548
    ret.eq[i].zoneIndex=rb.readByte(); // 2556
  } // 2556
  if (true || ret.firmwareVersion >= 0x02020009){ // 2556
    ret.dualSensorModuleBackup=rb.readBool(); // 2557
  } // 2557
  rb.showOffset("finished"); // 2557
*/
}

var procPacket=(pack)=>{
//   let pack2 = Buffer.from(pack, 'base64');
  let head = getPackHead(pack);
  cl(head);
//   cl(String.fromCharCode.apply(null, pack2.slice(0, 4)));
//   cl(head)
//   return
//
//   bString = Buffer.from(pack, 'base64').toString();
// //   cl(pack2.toJSON().data)
// //   let head = getPackHead(bString);
//   cl(head.head);
  let rb = new rawBytes(pack, head.len);
  rb.serial = rb.readSerial();
  if (!(rb.serial in keyStore)){
    keyStore[rb.serial] = {};
  }
  rb.kv = keyStore[rb.serial];
  switch(head.head){
    case 'wUserActionIP2':
      return procwUserActionIP2(rb);
    case 'rCheckIP2':
      return procrCheckIP2(rb);
    case 'wInfoIP2':
      return procwInfoIP2(rb);
    case 'wRtdIP2':
      return procwRtdIP2(rb);
    case 'rConIP2':
      return procrConIP2(rb);
    case 'wConIP2':
      return procwConIP2(rb);

    default:
      return
  }
}


var addCors = (req, res)=>{
  cl("add cors");
  utils.addCorsHeaders(res);
  res.status(200).send("ok");
}

var iponic = (req, res)=>{
  cl("iponic");
  utils.addCorsHeaders(res);
//   cl(req.body);
  req.body.forEach(p=>{
    procPacket(Buffer.from(p, 'base64'))
  });
  let ret = {result: "ok"};
  res.status(200).send(ret);
}

var setRoutes = (r)=>{
  g.app.options('/*', addCors);
  g.app.post('/iponic', iponic);
  g.app.get('/iponic', iponic);
}

var g = {
  url: utils.mqttUrl, // 'mqtt://dev.l4ol.com',


  init: ()=>{
    cl("init iponic");

//     utils.openMQTTConsumer(g.url, g.topic, g.mqttConsume).then((client)=>{
//       g.mqttClient = client;
//       cl("mqtt connected to " + g.url + ", subscribed: " + g.topic);
//     });

//     utils.openAMQP(utils.amqpUrl).then(conn=>{
//       cl("mqtt connected to AMQP at " + utils.amqpUrl);
//       utils.openAMQPChannel00(conn, "mqtt", 'topic', msgMQTT).then((ret)=>{
//         cl("mqtt listening on " + ret.queue);
//         g.chanMQTT = ret.chan;
//         g.queueMQTT = ret.queue;
//       });
//     })
    utils.openExpressServer(3375, g.onListen).then(r=>{
      r.use(bodyParser.json());
      g.app = r;
      setRoutes(r);
    });
/*ports:
 3374 authorize
 3373 logging
 3375 iponic
 3376 websocket
 */


  }


};

g.init();

module.exports = g;
