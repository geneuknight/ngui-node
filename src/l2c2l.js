var utils = require('./utils');

// import {cl, lc} from './landruConstants.js';
// import {p, pb, pInd, pi, pids, tableIds, tableBases2, offsetTables} from './paramIds.js';

var cl = (m)=>{
  let pos = 1 + __filename.lastIndexOf('/');
  let ln = utils.callStack()[1].getLineNumber()
  if (typeof m === "object"){ m = JSON.stringify(m); }
  console.log(__filename.substr(pos) + " " + ln + ": " + m);
}

var dbl = (m)=>{
  let pos = 1 + __filename.lastIndexOf('/');
  let ln = utils.callStack()[1].getLineNumber()
  if (typeof m === "object"){ m = JSON.stringify(m); }
  utils.dbl(__filename.substr(pos) + " " + ln + ": " + m);
}



var pi=require('./paramIds');
var lc=require('./landruConstants');
var l2c={};
// var sl2c={};
// var al2c={};
// var l2c={};
var c2l={};
var init=()=>{
  /*
   * type 0 - the value follows
   * type 1 [ix] is value + base
   * type 2 [ch][ix] is value + base
   */
  // for landru, assume that u=0, channel is used in IF_t, chnl_t, ecph, 
  l2c[lc.setup_t_CoolStages]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Cool_stages, 0];// type0]= use the given value
  l2c[lc.setup_t_HeatStages]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Heat_stages, 0];
  l2c[lc.setup_t_StageWidth]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Stage_Width, 0];
  // l2c[lc.setup_t_TimePeriods]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + 46, 0];
  // l2c[lc.setup_t_Zones]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + 46, 0];
  l2c[lc.setup_t_RainHoldTime]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Rain_Hold_Time, 0];
  // l2c[lc.setup_t_CmdDelayMin]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + 6, 0];
  l2c[lc.setup_t_CmdDelaySec]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Command_Delay_Time, 0];
  l2c[lc.setup_t_CoolDeadband]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Cool_Deadband, 0];
  l2c[lc.setup_t_HeatDeadband]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Heat_Deadband, 0];
  l2c[lc.setup_t_HumDeadband]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Humidity_Deadband, 0];
  
  l2c[lc.setup_t_InputMap]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS, 1, []];// type1]= look up the value in the given table
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_INTEMP]=lc.CC_Inside_Temperature_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_OUTTEMP]=lc.CC_Outside_Temperature_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_HUM]=lc.CC_Relative_Humidity_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_CO2]=lc.CC_CO2_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_LITE]=lc.CC_Outside_Light_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_WINSPD]=lc.CC_Wind_Speed_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_BHEAT1]=lc.CC_Analog_Temperature_1_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_BHEAT2]=lc.CC_Analog_Temperature_2_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_BHEAT3]=lc.CC_Analog_Temperature_3_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_BHEAT4]=lc.CC_Analog_Temperature_4_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_BHEAT5]=lc.CC_Analog_Temperature_5_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_FBINTEMP]=lc.CC_Fallback_Sensor_Input_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_WINDIR]=lc.CC_Wind_Direction_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_SOIL1]=lc.CC_Soil_Moisture_1_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_SOIL2]=lc.CC_Soil_Moisture_2_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_SOIL3]=lc.CC_Soil_Moisture_3_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_SOIL4]=lc.CC_Soil_Moisture_4_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_SOIL5]=lc.CC_Soil_Moisture_5_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_RAIN]=lc.CC_Rain_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_IRR_TRIG]=lc.CC_Irrigation_Trigger_Input_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_DIFFP]=lc.CC_Differential_Pressure_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_INLIGHT]=lc.CC_Inside_Light_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_VENTPOS1]=lc.CC_Vent_Position_Sensor_1_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_VENTPOS2]=lc.CC_Vent_Position_Sensor_2_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_VENTPOS3]=lc.CC_Vent_Position_Sensor_3_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_VENTPOS4]=lc.CC_Vent_Position_Sensor_4_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_VENTPOS5]=lc.CC_Vent_Position_Sensor_5_Mapping;
  // l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_ECPH1]=lc.CC_EC_pH_1_Mapping;
  // l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_ECPH2]=lc.CC_EC_pH_2_Mapping;
  // l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_ECPH3]=lc.CC_EC_pH_3_Mapping;
  // l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_ECPH4]=lc.CC_EC_pH_4_Mapping;
  // l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_ECPH5]=lc.CC_EC_pH_5_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_GENERIC1]=lc.CC_Generic_1_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_GENERIC2]=lc.CC_Generic_2_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_OUTHUM]=lc.CC_Outside_Humidity_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_LOCAL_INTEMP]=lc.CC_Local_Temperature_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_LOCAL_INHUM]=lc.CC_Local_Humidity_Mapping;
  
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_SNOW]=lc.CC_Snow_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_OUTTEMP2]=lc.CC_Outside_Temperature_2_Mapping;
  l2c[lc.setup_t_InputMap][2][lc.setup_t_InputMap_INPUTIDX_BARO]=lc.CC_Barometric_Pressure_Mapping;
  
  l2c[lc.setup_t_Accumulator_t_id]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS, 2, [[], []]];// type 2]= [ch][ix]
  l2c[lc.setup_t_Accumulator_t_id][2][0][0]=lc.CC_Accumulator_1_Sensor;
  l2c[lc.setup_t_Accumulator_t_id][2][0][1]=lc.CC_Accumulator_1_Operating_Period;
  l2c[lc.setup_t_Accumulator_t_id][2][1][0]=lc.CC_Accumulator_2_Sensor;
  l2c[lc.setup_t_Accumulator_t_id][2][1][1]=lc.CC_Accumulator_2_Operating_Period;
  
  l2c[lc.setup_t_DaylightSavings]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Daylight_Savings_Time, 0];
  l2c[lc.setup_t_TempUnits]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Temperature_Units, 0];
  l2c[lc.setup_t_WindUnits]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Windspeed_Units, 0];
  l2c[lc.setup_t_LiteUnits]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Light_Units, 0];
  l2c[lc.setup_t_Latitude]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Latitude, 0];
  l2c[lc.setup_t_Longitude]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Longitude, 0];
  // l2c[lc.setup_t_TimeZone]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + 46, 0];
  l2c[lc.setup_t_fb_enable]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Use_Fallback_Sensor_if_In_Temp_Fails, 0];
  l2c[lc.setup_t_fb_stage]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Fallback_Sensor_Failed_Temperature_Stage, 0];
  // l2c[lc.setup_t_Password]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + 46, 0];
  l2c[lc.setup_t_fb_enable_hi]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Force_To_No_Sensor_Stage_setting_3_if_High_Alarm_occurs, 0];
  l2c[lc.setup_t_CalInTemp]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.CC_Inside_Temperature_Calibration, 0];
  l2c[lc.setup_t_CalOutTemp]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.CC_Outside_Temperature_Calibration, 0];
  l2c[lc.setup_t_CalInHum]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.CC_Relative_Humidity_Calibration, 0];
  l2c[lc.setup_t_CalCo2]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.CC_CO2_Calibration, 0];
  l2c[lc.setup_t_CalLite]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.CC_Outside_Light_Calibration, 0];
  l2c[lc.setup_t_CalWind]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.CC_Wind_Speed_Calibration, 0];
  l2c[lc.setup_t_CalWindDir]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.CC_Wind_Direction_Calibration, 0];
  
  l2c[lc.setup_t_CalBHeat]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS, 1, []];
  l2c[lc.setup_t_CalBHeat][2][0]=lc.CC_Analog_Temperature_1_Calibration;
  l2c[lc.setup_t_CalBHeat][2][1]=lc.CC_Analog_Temperature_2_Calibration;
  l2c[lc.setup_t_CalBHeat][2][2]=lc.CC_Analog_Temperature_3_Calibration;
  l2c[lc.setup_t_CalBHeat][2][3]=lc.CC_Analog_Temperature_4_Calibration;
  l2c[lc.setup_t_CalBHeat][2][4]=lc.CC_Analog_Temperature_5_Calibration;
  
  l2c[lc.setup_t_CalSoil]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS, 1, []];
  l2c[lc.setup_t_CalSoil][2][0]=lc.CC_Soil_Mositure_1_Calibration;
  l2c[lc.setup_t_CalSoil][2][1]=lc.CC_Soil_Mositure_2_Calibration;
  l2c[lc.setup_t_CalSoil][2][2]=lc.CC_Soil_Mositure_3_Calibration;
  l2c[lc.setup_t_CalSoil][2][3]=lc.CC_Soil_Mositure_4_Calibration;
  l2c[lc.setup_t_CalSoil][2][4]=lc.CC_Soil_Mositure_5_Calibration;
  
  l2c[lc.setup_t_CalFallBack]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.CC_Fallback_Temperature_Calibration, 0];
  
  l2c[lc.setup_t_CalVOpen]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS, 1, []];
  l2c[lc.setup_t_CalVOpen][2][0]=lc.CC_Vent_Position_1_Calibration_Open;
  l2c[lc.setup_t_CalVOpen][2][1]=lc.CC_Vent_Position_2_Calibration_Open;
  l2c[lc.setup_t_CalVOpen][2][2]=lc.CC_Vent_Position_3_Calibration_Open;
  l2c[lc.setup_t_CalVOpen][2][3]=lc.CC_Vent_Position_4_Calibration_Open;
  l2c[lc.setup_t_CalVOpen][2][4]=lc.CC_Vent_Position_5_Calibration_Open;
  
  l2c[lc.setup_t_CalVClose]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS, 1, []];
  l2c[lc.setup_t_CalVClose][2][0]=lc.CC_Vent_Position_1_Calibration_Close;
  l2c[lc.setup_t_CalVClose][2][1]=lc.CC_Vent_Position_2_Calibration_Close;
  l2c[lc.setup_t_CalVClose][2][2]=lc.CC_Vent_Position_3_Calibration_Close;
  l2c[lc.setup_t_CalVClose][2][3]=lc.CC_Vent_Position_4_Calibration_Close;
  l2c[lc.setup_t_CalVClose][2][4]=lc.CC_Vent_Position_5_Calibration_Close;
  
  l2c[lc.setup_t_CalVPos]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS, 1, []];
  l2c[lc.setup_t_CalVPos][2][0]=lc.CC_Vent_Position_Sensor_1_Calibration_Adjust;
  l2c[lc.setup_t_CalVPos][2][1]=lc.CC_Vent_Position_Sensor_2_Calibration_Adjust;
  l2c[lc.setup_t_CalVPos][2][2]=lc.CC_Vent_Position_Sensor_3_Calibration_Adjust;
  l2c[lc.setup_t_CalVPos][2][3]=lc.CC_Vent_Position_Sensor_4_Calibration_Adjust;
  l2c[lc.setup_t_CalVPos][2][4]=lc.CC_Vent_Position_Sensor_5_Calibration_Adjust;
  
  l2c[lc.setup_t_CalDiffP]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.CC_Differential_Pressure_Calibration, 0];
  l2c[lc.setup_t_CalInLight]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.CC_Inside_Light_Calibration, 0];
  l2c[lc.setup_t_IrrCntl]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Irrigation_Mode, 0];
  // l2c[lc.setup_t_IrrDelayMin]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS, 0];
  l2c[lc.setup_t_IrrDelaySec]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Irrigation_Delay, 0];
  // l2c[lc.setup_t_hist_hh]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS, 0];
  l2c[lc.setup_t_hist_mm]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Log_History, 0];
  // l2c[lc.setup_t_comm_mode]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.ZZZZ, 0];
  l2c[lc.setup_t_zoneID]=-1;// not used [pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.ZZZZ, 0];
  
  l2c[lc.setup_t_BHeatDeadband]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Analog_Temperature_Deadband, 0];
  // l2c[lc.setup_t_EnableAdvanced]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.ZZZZ, 0];
  l2c[lc.setup_t_remoteSetp]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.CC_Local_Remote_Setpoints, 0];
  l2c[lc.setup_t_eq_cycle_sec]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Equipment_Delay, 0];
  l2c[lc.setup_t_EnergyLightHyst]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Curtain_Energy_Mode_Light_Deadband, 0];
  l2c[lc.setup_t_EnergyTempHyst]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Curtain_Energy_Mode_Temperature_Deadband, 0];
  l2c[lc.setup_t_CO2_LSB]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.CC_CO2_Least_Significant_Bit, 0];
  l2c[lc.setup_t_chnl_addr]=-1//[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.ZZZZ, 0];
  l2c[lc.setup_t_smart_cool_enable]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Enable_SmartCool, 0];
  l2c[lc.setup_t_smart_cool_number]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_SmartCool_Setting, 0];
  
  l2c[lc.setup_t_sc_t_KU]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS, 1, []];
  l2c[lc.setup_t_sc_t_KU][2][0]=lc.ZC_SmartCool_0_Ku;
  l2c[lc.setup_t_sc_t_KU][2][1]=lc.ZC_SmartCool_1_Ku;
  l2c[lc.setup_t_sc_t_KU][2][2]=lc.ZC_SmartCool_2_Ku;
  l2c[lc.setup_t_sc_t_KU][2][3]=lc.ZC_SmartCool_3_Ku;
  l2c[lc.setup_t_sc_t_KU][2][4]=lc.ZC_SmartCool_4_Ku;
  l2c[lc.setup_t_sc_t_KU][2][5]=lc.ZC_SmartCool_5_Ku;
  l2c[lc.setup_t_sc_t_KU][2][6]=lc.ZC_SmartCool_6_Ku;
  l2c[lc.setup_t_sc_t_KU][2][7]=lc.ZC_SmartCool_7_Ku;
  l2c[lc.setup_t_sc_t_KU][2][8]=lc.ZC_SmartCool_8_Ku;
  l2c[lc.setup_t_sc_t_KU][2][9]=lc.ZC_SmartCool_9_Ku;
  
  l2c[lc.setup_t_sc_t_Gmax]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS, 1, []];
  l2c[lc.setup_t_sc_t_Gmax][2][0]=lc.ZC_SmartCool_0_Gmax;
  l2c[lc.setup_t_sc_t_Gmax][2][1]=lc.ZC_SmartCool_1_Gmax;
  l2c[lc.setup_t_sc_t_Gmax][2][2]=lc.ZC_SmartCool_2_Gmax;
  l2c[lc.setup_t_sc_t_Gmax][2][3]=lc.ZC_SmartCool_3_Gmax;
  l2c[lc.setup_t_sc_t_Gmax][2][4]=lc.ZC_SmartCool_4_Gmax;
  l2c[lc.setup_t_sc_t_Gmax][2][5]=lc.ZC_SmartCool_5_Gmax;
  l2c[lc.setup_t_sc_t_Gmax][2][6]=lc.ZC_SmartCool_6_Gmax;
  l2c[lc.setup_t_sc_t_Gmax][2][7]=lc.ZC_SmartCool_7_Gmax;
  l2c[lc.setup_t_sc_t_Gmax][2][8]=lc.ZC_SmartCool_8_Gmax;
  l2c[lc.setup_t_sc_t_Gmax][2][9]=lc.ZC_SmartCool_9_Gmax;
  
  l2c[lc.setup_t_sc_t_Kd]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS, 1, []];
  l2c[lc.setup_t_sc_t_Kd][2][0]=lc.ZC_SmartCool_0_Kd;
  l2c[lc.setup_t_sc_t_Kd][2][1]=lc.ZC_SmartCool_1_Kd;
  l2c[lc.setup_t_sc_t_Kd][2][2]=lc.ZC_SmartCool_2_Kd;
  l2c[lc.setup_t_sc_t_Kd][2][3]=lc.ZC_SmartCool_3_Kd;
  l2c[lc.setup_t_sc_t_Kd][2][4]=lc.ZC_SmartCool_4_Kd;
  l2c[lc.setup_t_sc_t_Kd][2][5]=lc.ZC_SmartCool_5_Kd;
  l2c[lc.setup_t_sc_t_Kd][2][6]=lc.ZC_SmartCool_6_Kd;
  l2c[lc.setup_t_sc_t_Kd][2][7]=lc.ZC_SmartCool_7_Kd;
  l2c[lc.setup_t_sc_t_Kd][2][8]=lc.ZC_SmartCool_8_Kd;
  l2c[lc.setup_t_sc_t_Kd][2][9]=lc.ZC_SmartCool_9_Kd;
  
  l2c[lc.setup_t_sc_t_Gmin]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS, 1, []];
  l2c[lc.setup_t_sc_t_Gmin][2][0]=lc.ZC_SmartCool_0_Gmin;
  l2c[lc.setup_t_sc_t_Gmin][2][1]=lc.ZC_SmartCool_1_Gmin;
  l2c[lc.setup_t_sc_t_Gmin][2][2]=lc.ZC_SmartCool_2_Gmin;
  l2c[lc.setup_t_sc_t_Gmin][2][3]=lc.ZC_SmartCool_3_Gmin;
  l2c[lc.setup_t_sc_t_Gmin][2][4]=lc.ZC_SmartCool_4_Gmin;
  l2c[lc.setup_t_sc_t_Gmin][2][5]=lc.ZC_SmartCool_5_Gmin;
  l2c[lc.setup_t_sc_t_Gmin][2][6]=lc.ZC_SmartCool_6_Gmin;
  l2c[lc.setup_t_sc_t_Gmin][2][7]=lc.ZC_SmartCool_7_Gmin;
  l2c[lc.setup_t_sc_t_Gmin][2][8]=lc.ZC_SmartCool_8_Gmin;
  l2c[lc.setup_t_sc_t_Gmin][2][9]=lc.ZC_SmartCool_9_Gmin;
  
  l2c[lc.setup_t_exbFlg]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.CC_Enable_expansion_board, 0];// bit flags - needs to be broken up
  
  l2c[lc.setup_t_htdDT]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Greenhouse_heating_design_delta_T, 0];
  l2c[lc.setup_t_htdMaxLight]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Heat_Demand_Max_Light, 0];
  l2c[lc.setup_t_htdMaxWind]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Heat_Demand_Max_Wind, 0];
  l2c[lc.setup_t_htdLightFactor]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Light_s_influence_on_Heat_Demand, 0];
  l2c[lc.setup_t_htdWindFactor]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Wind_s_influence_on_Heat_Demand, 0];
  
  l2c[lc.setup_t_htdShow]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Show_Heat_Demand_on_iGrow, 0];
  
  l2c[lc.setup_t_cldDT]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Greenhouse_cooling_design_delta_T, 0];
  l2c[lc.setup_t_cldMaxLight]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Cool_Demand_Max_Light, 0];
  l2c[lc.setup_t_cldLightFactor]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Cool_Demand_Light_Factor, 0];
  
  l2c[lc.setup_t_cldActiveSet]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Enter_Active_Cooling_Cool_Demand_Threshold, 0];
  l2c[lc.setup_t_cldPassiveSet]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Exit_Active_Cooling_Cool_Demand_Threshold, 0];
  l2c[lc.setup_t_cldEnableActive]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Enable_Active_Cooling, 0];
  l2c[lc.setup_t_cldPassiveLockStg]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Passive_Lock_Stage, 0];
  l2c[lc.setup_t_cldShow]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Show_Cool_Demand_on_iGrow, 0];
  
  l2c[lc.setup_t_bump]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.CC_Enable_Bump_Vents, 0];
  
  l2c[lc.setup_t_AutoDetectMode]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.CC_Autodetect_Mode, 0];
  l2c[lc.setup_t_htdOffset]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.CC_Heat_Demand_Offset, 0];
  l2c[lc.setup_t_cldOffset]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.CC_Cool_Demand_Offset, 0];
  l2c[lc.setup_t_sns_wait_cnt]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.CC_Sensor_Delay, 0];
  l2c[lc.setup_t_HaltOnVentCurt]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.CC_Enable_Loud_Vent, 0];
  
  l2c[lc.setup_t_tempSpikeThresh]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.CC_Spike_Temperature_Delta, 0];
  // l2c[lc.setup_t_tempSpikeMin]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.ZC_Analog_Temperature_Deadband, 0];
  l2c[lc.setup_t_tempSpikeSec]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.CC_Spike_Temperature_Hold_Time_Exception, 0];
  l2c[lc.setup_t_lightMultipler]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.CC_Light_Multiplier, 0];
  
  l2c[lc.setup_t_genericMult]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS, 1, []];
  l2c[lc.setup_t_genericMult][2][0]= lc.CC_Generic_1_Multiplier;
  l2c[lc.setup_t_genericMult][2][1]= lc.CC_Generic_2_Multiplier;
  
  l2c[lc.setup_t_genericCal]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS, 1, []];
  l2c[lc.setup_t_genericCal][2][0]= lc.CC_Generic_1_Calibration;
  l2c[lc.setup_t_genericCal][2][1]= lc.CC_Generic_2_Calibration;
  
  l2c[lc.setup_t_genericUnits]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS, 1, []];
  l2c[lc.setup_t_genericUnits][2][0]= lc.CC_Generic_Sensor_1_Units;
  l2c[lc.setup_t_genericUnits][2][1]= lc.CC_Generic_Sensor_2_Units;
  
  l2c[lc.setup_t_genericRange]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS, 1, []];
  l2c[lc.setup_t_genericRange][2][0]= lc.CC_Generic_Sensor_1_Range;
  l2c[lc.setup_t_genericRange][2][1]= lc.CC_Generic_Sensor_2_Range;
  
  l2c[lc.setup_t_pidMultiplier]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS, 1, []];
  l2c[lc.setup_t_pidMultiplier][2][0]= lc.CC_Generic_PID_Multipliers_P;
  l2c[lc.setup_t_pidMultiplier][2][1]= lc.CC_Generic_PID_Multipliers_I;
  l2c[lc.setup_t_pidMultiplier][2][2]= lc.CC_Generic_PID_Multipliers_D;
  
  l2c[lc.setup_t_filter_cfg_t_max_samples]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS, 1, []];
  l2c[lc.setup_t_filter_cfg_t_max_samples][2][0]= lc.CC_Generic_Sensor_1_Filter_Max_Samples;
  l2c[lc.setup_t_filter_cfg_t_max_samples][2][1]= lc.CC_Generic_Sensor_2_Filter_Max_Samples;
  
  l2c[lc.setup_t_CalOutHum]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.CC_Outside_Humidity_Calibration, 0];
  l2c[lc.setup_t_d2avg_enable]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Drive_to_Average_Enable, 0];
  l2c[lc.setup_t_d2avg_target]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Drive_to_Average_Target_Temperature, 0];
  l2c[lc.setup_t_d2avg_maxFailDays]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Drive_to_Average_Maximum_Failed_Days, 0];
  l2c[lc.setup_t_d2avg_successTemp]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Drive_to_Average_Setpoint_Correction, 0];
  l2c[lc.setup_t_d2avg_maxTempDiff]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Drive_to_Average_Deviated_Temperature_Threshold, 0];
  l2c[lc.setup_t_tmInput]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Vapor_Pressure_Deficit_Media_Sensor, 0];
  l2c[lc.setup_t_hidCntl]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Lighting_Cyclic_Mode, 0];
  l2c[lc.setup_t_hidInitPauseMin]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Lighting_Start_Delay, 0];
  l2c[lc.setup_t_hidActiveMin]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Lighting_Active_Time, 0];
  l2c[lc.setup_t_hidFinalPauseMin]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Lighting_Finish_Delay, 0];
  l2c[lc.setup_t_hidMinOnMin]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Lighting_Minimum_On_Time, 0];
  
  // l2c[lc.setup_t_InfluenceFactor_id]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS, 0];
  // l2c[lc.setup_t_InfluenceFactor_enable]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS, 2, [[], []]];
  // l2c[lc.setup_t_InfluenceFactor_enable][2][0][0]=lc.ZC_Heat_Setpoint_Influence_Factor_Enable;
  
  l2c[lc.setup_t_InfluenceFactor_TempSPOffset]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS, 2, [[], []]];
  l2c[lc.setup_t_InfluenceFactor_TempSPOffset][2][0][0]=lc.ZC_Heat_Setpoint_Influence_Factor_Upper_Offset;
  l2c[lc.setup_t_InfluenceFactor_TempSPOffset][2][0][1]=lc.ZC_Heat_Setpoint_Influence_Factor_Lower_Offset;
  // l2c[lc.setup_t_InfluenceFactor_TempSPOffset]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS, 2, [[], []]];
  l2c[lc.setup_t_InfluenceFactor_TempSPOffset][2][1][0]=lc.ZC_Cool_Setpoint_Influence_Factor_Upper_Offset;
  l2c[lc.setup_t_InfluenceFactor_TempSPOffset][2][1][1]=lc.ZC_Cool_Setpoint_Influence_Factor_Lower_Offset;
  
  l2c[lc.setup_t_InfluenceFactor_AC]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS, 2, [[], []]];// like 2, but offset *2, for zone, cont config
  l2c[lc.setup_t_InfluenceFactor_AC][2][0][0]=lc.ZC_Heat_Setpoint_Influence_Factor_Upper_Threshold;
  l2c[lc.setup_t_InfluenceFactor_AC][2][0][1]=lc.ZC_Heat_Setpoint_Influence_Factor_Lower_Threshold;
  l2c[lc.setup_t_InfluenceFactor_AC][2][1][0]=lc.ZC_Cool_Setpoint_Influence_Factor_Upper_Threshold;
  l2c[lc.setup_t_InfluenceFactor_AC][2][1][1]=lc.ZC_Cool_Setpoint_Influence_Factor_Lower_Threshold;
  
  // l2c[lc.setup_t_InfluenceFactor_AC_type]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS, 2, [[], []]];
  // l2c[lc.setup_t_InfluenceFactor_AC_type][2][0][0]=lc.ZC_Heat_Setpoint_Influence_Factor_Sensor;
  
  
  l2c[lc.setup_t_InfluenceFactor_enable]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS, 2, [[], []]];
  l2c[lc.setup_t_InfluenceFactor_enable][2][0][0]=lc.ZC_Heat_Setpoint_Influence_Factor_Enable;
  l2c[lc.setup_t_InfluenceFactor_enable][2][1][0]=lc.ZC_Cool_Setpoint_Influence_Factor_Enable;
  
  
  // l2c[lc.setup_t_InfluenceFactor_AC]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS, 2, [[], []]];
  
  
  l2c[lc.setup_t_InfluenceFactor_AC_type]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS, 2, [[], []]];
  l2c[lc.setup_t_InfluenceFactor_AC_type][2][0][0]=lc.ZC_Heat_Setpoint_Influence_Factor_Sensor;
  l2c[lc.setup_t_InfluenceFactor_AC_type][2][1][0]=lc.ZC_Cool_Setpoint_Influence_Factor_Sensor;
  
  l2c[lc.setup_t_ZoneDelaySec]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Start_Up_Delay, 0];
  // l2c[lc.setup_t_ZoneDelayMin]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Lighting_Minimum_On_Time, 0];
  l2c[lc.setup_t_SetupFlags]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Enable_Demands_Based_on_Active_Cool, 0];// 100, too
  
  l2c[lc.setup_t_bOutExpEnable]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.CC_Enable_Expansion_Board, 0];
  // l2c[lc.setup_t_bOutExpAutoDetect]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Lighting_Minimum_On_Time, 0];
  // l2c[lc.setup_t_Unused]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Lighting_Minimum_On_Time, 0];
  l2c[lc.setup_t_nutrientsUnits]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Nutrient_Units, 0];
  l2c[lc.setup_t_tdsConversionFactor]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Nutrient_Units_TDS_Conversion_Factor, 0];
  l2c[lc.setup_t_peristalticCntl]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Peristaltic_Output_Mode, 0];
  // l2c[lc.setup_t_peristalticDelayMin]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Lighting_Minimum_On_Time, 0];
  
  l2c[lc.setup_t_peristalticDelaySec]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Peristaltic_Output_Delay, 0];
  l2c[lc.setup_t_volumeUnits]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Volume_Measurement_Units, 0];
  l2c[lc.setup_t_inLightMultiplier]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.CC_Inside_Light_Multiplier, 0];
  
  l2c[lc.setup_t_advTrigger]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Peristaltic_Pump_Advanced_Mode, 0];
  l2c[lc.setup_t_CalRain]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.CC_Rain_Calibration, 0];
  l2c[lc.setup_t_CalSnow]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.CC_Snow_Calibration, 0];
  l2c[lc.setup_t_CalOutTemp2]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.CC_Outside_Temperature_2_Calibration, 0];
  l2c[lc.setup_t_CalPressure]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.CC_Barometric_Pressure_Calibration, 0];
  // l2c[lc.setup_t_press_LSB]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.ZC_Lighting_Minimum_On_Time, 0];
  // l2c[lc.setup_t_initVentClosed]=[pi.p.PID_BASE_CONFIG_CONTROLLER_SETTINGS + lc.ZC_Lighting_Minimum_On_Time, 0];
  
  l2c[lc.configPartial_t_ovr_t_enable]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS, 1, []];
  l2c[lc.configPartial_t_ovr_t_enable][2][0]=lc.ZC_Stage_Override_1_Enabled;
  l2c[lc.configPartial_t_ovr_t_enable][2][1]=lc.ZC_Stage_Override_2_Enabled;
  l2c[lc.configPartial_t_ovr_t_enable][2][2]=lc.ZC_Stage_Override_3_Enabled;
  l2c[lc.configPartial_t_ovr_t_enable][2][3]=lc.ZC_Stage_Override_4_Enabled;
  
  l2c[lc.configPartial_t_ovr_t_force]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS, 1, []];
  l2c[lc.configPartial_t_ovr_t_force][2][0]=lc.ZC_Stage_Override_1_Temperature_Stage;
  l2c[lc.configPartial_t_ovr_t_force][2][1]=lc.ZC_Stage_Override_2_Temperature_Stage;
  l2c[lc.configPartial_t_ovr_t_force][2][2]=lc.ZC_Stage_Override_3_Temperature_Stage;
  l2c[lc.configPartial_t_ovr_t_force][2][3]=lc.ZC_Stage_Override_4_Temperature_Stage;
  
  l2c[lc.configPartial_t_ovr_t_start]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS, 1, []];
  l2c[lc.configPartial_t_ovr_t_start][2][0]=lc.ZC_Stage_Override_1_Start_Time;
  l2c[lc.configPartial_t_ovr_t_start][2][1]=lc.ZC_Stage_Override_2_Start_Time;
  l2c[lc.configPartial_t_ovr_t_start][2][2]=lc.ZC_Stage_Override_3_Start_Time;
  l2c[lc.configPartial_t_ovr_t_start][2][3]=lc.ZC_Stage_Override_4_Start_Time;
  
  l2c[lc.configPartial_t_ovr_t_end]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS, 1, []];
  l2c[lc.configPartial_t_ovr_t_end][2][0]=lc.ZC_Stage_Override_1_End_Time;
  l2c[lc.configPartial_t_ovr_t_end][2][1]=lc.ZC_Stage_Override_2_End_Time;
  l2c[lc.configPartial_t_ovr_t_end][2][2]=lc.ZC_Stage_Override_3_End_Time;
  l2c[lc.configPartial_t_ovr_t_end][2][3]=lc.ZC_Stage_Override_4_End_Time;
  
  l2c[lc.configPartial_t_ovr_t_sched]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS, 1, []];
  l2c[lc.configPartial_t_ovr_t_sched][2][0]=lc.ZC_Stage_Override_1_Interval;
  l2c[lc.configPartial_t_ovr_t_sched][2][1]=lc.ZC_Stage_Override_2_Interval;
  l2c[lc.configPartial_t_ovr_t_sched][2][2]=lc.ZC_Stage_Override_3_Interval;
  l2c[lc.configPartial_t_ovr_t_sched][2][3]=lc.ZC_Stage_Override_4_Interval;
  
  l2c[lc.configPartial_t_alarm_t_hitempoffset]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_High_Alarm_Temperature_Above_Cool_Setpoint_Threshold, 0];
  l2c[lc.configPartial_t_alarm_t_hitemptime]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_High_Alarm_Temperature_Hold_Time, 0];
  l2c[lc.configPartial_t_alarm_t_lotempoffset]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Low_Alarm_Temperature_Below_Heat_Setpoint_Threshold, 0];
  l2c[lc.configPartial_t_alarm_t_lotemptime]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Low_Alarm_Temperature_Hold_Time, 0];
  // l2c[lc.configPartial_t_humdehum_t_dh_maxon_min]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.SP_humidifySetpoint, 0];
  // l2c[lc.configPartial_t_humdehum_t_dh_minoff_min]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.SP_humidifySetpoint, 0];
  l2c[lc.configPartial_t_humdehum_t_heat_boost]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Dehumidify_Heat_Boost, 0];
  l2c[lc.configPartial_t_humdehum_t_dhlt_thresh]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Dehumidify_Low_Outside_Temperature_Threshold, 0];
  l2c[lc.configPartial_t_humdehum_t_dh_maxon_sec]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Humidity_Override_On_Time, 0];
  l2c[lc.configPartial_t_humdehum_t_dh_minoff_sec]=[pi.p.PID_BASE_CONFIG_ZONE_SETTINGS + lc.ZC_Humidity_Override_Off_Time, 0];
  
  l2c[lc.xboard_t_numOutputs]=[pi.p.PID_BASE_CONFIG_EXPANSION_BOARDS + lc.CEB_numOutputs, 3, 5];// numOutputs not in db
  l2c[lc.xboard_t_type]=[pi.p.PID_BASE_CONFIG_EXPANSION_BOARDS + lc.CEB_boardType, 3, 5];
  l2c[lc.xboard_t_addr]=[pi.p.PID_BASE_CONFIG_EXPANSION_BOARDS + lc.CEB_address, 3, 5];
  l2c[lc.xboard_t_startOutput]=[pi.p.PID_BASE_CONFIG_EXPANSION_BOARDS + lc.CEB_startChannelIndex, 3, 5];
  
  // these are doubly indexed: ch for tank, and ix for 1 of 3 sensors
  l2c[lc.ecphConfig_t_ecphSensor_t_ecMapping]=[pi.p.PID_BASE_CONFIG_ECPH_SENSORS + lc.CES_ecMapping, 8, 23];
  l2c[lc.ecphConfig_t_ecphSensor_t_pHMapping]=[pi.p.PID_BASE_CONFIG_ECPH_SENSORS + lc.CES_phMapping, 8, 23];
  
  l2c[lc.ecphConfig_t_ecphSensor_t_tempMapping]=[pi.p.PID_BASE_CONFIG_ECPH_SENSORS + lc.CES_temperatureMapping, 8, 23];
  l2c[lc.ecphConfig_t_ecphSensor_t_tempCompensationMode]=[pi.p.PID_BASE_CONFIG_ECPH_SENSORS + lc.CES_temperatureCompensationMode, 8, 23];
  l2c[lc.ecphConfig_t_ecphSensor_t_tempCompensationValx10]=[pi.p.PID_BASE_CONFIG_ECPH_SENSORS + lc.CES_temperatureCompensationValue, 8, 23];
  
  l2c[lc.ecphConfig_t_ecphSensor_t_ecServiceIntervalDays]=[pi.p.PID_BASE_CONFIG_ECPH_SENSORS + lc.CES_ecServiceIntervalDays, 8, 23];
  l2c[lc.ecphConfig_t_ecphSensor_t_ecCalibrationIntervalDays]=[pi.p.PID_BASE_CONFIG_ECPH_SENSORS + lc.CES_ecCalibrationIntervalDays, 8, 23];
  l2c[lc.ecphConfig_t_ecphSensor_t_ecServiceTime]=[pi.p.PID_BASE_CONFIG_ECPH_SENSORS + lc.CES_ecServiceTime, 8, 23];
  l2c[lc.ecphConfig_t_ecphSensor_t_ecCalibrationTime]=[pi.p.PID_BASE_CONFIG_ECPH_SENSORS + lc.CES_ecCalibrationTime, 8, 23];
  
  l2c[lc.ecphConfig_t_ecphSensor_t_phServiceIntervalDays]=[pi.p.PID_BASE_CONFIG_ECPH_SENSORS + lc.CES_phServiceIntervalDays, 8, 23];
  l2c[lc.ecphConfig_t_ecphSensor_t_phCalibrationIntervalDays]=[pi.p.PID_BASE_CONFIG_ECPH_SENSORS + lc.CES_phCalibrationIntervalDays, 8, 23];
  l2c[lc.ecphConfig_t_ecphSensor_t_phServiceTime]=[pi.p.PID_BASE_CONFIG_ECPH_SENSORS + lc.CES_phServiceTime, 8, 23];
  l2c[lc.ecphConfig_t_ecphSensor_t_phCalibrationTime]=[pi.p.PID_BASE_CONFIG_ECPH_SENSORS + lc.CES_phCalibrationTime, 8, 23];
  
  l2c[lc.ecphConfig_t_ecphSensor_t_calECRef1Val]=[pi.p.PID_BASE_CONFIG_ECPH_SENSORS + lc.CES_ecCalibration1Value, 8, 23];
  l2c[lc.ecphConfig_t_ecphSensor_t_calECRef1Readingx10]=[pi.p.PID_BASE_CONFIG_ECPH_SENSORS + lc.CES_ecCalibration1Raw, 8, 23];
  l2c[lc.ecphConfig_t_ecphSensor_t_calECRef2Val]=[pi.p.PID_BASE_CONFIG_ECPH_SENSORS + lc.CES_ecCalibration2Value, 8, 23];
  l2c[lc.ecphConfig_t_ecphSensor_t_calECRef2Readingx10]=[pi.p.PID_BASE_CONFIG_ECPH_SENSORS + lc.CES_ecCalibration2Raw, 8, 23];
  
  l2c[lc.ecphConfig_t_ecphSensor_t_calPHRef1Valx10]=[pi.p.PID_BASE_CONFIG_ECPH_SENSORS + lc.CES_phCalibration1Value, 8, 23];
  l2c[lc.ecphConfig_t_ecphSensor_t_calPHRef1Readingx100]=[pi.p.PID_BASE_CONFIG_ECPH_SENSORS + lc.CES_phCalibration1Raw, 8, 23];
  l2c[lc.ecphConfig_t_ecphSensor_t_calPHRef2Valx10]=[pi.p.PID_BASE_CONFIG_ECPH_SENSORS + lc.CES_phCalibration2Value, 8, 23];
  l2c[lc.ecphConfig_t_ecphSensor_t_calPHRef2Readingx100]=[pi.p.PID_BASE_CONFIG_ECPH_SENSORS + lc.CES_phCalibration2Raw, 8, 23];
  l2c[lc.ecphConfig_t_ecphSensor_t_calTempx10]=[pi.p.PID_BASE_CONFIG_ECPH_SENSORS + lc.CES_temperatureCalibration, 8, 23];
  
  l2c[lc.ecphConfig_t_ecType]=[pi.p.PID_BASE_CONFIG_ECPH + lc.CE_ecType, 9];
  l2c[lc.ecphConfig_t_alarmHoldTimeMinutes]=[pi.p.PID_BASE_CONFIG_ECPH + lc.CE_alarmHoldTime, 9];
  l2c[lc.ecphConfig_t_lowECThreshold]=[pi.p.PID_BASE_CONFIG_ECPH + lc.CE_lowECThreshold, 9];
  l2c[lc.ecphConfig_t_highECThreshold]=[pi.p.PID_BASE_CONFIG_ECPH + lc.CE_highECThreshold, 9];
  l2c[lc.ecphConfig_t_lowPHThresholdx10]=[pi.p.PID_BASE_CONFIG_ECPH + lc.CE_lowPHThreshold, 9];
  l2c[lc.ecphConfig_t_highPHThresholdx10]=[pi.p.PID_BASE_CONFIG_ECPH + lc.CE_highPHThreshold, 9];
  l2c[lc.ecphConfig_t_maxECDevThreshold]=[pi.p.PID_BASE_CONFIG_ECPH + lc.CE_highECDeviationThreshold, 9];
  l2c[lc.ecphConfig_t_maxPHDevThresholdx10]=[pi.p.PID_BASE_CONFIG_ECPH + lc.CE_highPHDeviationThreshold, 9];
  // l2c[lc.ecphConfig_t_tempECErrorCoefficient]=[pi.p.PID_BASE_CONFIG_ECPH + lc.CES_temperatureCalibration, 8, 23];
  
  // _fields_ = [('type', uint8),
  // ('addr', uint8),
  // ('numOutputs', uint8),
  // ('startOutput', uint8),
  // ('pad', uint8 * 8),
  // 
  // CEB_boardIndex: 0, // 64
  // CEB_numOutputs: 1,
  // CEB_boardType: 2,
  // CEB_address: 3, addr
  // CEB_startChannelIndex: 4,- start output
  
  
  
  
  // type3]= multiply ix by following coefficient, and add to base
  l2c[lc.setpoint_t_enable]=[pi.p.PID_BASE_CONFIG_SETPOINTS + lc.SP_enabled, 3, 10];
  l2c[lc.setpoint_t_startmode]=[pi.p.PID_BASE_CONFIG_SETPOINTS + lc.SP_astroAdjust, 3, 10];
  
  l2c[lc.setpoint_t_starttime]=[pi.p.PID_BASE_CONFIG_SETPOINTS + lc.SP_startTimeOfDay, 3, 10];
  
  l2c[lc.setpoint_t_ramp]=[pi.p.PID_BASE_CONFIG_SETPOINTS + lc.SP_rampMinutes, 3, 10];// ramp minutes = 5
  l2c[lc.setpoint_t_cool]=[pi.p.PID_BASE_CONFIG_SETPOINTS + lc.SP_coolSetpoint, 3, 10];
  l2c[lc.setpoint_t_heat]=[pi.p.PID_BASE_CONFIG_SETPOINTS + lc.SP_heatSetpoint, 3, 10];
  l2c[lc.setpoint_t_dehum]=[pi.p.PID_BASE_CONFIG_SETPOINTS + lc.SP_dehumidifySetpoint, 3, 10];
  l2c[lc.setpoint_t_hum]=[pi.p.PID_BASE_CONFIG_SETPOINTS + lc.SP_humidifySetpoint, 3, 10];
  
  // type6, no indexing, but ch gets copied to cloud channel
  l2c[lc.chnl_t_irr_t_mode]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_irrigation_mode, 0];
  l2c[lc.chnl_t_irr_t_on_ss]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_on_duration, 0];
  
  // type7 use ix to choose an offset
  l2c[lc.chnl_t_irr_t_scheduledTime]=[pi.p.PID_BASE_CONF_CHAN_DATA, 1, []];
  l2c[lc.chnl_t_irr_t_scheduledTime][2][0]=lc.CD_start_time_1_irr;
  l2c[lc.chnl_t_irr_t_scheduledTime][2][1]=lc.CD_start_time_2_irr;
  l2c[lc.chnl_t_irr_t_scheduledTime][2][2]=lc.CD_start_time_3_irr;
  l2c[lc.chnl_t_irr_t_scheduledTime][2][3]=lc.CD_start_time_4_irr;
  l2c[lc.chnl_t_irr_t_scheduledTime][2][4]=lc.CD_start_time_5_irr;
  l2c[lc.chnl_t_irr_t_scheduledTime][2][5]=lc.CD_start_time_6_irr;
  
  l2c[lc.chnl_t_irr_t_scheddays]=[pi.p.PID_BASE_CONF_CHAN_DATA, 1, []];
  l2c[lc.chnl_t_irr_t_scheddays][2][0]=lc.CD_week_a_sun_irr;
  l2c[lc.chnl_t_irr_t_scheddays][2][1]=lc.CD_week_a_mon_irr;
  l2c[lc.chnl_t_irr_t_scheddays][2][2]=lc.CD_week_a_tue_irr;
  l2c[lc.chnl_t_irr_t_scheddays][2][3]=lc.CD_week_a_wed_irr;
  l2c[lc.chnl_t_irr_t_scheddays][2][4]=lc.CD_week_a_thu_irr;
  l2c[lc.chnl_t_irr_t_scheddays][2][5]=lc.CD_week_a_fri_irr;
  l2c[lc.chnl_t_irr_t_scheddays][2][6]=lc.CD_week_a_sat_irr;
  // TODO 6 should be b (for sun and mon)
  l2c[lc.chnl_t_irr_t_scheddays][2][7]=lc.CD_week_b_sun_irr;
  l2c[lc.chnl_t_irr_t_scheddays][2][8]=lc.CD_week_b_mon_irr;
  l2c[lc.chnl_t_irr_t_scheddays][2][9]=lc.CD_week_b_tue_irr;
  l2c[lc.chnl_t_irr_t_scheddays][2][10]=lc.CD_week_b_wed_irr;
  l2c[lc.chnl_t_irr_t_scheddays][2][11]=lc.CD_week_b_thu_irr;
  l2c[lc.chnl_t_irr_t_scheddays][2][12]=lc.CD_week_b_fri_irr;
  l2c[lc.chnl_t_irr_t_scheddays][2][13]=lc.CD_week_b_sat_irr;
  
  l2c[lc.chnl_t_irr_t_startastro]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_start_astroadjust_irr, 0];
  l2c[lc.chnl_t_irr_t_startWindowTime]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_start_irr, 0];
  l2c[lc.chnl_t_irr_t_endastro]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_end_astroadjust_irr, 0];
  l2c[lc.chnl_t_irr_t_endWindowTime]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_end_irr, 0];
  l2c[lc.chnl_t_irr_t_accl_litemin]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_setting_1_threshold_light, 0];
  l2c[lc.chnl_t_irr_t_accl_tempmin]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_setting_1_threshold_temp, 0];
  l2c[lc.chnl_t_irr_t_accl_litemax]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_setting_2_threshold_light, 0];
  l2c[lc.chnl_t_irr_t_accl_tempmax]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_setting_2_threshold_temp, 0];
  l2c[lc.chnl_t_irr_t_maxoffmm]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_off_duration, 0];
  l2c[lc.chnl_t_irr_t_soilInput]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_soil_moisture_input, 0];
  l2c[lc.chnl_t_irr_t_soilThreshold]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_soil_moisture_threshold, 0];
  l2c[lc.chnl_t_irr_t_vpdacc_thresh]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_vpdacc_threshold, 0];
  l2c[lc.chnl_t_irr_t_tankSensor]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_tankSensor_irr, 0];
  
  // l2c[lc.chnl_t_vent_t_openmin]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_time, 0];
  l2c[lc.chnl_t_vent_t_opensec]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_time_vent, 0];
  l2c[lc.chnl_t_vent_t_facing]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_direction, 0];
  l2c[lc.chnl_t_vent_t_wwmin]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_begin_closing_windward_trigger_start, 0];
  l2c[lc.chnl_t_vent_t_wwmax]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_begin_closing_windward_trigger_end, 0];
  l2c[lc.chnl_t_vent_t_lwmin]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_begin_closing_leeward_trigger_start, 0];
  l2c[lc.chnl_t_vent_t_lwmax]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_begin_closing_leeward_trigger_end, 0];
  l2c[lc.chnl_t_vent_t_holdtime]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_closing_delay, 0];
  l2c[lc.chnl_t_vent_t_rainopen]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_max_open_rain, 0];
  l2c[lc.chnl_t_vent_t_LoTempOutT]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_temp_below_trigger, 0];
  l2c[lc.chnl_t_vent_t_LoTempMaxOpen]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_max_open, 0];
  l2c[lc.chnl_t_vent_t_HiTempOutT]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_temp_var, 0];
  l2c[lc.chnl_t_vent_t_HiTempMinOpen]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_open_var, 0];
  l2c[lc.chnl_t_vent_t_vpsSensorID]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_vps_sensor, 0];
  l2c[lc.chnl_t_vent_t_vpsMaxErr]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_vps_alarm_limit, 0];
  l2c[lc.chnl_t_vent_t_vpsTimeout]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_vps_alarm_duration, 0];
  l2c[lc.chnl_t_vent_t_vpsIgnore]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_vps_error_exception_limit, 0];
  // l2c[lc.chnl_t_vent_t_vpsSCMode]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_time, 0];
  // l2c[lc.chnl_t_vent_t_vpsAllowErr]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_time, 0];
  // l2c[lc.chnl_t_vent_t_vpsDelay]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_time, 0];
  l2c[lc.chnl_t_vent_t_bVPosAutoCal]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_autoCalibration, 0];
  
  // l2c[lc.chnl_t_vent_t_ActiveCoolStageOvr]=[pi.p.PID_BASE_CONF_CHAN_DATA, 0];
  
  l2c[lc.chnl_t_vent_t_ActiveCoolStageOvr]=[pi.p.PID_BASE_CONF_CHAN_DATA, 1, []];
  l2c[lc.chnl_t_vent_t_ActiveCoolStageOvr][2][0]=lc.CD_activeCool_normal;
  l2c[lc.chnl_t_vent_t_ActiveCoolStageOvr][2][1]=lc.CD_activeCool_cool1;
  l2c[lc.chnl_t_vent_t_ActiveCoolStageOvr][2][2]=lc.CD_activeCool_cool2;
  l2c[lc.chnl_t_vent_t_ActiveCoolStageOvr][2][3]=lc.CD_activeCool_cool3;
  l2c[lc.chnl_t_vent_t_ActiveCoolStageOvr][2][4]=lc.CD_activeCool_cool4;
  l2c[lc.chnl_t_vent_t_ActiveCoolStageOvr][2][5]=lc.CD_activeCool_cool5;
  l2c[lc.chnl_t_vent_t_ActiveCoolStageOvr][2][6]=lc.CD_activeCool_cool6;
  
  
  l2c[lc.chnl_t_vent_t_ventType]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_vent_type, 0];
  // TODO these need to be completely thought out!
  /* in the vent, there's an array of 4 of these, indexed by bob's ix,
   * these are represented by values like]= CD_hgPcnt_1, these need to be duplicated for 
   * vent and cvent, the prefixes are hg, ac, thg, rtpcnt, rthum, hgtemp, actemp, rhgtemp
   * ig1800 and linkconn use different names for these values]=
   * ig1800            linkconn
   * wbPcnt	        ('hgPcnt', uint8),
   * ofPcnt	        ('acPcnt', uint8),
   * shPcnt	        ('thgPcnt', uint8),
   * hiPcnt	        ('rtPcnt', uint8),
   * hiHum	        ('rtHum', uint8),
   * wbTemp	        ('hgTemp', float32),
   * ofTemp	        ('acTemp', float32),
   * shTemp	        ('rhgTemp', float32),
   * 
   */
  l2c[lc.chnl_t_vent_t_setpointList_wbPcnt]=[pi.p.PID_BASE_CONF_CHAN_DATA, 1, []];
  l2c[lc.chnl_t_vent_t_setpointList_wbPcnt][2][0]=lc.CD_hgPcnt_1_vent;
  l2c[lc.chnl_t_vent_t_setpointList_wbPcnt][2][1]=lc.CD_hgPcnt_2_vent;
  l2c[lc.chnl_t_vent_t_setpointList_wbPcnt][2][2]=lc.CD_hgPcnt_3_vent;
  l2c[lc.chnl_t_vent_t_setpointList_wbPcnt][2][3]=lc.CD_hgPcnt_4_vent;
  
  l2c[lc.chnl_t_vent_t_setpointList_ofPcnt]=[pi.p.PID_BASE_CONF_CHAN_DATA, 1, []];
  l2c[lc.chnl_t_vent_t_setpointList_ofPcnt][2][0]=lc.CD_acPcnt_1_vent;
  l2c[lc.chnl_t_vent_t_setpointList_ofPcnt][2][1]=lc.CD_acPcnt_2_vent;
  l2c[lc.chnl_t_vent_t_setpointList_ofPcnt][2][2]=lc.CD_acPcnt_3_vent;
  l2c[lc.chnl_t_vent_t_setpointList_ofPcnt][2][3]=lc.CD_acPcnt_4_vent;
  
  l2c[lc.chnl_t_vent_t_setpointList_shPcnt]=[pi.p.PID_BASE_CONF_CHAN_DATA, 1, []];
  l2c[lc.chnl_t_vent_t_setpointList_shPcnt][2][0]=lc.CD_thgPcnt_1_vent;
  l2c[lc.chnl_t_vent_t_setpointList_shPcnt][2][1]=lc.CD_thgPcnt_2_vent;
  l2c[lc.chnl_t_vent_t_setpointList_shPcnt][2][2]=lc.CD_thgPcnt_3_vent;
  l2c[lc.chnl_t_vent_t_setpointList_shPcnt][2][3]=lc.CD_thgPcnt_4_vent;
  
  l2c[lc.chnl_t_vent_t_setpointList_hiPcnt]=[pi.p.PID_BASE_CONF_CHAN_DATA, 1, []];
  l2c[lc.chnl_t_vent_t_setpointList_hiPcnt][2][0]=lc.CD_rtPcnt_1_vent;
  l2c[lc.chnl_t_vent_t_setpointList_hiPcnt][2][1]=lc.CD_rtPcnt_2_vent;
  l2c[lc.chnl_t_vent_t_setpointList_hiPcnt][2][2]=lc.CD_rtPcnt_3_vent;
  l2c[lc.chnl_t_vent_t_setpointList_hiPcnt][2][3]=lc.CD_rtPcnt_4_vent;
  
  l2c[lc.chnl_t_vent_t_setpointList_hiHum]=[pi.p.PID_BASE_CONF_CHAN_DATA, 1, []];
  l2c[lc.chnl_t_vent_t_setpointList_hiHum][2][0]=lc.CD_rtHum_1_vent;
  l2c[lc.chnl_t_vent_t_setpointList_hiHum][2][1]=lc.CD_rtHum_2_vent;
  l2c[lc.chnl_t_vent_t_setpointList_hiHum][2][2]=lc.CD_rtHum_3_vent;
  l2c[lc.chnl_t_vent_t_setpointList_hiHum][2][3]=lc.CD_rtHum_4_vent;
  
  l2c[lc.chnl_t_vent_t_setpointList_wbTemp]=[pi.p.PID_BASE_CONF_CHAN_DATA, 1, []];
  l2c[lc.chnl_t_vent_t_setpointList_wbTemp][2][0]=lc.CD_hgTemp_1_vent;
  l2c[lc.chnl_t_vent_t_setpointList_wbTemp][2][1]=lc.CD_hgTemp_2_vent;
  l2c[lc.chnl_t_vent_t_setpointList_wbTemp][2][2]=lc.CD_hgTemp_3_vent;
  l2c[lc.chnl_t_vent_t_setpointList_wbTemp][2][3]=lc.CD_hgTemp_4_vent;
  
  l2c[lc.chnl_t_vent_t_setpointList_ofTemp]=[pi.p.PID_BASE_CONF_CHAN_DATA, 1, []];
  l2c[lc.chnl_t_vent_t_setpointList_ofTemp][2][0]=lc.CD_acTemp_1_vent;
  l2c[lc.chnl_t_vent_t_setpointList_ofTemp][2][1]=lc.CD_acTemp_2_vent;
  l2c[lc.chnl_t_vent_t_setpointList_ofTemp][2][2]=lc.CD_acTemp_3_vent;
  l2c[lc.chnl_t_vent_t_setpointList_ofTemp][2][3]=lc.CD_acTemp_4_vent;
  
  l2c[lc.chnl_t_vent_t_setpointList_shTemp]=[pi.p.PID_BASE_CONF_CHAN_DATA, 1, []];
  l2c[lc.chnl_t_vent_t_setpointList_shTemp][2][0]=lc.CD_rhgTemp_1_vent;
  l2c[lc.chnl_t_vent_t_setpointList_shTemp][2][1]=lc.CD_rhgTemp_2_vent;
  l2c[lc.chnl_t_vent_t_setpointList_shTemp][2][2]=lc.CD_rhgTemp_3_vent;
  l2c[lc.chnl_t_vent_t_setpointList_shTemp][2][3]=lc.CD_rhgTemp_4_vent;
  
  
  // l2c[lc.chnl_t_vent_t_setpointList_ofPcnt]=[pi.p.PID_BASE_CONF_CHAN_DATA + 0 +9999, 0];
  // 
  // l2c[lc.chnl_t_vent_t_setpointList_shPcnt]=[pi.p.PID_BASE_CONF_CHAN_DATA + 0 +9999, 0];
  // l2c[lc.chnl_t_vent_t_setpointList_hiPcnt]=[pi.p.PID_BASE_CONF_CHAN_DATA + 0 +9999, 0];
  // l2c[lc.chnl_t_vent_t_setpointList_hiHum]=[pi.p.PID_BASE_CONF_CHAN_DATA + 0 +9999, 0];
  // l2c[lc.chnl_t_vent_t_setpointList_wbTemp]=[pi.p.PID_BASE_CONF_CHAN_DATA + 0 +9999, 0];
  // l2c[lc.chnl_t_vent_t_setpointList_ofTemp]=[pi.p.PID_BASE_CONF_CHAN_DATA + 0 +9999, 0];
  // l2c[lc.chnl_t_vent_t_setpointList_shTemp]=[pi.p.PID_BASE_CONF_CHAN_DATA + 0 +9999, 0];
  
  // this still needs the 
  
  
  l2c[lc.chnl_t_chnl]=[pi.p.PID_BASE_CONF_CHANNELS + lc.CHC_channelIndex, 0];
  l2c[lc.chnl_t_eq_type]=[pi.p.PID_BASE_CONF_CHANNELS + lc.CHC_channelType, 0];
  l2c[lc.chnl_t_name]=[pi.p.PID_BASE_CONF_CHANNELS + lc.CHC_channelName, 0];
  // l2c[lc.chnl_t_OnBitMask]=[pi.p.PID_BASE_CONF_CHANNELS + lc.CD_time, 0];
  // l2c[lc.chnl_t_VentClosing]=[pi.p.PID_BASE_CONF_CHANNELS + lc.CD_time, 0];
  
  // TODO this one needs assigning
  // l2c[lc.chnl_t_stage]=[pi.p.PID_BASE_CONF_CHANNELS + lc.CD_time, 0];
  
  l2c[lc.chnl_t_stage]=[pi.p.PID_BASE_CONF_CHAN_DATA, 1, []];
  l2c[lc.chnl_t_stage][2][0]=lc.CD_heat6;
  l2c[lc.chnl_t_stage][2][1]=lc.CD_heat5;
  l2c[lc.chnl_t_stage][2][2]=lc.CD_heat4;
  l2c[lc.chnl_t_stage][2][3]=lc.CD_heat3;
  l2c[lc.chnl_t_stage][2][4]=lc.CD_heat2;
  l2c[lc.chnl_t_stage][2][5]=lc.CD_heat1;
  l2c[lc.chnl_t_stage][2][6]=lc.CD_normal;
  l2c[lc.chnl_t_stage][2][7]=lc.CD_cool1;
  l2c[lc.chnl_t_stage][2][8]=lc.CD_cool2;
  l2c[lc.chnl_t_stage][2][9]=lc.CD_cool3;
  l2c[lc.chnl_t_stage][2][10]=lc.CD_cool4;
  l2c[lc.chnl_t_stage][2][11]=lc.CD_cool5;
  l2c[lc.chnl_t_stage][2][12]=lc.CD_cool6;
  
  l2c[lc.chnl_t_ovr_t_enable]=[pi.p.PID_BASE_CONF_CHANNELS + lc.CHC_timedEnabled, 0];
  l2c[lc.chnl_t_ovr_t_force]=[pi.p.PID_BASE_CONF_CHANNELS + lc.CHC_timedInterval, 0];
  l2c[lc.chnl_t_ovr_t_start]=[pi.p.PID_BASE_CONF_CHANNELS + lc.CHC_timedStartTime, 0];
  l2c[lc.chnl_t_ovr_t_end]=[pi.p.PID_BASE_CONF_CHANNELS + lc.CHC_timedEndTime, 0];
  l2c[lc.chnl_t_ovr_t_sched]=[pi.p.PID_BASE_CONF_CHANNELS + lc.CHC_timedOutput, 0];
  l2c[lc.chnl_t_ch_hdh_t_dh1]=[pi.p.PID_BASE_CONF_CHANNELS + lc.CHC_stageDehumidfy1, 0];
  l2c[lc.chnl_t_ch_hdh_t_dh2]=[pi.p.PID_BASE_CONF_CHANNELS + lc.CHC_stageDehumidfy2, 0];
  l2c[lc.chnl_t_ch_hdh_t_dhlt]=[pi.p.PID_BASE_CONF_CHANNELS + lc.CHC_stageColdDehumidfy, 0];
  l2c[lc.chnl_t_ch_hdh_t_h]=[pi.p.PID_BASE_CONF_CHANNELS + lc.CHC_stageHumidfy, 0];
  l2c[lc.chnl_t_P]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_P, 0];
  l2c[lc.chnl_t_I]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_I, 0];
  l2c[lc.chnl_t_D]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_D, 0];
  
  // ryan adds values
  
  // curt
  // TODO openmin is converted to seconds
  // l2c[lc.chnl_t_curt_t_openmin]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_advanced_time, 0]; 
  l2c[lc.chnl_t_curt_t_opensec]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_advanced_time, 0];
  l2c[lc.chnl_t_curt_t_ShadeCloseMode]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_shade_start_astroadjust, 0];
  l2c[lc.chnl_t_curt_t_ShadeOpenMode]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_shade_end_astroadjust, 0];
  l2c[lc.chnl_t_curt_t_ShadeCloseTime]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_shade_start, 0];
  l2c[lc.chnl_t_curt_t_ShadeOpenTime]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_shade_end, 0];
  l2c[lc.chnl_t_curt_t_ShadeMaxClose]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_highlight_cover_perc, 0];
  l2c[lc.chnl_t_curt_t_ShadeCloseAbove]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_highlight_cover_trigger_light, 0];
  l2c[lc.chnl_t_curt_t_ShadeOutT]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_highlight_cover_trigger_temp, 0];
  l2c[lc.chnl_t_curt_t_ShadeOpenBelow]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_low_light_threshold_light, 0];
  l2c[lc.chnl_t_curt_t_ShadeOpenDelay]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_low_light_threshold_duration, 0];
  l2c[lc.chnl_t_curt_t_EnergyCloseMode]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_energy_start_astroadjust, 0];
  l2c[lc.chnl_t_curt_t_EnergyOpenMode]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_energy_end_astroadjust, 0];
  l2c[lc.chnl_t_curt_t_EnergyCloseTime]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_energy_start, 0];
  l2c[lc.chnl_t_curt_t_EnergyOpenTime]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_energy_end, 0];
  l2c[lc.chnl_t_curt_t_EnergyLiteThresh]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_keep_open_threshold_light, 0];
  l2c[lc.chnl_t_curt_t_EnergyTempThresh]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_keep_open_threshold_temp, 0];
  l2c[lc.chnl_t_curt_t_MaxStagePcnt]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_advanced_close_perc, 0];
  l2c[lc.chnl_t_curt_t_MaxAtStage]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_advanced_stage, 0];
  l2c[lc.chnl_t_curt_t_ShockProtect]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_advanced_shock_threshold, 0];
  
  // co2
  // TODO are these the right astroadjust vars
  l2c[lc.chnl_t_co2_t_startastro]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_start_astroadjust_co2, 0];
  l2c[lc.chnl_t_co2_t_starttime]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_start_time, 0];
  l2c[lc.chnl_t_co2_t_endastro]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_end_astroadjust_co2, 0];
  l2c[lc.chnl_t_co2_t_endtime]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_end_time, 0];
  l2c[lc.chnl_t_co2_t_co2_min]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_normal_co2_level, 0];
  l2c[lc.chnl_t_co2_t_co2_max]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_increased_co2_level, 0];
  l2c[lc.chnl_t_co2_t_litemin]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_light_level, 0];
  l2c[lc.chnl_t_co2_t_lockstage]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_stage, 0];
  
  // hid
  // TODO are these the right astroadjust vars
  l2c[lc.chnl_t_hid_t_startastro]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_start_astroadjust_hid, 0];
  l2c[lc.chnl_t_hid_t_starttime]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_start_hid, 0];
  l2c[lc.chnl_t_hid_t_endastro]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_end_astroadjust_hid, 0];
  l2c[lc.chnl_t_hid_t_endtime]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_end_hid, 0];
  l2c[lc.chnl_t_hid_t_lite_on]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_on_threshold, 0];
  l2c[lc.chnl_t_hid_t_lite_off]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_off_threshold, 0];
  l2c[lc.chnl_t_hid_t_offdelay]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_off_delay, 0];
  l2c[lc.chnl_t_hid_t_use_sensor]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_light_sensor_enabled, 0];
  l2c[lc.chnl_t_hid_t_mode]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_light_mode, 0];
  // TODO assigns to array - 6 elements (adapted irr structure)
  l2c[lc.chnl_t_hid_t_scheduledTime]=[pi.p.PID_BASE_CONF_CHAN_DATA, 1, []];
  l2c[lc.chnl_t_hid_t_scheduledTime][2][0]=lc.CD_start_time_1_hid;
  l2c[lc.chnl_t_hid_t_scheduledTime][2][1]=lc.CD_start_time_2_hid;
  l2c[lc.chnl_t_hid_t_scheduledTime][2][2]=lc.CD_start_time_3_hid;
  l2c[lc.chnl_t_hid_t_scheduledTime][2][3]=lc.CD_start_time_4_hid;
  l2c[lc.chnl_t_hid_t_scheduledTime][2][4]=lc.CD_start_time_5_hid;
  l2c[lc.chnl_t_hid_t_scheduledTime][2][5]=lc.CD_start_time_6_hid;
  // TODO assigns to array - 14 elements (adapted irr structure)
  l2c[lc.chnl_t_hid_t_scheduleddays]=[pi.p.PID_BASE_CONF_CHAN_DATA, 1, []];
  l2c[lc.chnl_t_hid_t_scheduleddays][2][0]=lc.CD_week_a_sun_hid;
  l2c[lc.chnl_t_hid_t_scheduleddays][2][1]=lc.CD_week_a_mon_hid;
  l2c[lc.chnl_t_hid_t_scheduleddays][2][2]=lc.CD_week_a_tue_hid;
  l2c[lc.chnl_t_hid_t_scheduleddays][2][3]=lc.CD_week_a_wed_hid;
  l2c[lc.chnl_t_hid_t_scheduleddays][2][4]=lc.CD_week_a_thu_hid;
  l2c[lc.chnl_t_hid_t_scheduleddays][2][5]=lc.CD_week_a_fri_hid;
  l2c[lc.chnl_t_hid_t_scheduleddays][2][6]=lc.CD_week_a_sat_hid;
  l2c[lc.chnl_t_hid_t_scheduleddays][2][7]=lc.CD_week_b_sun_hid;
  l2c[lc.chnl_t_hid_t_scheduleddays][2][8]=lc.CD_week_b_mon_hid;
  l2c[lc.chnl_t_hid_t_scheduleddays][2][9]=lc.CD_week_b_tue_hid;
  l2c[lc.chnl_t_hid_t_scheduleddays][2][10]=lc.CD_week_b_wed_hid;
  l2c[lc.chnl_t_hid_t_scheduleddays][2][11]=lc.CD_week_b_thu_hid;
  l2c[lc.chnl_t_hid_t_scheduleddays][2][12]=lc.CD_week_b_fri_hid;
  l2c[lc.chnl_t_hid_t_scheduleddays][2][13]=lc.CD_week_b_sat_hid;
  l2c[lc.chnl_t_hid_t_DLIsensor]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_dliSensor, 0];
  l2c[lc.chnl_t_hid_t_DLIthreshold]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_dliThreshold, 0];
  l2c[lc.chnl_t_hid_t_DLIstartime]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_dliStartTime, 0];
  l2c[lc.chnl_t_hid_t_DLIontime]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_dliOnTime, 0];
  l2c[lc.chnl_t_hid_t_DLIendtime]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_dliEndTime, 0];
  l2c[lc.chnl_t_hid_t_DLIoutput]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_dliEnergyOutput, 0];
  
  // mz
  // TODO assigns to struct - 5 elements (not too sure)
  l2c[lc.chnl_t_mzone_t_day_temp]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_daytime_setpoint, 0];
  l2c[lc.chnl_t_mzone_t_day_enable]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_daytime_enabled, 0];
  l2c[lc.chnl_t_mzone_t_day_start]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_daytime_start, 0];
  l2c[lc.chnl_t_mzone_t_day_astro]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_daytime_astroadjust, 0];
  l2c[lc.chnl_t_mzone_t_day_lock]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_daytime_stage, 0];
  // TODO assigns to array - 5 elements
  l2c[lc.chnl_t_mzone_t_nite_temp]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_nighttime_setpoint, 0];
  l2c[lc.chnl_t_mzone_t_nite_enable]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_nighttime_enabled, 0];
  l2c[lc.chnl_t_mzone_t_nite_start]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_nighttime_start, 0];
  l2c[lc.chnl_t_mzone_t_nite_astro]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_nighttime_astroadjust, 0];
  l2c[lc.chnl_t_mzone_t_nite_lock]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_nighttime_stage, 0];
  l2c[lc.chnl_t_mzone_t_sensor]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_sensor_mzone, 0];
  l2c[lc.chnl_t_mzone_t_mode]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_mode_mzone, 0];
  l2c[lc.chnl_t_mzone_t_pump]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_pump_mzone, 0];
  
  
  // TODO same variable names used for mz2
  // mixv
  l2c[lc.chnl_t_mv_pz_t_minT]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_water_temp_min, 0];
  l2c[lc.chnl_t_mv_pz_t_maxT]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_water_temp_max, 0];
  l2c[lc.chnl_t_mv_pz_t_P]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_inside_temp_p, 0];
  l2c[lc.chnl_t_mv_pz_t_I]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_inside_temp_i, 0];
  l2c[lc.chnl_t_mv_pz_t_D]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_inside_temp_d, 0];
  l2c[lc.chnl_t_mv_pz_t_sensor]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_water_temp_sensor, 0];
  // TODO openmin converted to seconds
  // l2c[chnl_t_mv_pz_t_openmin]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_, 0];
  l2c[lc.chnl_t_mv_pz_t_opensec]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_valve_open_time, 0];
  // TODO hh and mm converted to seconds
  // l2c[chnl_t_mv_pz_t_hh]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_, 0];
  // l2c[chnl_t_mv_pz_t_mm]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_, 0];
  l2c[lc.chnl_t_mv_pz_t_ss]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_heat_on_delay, 0];
  l2c[lc.chnl_t_mv_pz_t_InTempDisable]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_disable_inside_temp_pid, 0];
  l2c[lc.chnl_t_mv_pz_t_pump]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_supply_pump_mv_pz, 0];
  l2c[lc.chnl_t_mv_pz_t_retenable]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_shock_protect_enabled, 0];
  l2c[lc.chnl_t_mv_pz_t_retsensor]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_shock_protect_water_sensor, 0];
  l2c[lc.chnl_t_mv_pz_t_retthresh]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_shock_protect_threshold, 0];
  l2c[lc.chnl_t_mv_pz_t_retopen]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_shock_protect_open_valve_perc, 0];
  l2c[lc.chnl_t_mv_pz_t_rettime]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_shock_protect_time_closed, 0];
  l2c[lc.chnl_t_mv_pz_t_airsensor]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_air_temp_sensor, 0];
  l2c[lc.chnl_t_mv_pz_t_heat]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_air_heat_setpoint, 0];
  l2c[lc.chnl_t_mv_pz_t_cool]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_air_cool_setpoint, 0];
  l2c[lc.chnl_t_mv_pz_t_bCoolDevice]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_cooling_device, 0];
  l2c[lc.chnl_t_mv_pz_t_fineTempRange]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_fine_adjust_range, 0];
  l2c[lc.chnl_t_mv_pz_t_aval_1]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_analog_min_mv_pz, 0];
  // TODO two versions of analog_max
  l2c[lc.chnl_t_mv_pz_t_aval_2]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_analog_max, 0];
  l2c[lc.chnl_t_mv_pz_t_bPassiveCool]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_passive_cooling, 0];
  
  // pump
  l2c[lc.chnl_t_pump_t_type]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_pump_type, 0];
  // TODO is this the right threshold var
  l2c[lc.chnl_t_pump_t_Thresh]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_threshold, 0];
  l2c[lc.chnl_t_pump_t_id]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_pump_id, 0];
  l2c[lc.chnl_t_pump_t_ecph_probeType]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_probeType, 0];
  l2c[lc.chnl_t_pump_t_ecph_probeID]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_probeSensorIndex, 0];
  l2c[lc.chnl_t_pump_t_ecph_increase]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_dir, 0];
  l2c[lc.chnl_t_pump_t_ecph_setpoint]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_setpoint_pump, 0];
  l2c[lc.chnl_t_pump_t_ecph_deadband]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_deadband, 0];
  l2c[lc.chnl_t_pump_t_ecph_lite]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_light_threshold, 0];
  l2c[lc.chnl_t_pump_t_ecph_lite_drive_to]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_light_drive_to, 0];
  l2c[lc.chnl_t_pump_t_ecph_tempsensor]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_temp_sensor, 0];
  l2c[lc.chnl_t_pump_t_ecph_temp]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_temp_threshold, 0];
  l2c[lc.chnl_t_pump_t_ecph_temp_drive_to]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_temp_drive_to, 0];
  l2c[lc.chnl_t_pump_t_ecph_ontime]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_pulse_on_time, 0];
  l2c[lc.chnl_t_pump_t_ecph_offtime]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_pulse_ff_time, 0];
  // m_off_delay converted to seconds
  // l2c[chnl_t_pump_t_m_off_delay]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_, 0];
  l2c[lc.chnl_t_pump_t_ss_off_delay]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_offDelay, 0];
  l2c[lc.chnl_t_pump_t_feedingMode]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_feedingMode, 0];
  l2c[lc.chnl_t_pump_t_followChannelIdx]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_followChannelIndex, 0];
  l2c[lc.chnl_t_pump_t_scalingFactor]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_followRatio, 0];
  l2c[lc.chnl_t_pump_t_tankSizeLiter]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_tankSize, 0];
  l2c[lc.chnl_t_pump_t_flowRateMLiterPermInute]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_injectionRate, 0];
  l2c[lc.chnl_t_pump_t_mixingTime]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_mixingTimeSeconds, 0];
  l2c[lc.chnl_t_pump_t_injectingVolume]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_injectingVolumeMilliLiters, 0];
  l2c[lc.chnl_t_pump_t_limitInjectedVolumePerHour]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_injectingLimitMilliLiters, 0];
  
  l2c[lc.chnl_t_pump_t_injectingTime]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_injectingTimeSeconds, 0];
  l2c[lc.chnl_t_pump_t_limitInjectedTimePerHour]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_injectingLimitSeconds, 0];
  l2c[lc.chnl_t_pump_t_limitTime]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_injectingLimitHours, 0];
  
  l2c[lc.chnl_t_pump_t_resetInjection]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_resetInjectionTracker, 0];
  l2c[lc.chnl_t_pump_t_pumpMeasurementMode]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_pumpMeasurementUnits, 0];
  l2c[lc.chnl_t_pump_t_enableSmart]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_enableSmartInject, 0];
  // TODO not sure where assignment occurs
  // l2c[chnl_t_pump_t_smartLevels_minDifferencex10]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_, 0];
  // l2c[chnl_t_pump_t_smartLevels_ratiox100]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_, 0];
  l2c[lc.chnl_t_pump_t_sensorHoldTime]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_sensorHoldTime, 0];
  l2c[lc.chnl_t_pump_t_injectBatchConcentration]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_injectingBatchVolumeMilliLiters, 0];
  l2c[lc.chnl_t_pump_t_injectBatchTime]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_injectingBatchTimeSeconds, 0];
  // TODO not sure where assignment occurs
  // l2c[chnl_t_pump_t_trigStartType]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_triggers, 0];
  // l2c[chnl_t_pump_t_trigStartIDX]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_, 0];
  // l2c[chnl_t_pump_t_batchVolume]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_, 0];
  l2c[lc.chnl_t_pump_t_activeTrigger]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_activeTrigger_pump, 0];
  l2c[lc.chnl_t_pump_t_delayStartSS]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_triggerDelay, 0];
  // TODO delayStartMM and delayStartHH converted to seconds
  // l2c[chnl_t_pump_t_delayStartMM]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_, 0];
  // l2c[chnl_t_pump_t_delayStartHH]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_, 0];
  
  // fval
  // TODO assignment happens dynamically to trigger array
  l2c[lc.chnl_t_fval_t_trigStartType]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_triggers, 0];
  l2c[lc.chnl_t_fval_t_trigStartIDX]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_triggers, 0];
  l2c[lc.chnl_t_fval_t_trigStopType]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_triggers, 0];
  l2c[lc.chnl_t_fval_t_trigStopIDX]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_triggers, 0];
  l2c[lc.chnl_t_fval_t_timeHH]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_triggers, 0];
  l2c[lc.chnl_t_fval_t_timeMM]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_triggers, 0];
  l2c[lc.chnl_t_fval_t_timeSS]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_triggers, 0];
  l2c[lc.chnl_t_fval_t_volume]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_triggers, 0];
  l2c[lc.chnl_t_fval_t_activeTrigger]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_activeTrigger_fval, 0];
  // TODO spelled as tankSensor in struct
  l2c[lc.chnl_t_fval_t_tankeSensor]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_tankSensor_fval, 0];
  
  // TODO same variable names used for mixv, values are different
  // mz2
  
  // TODO could not find assignment in equipment1.php
  // htdem
  // l2c[chnl_t_hdemand_t_DT]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_, 0];
  // l2c[chnl_t_hdemand_t_maxwtemp]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_, 0];
  // l2c[chnl_t_hdemand_t_minwtemp]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_, 0];
  // l2c[chnl_t_hdemand_t_maxlight]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_, 0];
  // l2c[chnl_t_hdemand_t_maxwind]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_, 0];
  // l2c[chnl_t_hdemand_t_lightfactor]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_, 0];
  // l2c[chnl_t_hdemand_t_windfactor]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_, 0];
  
  // TODO could not find assignment in equipment1.php
  // cldem
  // l2c[chnl_t_cdemand_t_DT]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_, 0];
  // l2c[chnl_t_cdemand_t_maxwtemp]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_, 0];
  // l2c[chnl_t_cdemand_t_minwtemp]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_, 0];
  // l2c[chnl_t_cdemand_t_maxlight]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_, 0];
  // l2c[chnl_t_cdemand_t_lightfactor]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_, 0];
  
  // onoff
  l2c[lc.chnl_t_onoff_t_pump]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_pump_onoff, 0];
  l2c[lc.chnl_t_onoff_t_bCycle]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_isCycling, 0];
  l2c[lc.chnl_t_onoff_t_cycleOn]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_cycleOnPeriod, 0];
  l2c[lc.chnl_t_onoff_t_cycleOff]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_cycleOffPeriod, 0];
  l2c[lc.chnl_t_onoff_t_bActiveCoolOverrideEnable]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_enableActiveCoolStageOverride, 0];
  // TODO values assigned dynamically to array, how many is NUM_STAGES
  
  // l2c[chnl_t_onoff_t_activeCoolStageOvr]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_, 0];
  l2c[lc.chnl_t_onoff_t_activeCoolStageOvr]=[pi.p.PID_BASE_CONF_CHAN_DATA, 1, []];
  l2c[lc.chnl_t_onoff_t_activeCoolStageOvr][2][0]=lc.CD_activeCool_heat6;
  l2c[lc.chnl_t_onoff_t_activeCoolStageOvr][2][1]=lc.CD_activeCool_heat5;
  l2c[lc.chnl_t_onoff_t_activeCoolStageOvr][2][2]=lc.CD_activeCool_heat4;
  l2c[lc.chnl_t_onoff_t_activeCoolStageOvr][2][3]=lc.CD_activeCool_heat3;
  l2c[lc.chnl_t_onoff_t_activeCoolStageOvr][2][4]=lc.CD_activeCool_heat2;
  l2c[lc.chnl_t_onoff_t_activeCoolStageOvr][2][5]=lc.CD_activeCool_heat1;
  l2c[lc.chnl_t_onoff_t_activeCoolStageOvr][2][6]=lc.CD_activeCool_normal_onoff;
  l2c[lc.chnl_t_onoff_t_activeCoolStageOvr][2][7]=lc.CD_activeCool_cool1_onoff;
  l2c[lc.chnl_t_onoff_t_activeCoolStageOvr][2][8]=lc.CD_activeCool_cool2_onoff;
  l2c[lc.chnl_t_onoff_t_activeCoolStageOvr][2][9]=lc.CD_activeCool_cool3_onoff;
  l2c[lc.chnl_t_onoff_t_activeCoolStageOvr][2][10]=lc.CD_activeCool_cool4_onoff;
  l2c[lc.chnl_t_onoff_t_activeCoolStageOvr][2][11]=lc.CD_activeCool_cool5_onoff;
  l2c[lc.chnl_t_onoff_t_activeCoolStageOvr][2][12]=lc.CD_activeCool_cool6_onoff;
  
  // varout
  l2c[lc.chnl_t_varout_t_vc]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_mode_varout, 0];
  l2c[lc.chnl_t_varout_t_func]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_function, 0];
  l2c[lc.chnl_t_varout_t_maxf]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_function_float_max, 0];
  l2c[lc.chnl_t_varout_t_minf]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_function_float_min, 0];
  l2c[lc.chnl_t_varout_t_maxi]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_function_int_max, 0];
  l2c[lc.chnl_t_varout_t_mini]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_function_int_min, 0];
  l2c[lc.chnl_t_varout_t_maxout]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_analog_max_varout, 0];
  l2c[lc.chnl_t_varout_t_minout]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_analog_min_varout, 0];
  l2c[lc.chnl_t_varout_t_offset]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_offset, 0];
  l2c[lc.chnl_t_varout_t_gain]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_gain, 0];
  
  // generic
  l2c[lc.chnl_t_generic_t_type]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_output_type, 0];
  // TODO min converted to seconds
  // l2c[chnl_t_generic_t_min]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_time, 0];
  l2c[lc.chnl_t_generic_t_sec]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_time_generic, 0];
  l2c[lc.chnl_t_generic_t_analog00P]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_analog_min_generic, 0];
  l2c[lc.chnl_t_generic_t_analog99P]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_analog_max_generic, 0];
  l2c[lc.chnl_t_generic_t_sensor]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_sensor_generic, 0];
  l2c[lc.chnl_t_generic_t_setpoint]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_setpoint_generic, 0];
  l2c[lc.chnl_t_generic_t_orSensor]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_override_sensor, 0];
  l2c[lc.chnl_t_generic_t_orCompOp]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_override_dir, 0];
  l2c[lc.chnl_t_generic_t_orThreshold]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_override_trigger, 0];
  l2c[lc.chnl_t_generic_t_orValM]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_override_action_target, 0];
  l2c[lc.chnl_t_generic_t_orValPercent]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_override_limit, 0];
  l2c[lc.chnl_t_generic_t_pump]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_supply_pump_generic, 0];
  // TODO hh and mm converted to seconds
  // l2c[chnl_t_generic_t_hh]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_hold_delay, 0];
  // l2c[chnl_t_generic_t_mm]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_hold_delay, 0];
  l2c[lc.chnl_t_generic_t_ss]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_hold_delay, 0];
  
  // l2c[lc.chnl_t_cravoVent_t_wbUpDelay]=[pi.p.PID_BASE_CONF_CHAN_DATA + lc.CD_supply_pump_generic, 0];
  
  
  // snapshot values
  l2c[lc.snapshot_t_sensors_t_InTemp_Raw]=[pi.p.PID_BASE_SNAPSHOTS + lc.SN_inTemperature, 0];
  l2c[lc.snapshot_t_sensors_t_InTemp]=[pi.p.PID_BASE_SNAPSHOTS + lc.SN_inTemperature, 0];
  
  l2c[lc.snapshot_t_sensors_t_OutTemp]=[pi.p.PID_BASE_SNAPSHOTS + lc.SN_outTemperature, 0];
  l2c[lc.snapshot_t_sensors_t_InHum]=[pi.p.PID_BASE_SNAPSHOTS + lc.SN_inHumidity, 0];
  l2c[lc.snapshot_t_sensors_t_co2]=[pi.p.PID_BASE_SNAPSHOTS + lc.SN_co2, 0];
  l2c[lc.snapshot_t_sensors_t_OutHum]=[pi.p.PID_BASE_SNAPSHOTS + lc.SN_outHumidity, 0];
  l2c[lc.snapshot_t_sensors_t_Light]=[pi.p.PID_BASE_SNAPSHOTS + lc.SN_outLight, 0];
  l2c[lc.snapshot_t_sensors_t_WindSpd]=[pi.p.PID_BASE_SNAPSHOTS + lc.SN_windSpeed, 0];
  l2c[lc.snapshot_t_sensors_t_WindDir]=[pi.p.PID_BASE_SNAPSHOTS + lc.SN_windDirection, 0];
  l2c[lc.snapshot_t_sensors_t_Rain]=[pi.p.PID_BASE_SNAPSHOTS + lc.SN_rain, 0];
  l2c[lc.snapshot_t_sensors_t_FallbackInTemp]=[pi.p.PID_BASE_SNAPSHOTS + lc.SN_backupTemperature, 0];
  
  l2c[lc.snapshot_t_sensors_t_BHeat]=[pi.p.PID_BASE_SNAPSHOTS, 1, []];
  l2c[lc.snapshot_t_sensors_t_BHeat][2][0]=lc.SN_analogTemperature1;
  l2c[lc.snapshot_t_sensors_t_BHeat][2][1]=lc.SN_analogTemperature2;
  l2c[lc.snapshot_t_sensors_t_BHeat][2][2]=lc.SN_analogTemperature3;
  l2c[lc.snapshot_t_sensors_t_BHeat][2][3]=lc.SN_analogTemperature4;
  l2c[lc.snapshot_t_sensors_t_BHeat][2][4]=lc.SN_analogTemperature5;
  
  l2c[lc.snapshot_t_sensors_t_Soil]=[pi.p.PID_BASE_SNAPSHOTS, 1, []];
  l2c[lc.snapshot_t_sensors_t_Soil][2][0]=lc.SN_soilMoisture1;
  l2c[lc.snapshot_t_sensors_t_Soil][2][1]=lc.SN_soilMoisture2;
  l2c[lc.snapshot_t_sensors_t_Soil][2][2]=lc.SN_soilMoisture3;
  l2c[lc.snapshot_t_sensors_t_Soil][2][3]=lc.SN_soilMoisture4;
  l2c[lc.snapshot_t_sensors_t_Soil][2][4]=lc.SN_soilMoisture5;
  
  l2c[lc.snapshot_t_sensors_t_VentPos]=[pi.p.PID_BASE_SNAPSHOTS, 1, []];
  l2c[lc.snapshot_t_sensors_t_VentPos][2][0]=lc.SN_ventPosition1;
  l2c[lc.snapshot_t_sensors_t_VentPos][2][1]=lc.SN_ventPosition2;
  l2c[lc.snapshot_t_sensors_t_VentPos][2][2]=lc.SN_ventPosition3;
  l2c[lc.snapshot_t_sensors_t_VentPos][2][3]=lc.SN_ventPosition4;
  l2c[lc.snapshot_t_sensors_t_VentPos][2][4]=lc.SN_ventPosition5;
  
  l2c[lc.snapshot_t_sensors_t_Generic]=[pi.p.PID_BASE_SNAPSHOTS, 1, []];
  l2c[lc.snapshot_t_sensors_t_Generic][2][0]=lc.SN_generic1;
  l2c[lc.snapshot_t_sensors_t_Generic][2][1]=lc.SN_generic2;
  
  l2c[lc.snapshot_t_sensors_t_localTemp]=[pi.p.PID_BASE_SNAPSHOTS + lc.SN_localTemperature, 0];
  l2c[lc.snapshot_t_sensors_t_localHum]=[pi.p.PID_BASE_SNAPSHOTS + lc.SN_localHumidity, 0];
  l2c[lc.snapshot_t_sensors_t_diffp]=[pi.p.PID_BASE_SNAPSHOTS + lc.SN_differentialPressure, 0];
  l2c[lc.snapshot_t_sensors_t_InLight]=[pi.p.PID_BASE_SNAPSHOTS + lc.SN_inLight, 0];
  l2c[lc.snapshot_t_sensors_t_Snow]=[pi.p.PID_BASE_SNAPSHOTS + lc.SN_snow, 0];
  l2c[lc.snapshot_t_sensors_t_OutTemp2]=[pi.p.PID_BASE_SNAPSHOTS + lc.SN_outTemperatureSecondary, 0];
  l2c[lc.snapshot_t_sensors_t_Pressure]=[pi.p.PID_BASE_SNAPSHOTS + lc.SN_barometricPressure, 0];
  
  //coming in, ecph has two indexes ch0-7 is the tank, ix0-2 is the sensor
  // on our system, it just uses the channel, 192-199
  // this will be type4]= where the channel is stored as a part of the entry, in the c field + 192
  
  l2c[lc.snapshot_t_ecph_sensors_t_phx100]=[pi.p.PID_BASE_SNAP_ECPHS, 4, []];// type 4]= store ch + 192 as ch
  l2c[lc.snapshot_t_ecph_sensors_t_phx100][2][0]=lc.SE_ph1;// this ix is the sensor
  l2c[lc.snapshot_t_ecph_sensors_t_phx100][2][1]=lc.SE_ph2;
  l2c[lc.snapshot_t_ecph_sensors_t_phx100][2][2]=lc.SE_ph3;
  
  l2c[lc.snapshot_t_ecph_sensors_t_ecx10]=[pi.p.PID_BASE_SNAP_ECPHS, 4, []];// type 4]= store ch + 192 as ch
  l2c[lc.snapshot_t_ecph_sensors_t_ecx10][2][0]=lc.SE_ec1;// this ix is the sensor
  l2c[lc.snapshot_t_ecph_sensors_t_ecx10][2][1]=lc.SE_ec2;
  l2c[lc.snapshot_t_ecph_sensors_t_ecx10][2][2]=lc.SE_ec3;
  
  l2c[lc.snapshot_t_ecph_sensors_t_tempx10]=[pi.p.PID_BASE_SNAP_ECPHS, 4, []];// type 4]= store ch + 192 as ch
  l2c[lc.snapshot_t_ecph_sensors_t_tempx10][2][0]=lc.SE_temperature1;// this ix is the sensor
  l2c[lc.snapshot_t_ecph_sensors_t_tempx10][2][1]=lc.SE_temperature2;
  l2c[lc.snapshot_t_ecph_sensors_t_tempx10][2][2]=lc.SE_temperature3;
  
  l2c[lc.snapshot_t_setp_stat_t_cool]=[pi.p.PID_BASE_SNAPSHOTS + lc.SN_coolSetpoint, 0];
  l2c[lc.snapshot_t_setp_stat_t_heat]=[pi.p.PID_BASE_SNAPSHOTS + lc.SN_heatSetpoint, 0];
  l2c[lc.snapshot_t_setp_stat_t_hum]=[pi.p.PID_BASE_SNAPSHOTS + lc.SN_humidifySetpoint, 0];
  l2c[lc.snapshot_t_setp_stat_t_dehum]=[pi.p.PID_BASE_SNAPSHOTS + lc.SN_dehumidifySetpoint, 0];
  // l2c[lc.snapshot_t_setp_stat_t_ramp]=[pi.p.PID_BASE_SNAPSHOTS + lc.SN_coolSetpoint, 0];
  l2c[lc.snapshot_t_stage]=[pi.p.PID_BASE_SNAPSHOTS + lc.SN_temperatureStage, 0];
  l2c[lc.snapshot_t_hdhstage]=[pi.p.PID_BASE_SNAPSHOTS + lc.SN_humidityStage, 0];
  
  l2c[lc.snapshot_t_htd]=[pi.p.PID_BASE_SNAPSHOTS + lc.SN_heatDemand, 0];
  l2c[lc.snapshot_t_cld]=[pi.p.PID_BASE_SNAPSHOTS + lc.SN_coolDemand, 0];
  l2c[lc.snapshot_t_d2avg_t_avg]=[pi.p.PID_BASE_SNAPSHOTS, 1, []];
  l2c[lc.snapshot_t_d2avg_t_avg][2][0] = lc.SN_d2avgDayAverage;
  l2c[lc.snapshot_t_d2avg_t_avg][2][1] = lc.SN_d2avgNightAverage;
  
  l2c[lc.snapshot_t_d2avg_t_full_avg]=[pi.p.PID_BASE_SNAPSHOTS + lc.SN_d2avgTotalAverage, 0];
  l2c[lc.snapshot_t_d2avg_t_failDays]=[pi.p.PID_BASE_SNAPSHOTS + lc.SN_d2avgFailDays, 0];
  
  l2c[lc.snapshot_t_accum]=[pi.p.PID_BASE_SNAPSHOTS, 1, []];
  l2c[lc.snapshot_t_accum][2][0] = lc.SN_accumulator0;
  l2c[lc.snapshot_t_accum][2][1] = lc.SN_accumulator1;
  
  //type 3]= ix has the expansion board index, multiply ix by the coef]= 1, and add to base
  l2c[lc.snapshot_t_xcomm_status]=[pi.p.PID_BASE_EXPANSION_BOARDS, 3, 1];
  
  // type5]= add 192 to ch and store as ch, then lookup ix, and add to base
  l2c[lc.snapshot_t_ecphAlarms]=[pi.p.PID_BASE_SNAP_ECPHS, 5, []];
  l2c[lc.snapshot_t_ecphAlarms][2][0] = lc.SE_ec1SensorAlarm;
  l2c[lc.snapshot_t_ecphAlarms][2][1] = lc.SE_ec2SensorAlarm;
  l2c[lc.snapshot_t_ecphAlarms][2][2] = lc.SE_ec3SensorAlarm;
  // this is an array of 24 values, indexed by ch (0-2), and ix (0-7). Then, the 0-23
  // this also needs to be broken down by binary flags(12 bits!)
  // for each channel, there are 3 values to store
  // so, add 192 to ch to put in c, and then look up ix in the table, almost like type1
  
  l2c[lc.snapshot_t_tempAlarms]=[pi.p.PID_BASE_SNAPSHOTS + lc.SN_lowInTemperatureAlarm, 0];// needs to be broken into low, high, sensor alarms
}

// ryan pt2

// eq_rtd
l2c[lc.eqrtd_t_pos]=[pi.p.PID_BASE_SNAP_CHANNELS + lc.SNC_position, 0];
// l2c[lc.eqrtd_t_eq_type]=[pi.p.PID_BASE_SNAP_CHANNELS + lc.SNC, 0];
l2c[lc.eqrtd_t_ovr]=[pi.p.PID_BASE_SNAP_CHANNELS + lc.SNC_channelOverride, 0];
// l2c[lc.eqrtd_t_CVState]=[pi.p.PID_BASE_SNAP_CHANNELS + , 0];

// TODO don't know equivalents
// l2c[lc.eqrtd_t_eqrtd_curt_t_ShadeOpenDelay]=[pi.p.PID_BASE_SNAP_CHANNELS + lc., 0];
// l2c[lc.eqrtd_t_eqrtd_curt_t_ShockOpenDelay]=[pi.p.PID_BASE_SNAP_CHANNELS + , 0];

// TODO don't know equivalents
// l2c[lc.eqrtd_t_eqrtd_vent_t_CurrWindFacing]=[pi.p.PID_BASE_SNAP_CHANNELS + , 0];
// l2c[lc.eqrtd_t_eqrtd_vent_t_CurrWindOvrState]=[pi.p.PID_BASE_SNAP_CHANNELS + , 0];
// l2c[lc.eqrtd_t_eqrtd_vent_t_CurrWidOvrPos]=[pi.p.PID_BASE_SNAP_CHANNELS + , 0];
// l2c[lc.eqrtd_t_eqrtd_vent_t_CurrWindOvrHold]=[pi.p.PID_BASE_SNAP_CHANNELS + , 0];
// l2c[lc.eqrtd_t_eqrtd_vent_t_vpsTimeout]=[pi.p.PID_BASE_SNAP_CHANNELS + , 0];
// l2c[lc.eqrtd_t_eqrtd_vent_t_vpsAlarm]=[pi.p.PID_BASE_SNAP_CHANNELS + , 0];
// couldn't find VPosAutoCal_State in project
// l2c[lc.eqrtd_t_eqrtd_vent_t_VPosAutoCal_State]=[pi.p.PID_BASE_SNAP_CHANNELS + , 0];
// l2c[lc.eqrtd_t_eqrtd_vent_t_lastTime]=[pi.p.PID_BASE_SNAP_CHANNELS + , 0];
// l2c[lc.eqrtd_t_eqrtd_vent_t_startTime]=[pi.p.PID_BASE_SNAP_CHANNELS + , 0];


// l2c[lc.eqrtd_t_eqrtd_irr_t_IrrStartTime]=[pi.p.PID_BASE_SNAP_CHANNELS + , 0];
// l2c[lc.eqrtd_t_eqrtd_irr_t_lastIterationTime]=[pi.p.PID_BASE_SNAP_CHANNELS + , 0];
// l2c[lc.eqrtd_t_eqrtd_irr_t_accumOffCycleTime_secs]=[pi.p.PID_BASE_SNAP_CHANNELS + , 0];
// TODO assigns to an array of 6 
l2c[lc.eqrtd_t_eqrtd_irr_t_irrSched]=[pi.p.PID_BASE_SNAP_CHAN_DATA, 1, []];
l2c[lc.eqrtd_t_eqrtd_irr_t_irrSched][2][0]=lc.SNCD_activatedTimes_1;
l2c[lc.eqrtd_t_eqrtd_irr_t_irrSched][2][1]=lc.SNCD_activatedTimes_2;
l2c[lc.eqrtd_t_eqrtd_irr_t_irrSched][2][2]=lc.SNCD_activatedTimes_3;
l2c[lc.eqrtd_t_eqrtd_irr_t_irrSched][2][3]=lc.SNCD_activatedTimes_4;
l2c[lc.eqrtd_t_eqrtd_irr_t_irrSched][2][4]=lc.SNCD_activatedTimes_5;
l2c[lc.eqrtd_t_eqrtd_irr_t_irrSched][2][5]=lc.SNCD_activatedTimes_6;


l2c[lc.eqrtd_t_eqrtd_irr_t_inWindow]=[pi.p.PID_BASE_SNAP_CHAN_DATA + lc.SNCD_inWindow, 0];
// l2c[lc.eqrtd_t_eqrtd_irr_t_AactiveCyclesCount]=[pi.p.PID_BASE_SNAP_CHAN_DATA + lc.SNCD_activeCount, 0];
l2c[lc.eqrtd_t_eqrtd_irr_t_inqueue]=[pi.p.PID_BASE_SNAP_CHAN_DATA + lc.SNCD_inQueue, 0];
l2c[lc.eqrtd_t_eqrtd_irr_t_accl]=[pi.p.PID_BASE_SNAP_CHAN_DATA + lc.SNCD_accumulatedLight, 0];
l2c[lc.eqrtd_t_eqrtd_irr_t_accvpd]=[pi.p.PID_BASE_SNAP_CHAN_DATA + lc.SNCD_accvpd, 0];
// l2c[lc.eqrtd_t_eqrtd_irr_t_lasttimevpd]=[pi.p.PID_BASE_SNAP_CHAN_DATA + , 0];

// l2c[lc.eqrtd_t_eqrtd_mixv_t_IrrStartTime]=[pi.p.PID_BASE_SNAP_CHAN_DATA + , 0];
// l2c[lc.eqrtd_t_eqrtd_mixv_t_lasttime]=[pi.p.PID_BASE_SNAP_CHAN_DATA + , 0];
l2c[lc.eqrtd_t_eqrtd_mixv_t_offtime]=[pi.p.PID_BASE_SNAP_CHAN_DATA + lc.SNCD_offTime_mixv, 0];
// l2c[lc.eqrtd_t_eqrtd_mixv_t_protect_percent]=[pi.p.PID_BASE_SNAP_CHAN_DATA + , 0];
// l2c[lc.eqrtd_t_eqrtd_mixv_t_WtrTempErrSum]=[pi.p.PID_BASE_SNAP_CHAN_DATA + , 0];
// l2c[lc.eqrtd_t_eqrtd_mixv_t_AirTempErroSum]=[pi.p.PID_BASE_SNAP_CHAN_DATA + , 0];
// l2c[lc.eqrtd_t_eqrtd_mixv_t_AdjWtrTempSetpoint]=[pi.p.PID_BASE_SNAP_CHAN_DATA + , 0];

// l2c[lc.eqrtd_t_eqrtd_hid_t_lasttime]=[pi.p.PID_BASE_SNAP_CHAN_DATA + , 0];
l2c[lc.eqrtd_t_eqrtd_hid_t_DLIaccum]=[pi.p.PID_BASE_SNAP_CHAN_DATA + lc.SNCD_dailyLightIntegral, 0];
// l2c[lc.eqrtd_t_eqrtd_hid_t_DLIminOnTime]=[pi.p.PID_BASE_SNAP_CHAN_DATA + , 0];

// l2c[lc.eqrtd_t_eqrtd_varout_t_raw]=[pi.p.PID_BASE_SNAP_CHAN_DATA + , 0];
l2c[lc.eqrtd_t_eqrtd_varout_t_cal]=[pi.p.PID_BASE_SNAP_CHANNELS + lc.SNC_analogOutput_varout, 0];
// l2c[lc.eqrtd_t_eqrtd_varout_t_pcnt]=[pi.p.PID_BASE_SNAP_CHAN_DATA + , 0];

// l2c[lc.eqrtd_t_eqrtd_pump_t_currTime]=[pi.p.PID_BASE_SNAP_CHAN_DATA + , 0];
// l2c[lc.eqrtd_t_eqrtd_pump_t_pulseTime]=[pi.p.PID_BASE_SNAP_CHAN_DATA + , 0];
l2c[lc.eqrtd_t_eqrtd_pump_t_totalInjectedTime]=[pi.p.PID_BASE_SNAP_CHAN_DATA + lc.SNCD_injectedVolumeSeconds, 0];
l2c[lc.eqrtd_t_eqrtd_pump_t_totalInjectedVolume]=[pi.p.PID_BASE_SNAP_CHAN_DATA + lc.SNCD_injectedVolumeMLiters, 0];
// l2c[lc.eqrtd_t_eqrtd_pump_t_hourlyInjectedTime]=[pi.p.PID_BASE_SNAP_CHAN_DATA + , 0];
// l2c[lc.eqrtd_t_eqrtd_pump_t_hourlyInjectedVolume]=[pi.p.PID_BASE_SNAP_CHAN_DATA + , 0];
l2c[lc.eqrtd_t_eqrtd_pump_t_currSetpoint]=[pi.p.PID_BASE_SNAP_CHAN_DATA + lc.SNCD_currentSetpoint, 0];
// l2c[lc.eqrtd_t_eqrtd_pump_t_currHoldTime]=[pi.p.PID_BASE_SNAP_CHAN_DATA + , 0];
// l2c[lc.eqrtd_t_eqrtd_pump_t_applyDeadband]=[pi.p.PID_BASE_SNAP_CHAN_DATA + , 0];
// l2c[lc.eqrtd_t_eqrtd_pump_t_activeTrigger]=[pi.p.PID_BASE_SNAP_CHAN_DATA + , 0];
// l2c[lc.eqrtd_t_eqrtd_pump_t_batchVolume]=[pi.p.PID_BASE_SNAP_CHAN_DATA + , 0];
l2c[lc.eqrtd_t_eqrtd_pump_t_peristalticState]=[pi.p.PID_BASE_SNAP_CHAN_DATA + lc.SNCD_peristalticState, 0];
// l2c[lc.eqrtd_t_eqrtd_pump_t_pulse]=[pi.p.PID_BASE_SNAP_CHAN_DATA + , 0];

// l2c[lc.eqrtd_t_eqrtd_fval_t_fillTime]=[pi.p.PID_BASE_SNAP_CHAN_DATA + , 0];
// l2c[lc.eqrtd_t_eqrtd_fval_t_activeTrigger]=[pi.p.PID_BASE_SNAP_CHAN_DATA + , 0];
// l2c[lc.eqrtd_t_eqrtd_fval_t_triggerState]=[pi.p.PID_BASE_SNAP_CHAN_DATA + , 0];

// l2c[lc.eqrtd_t_eqrtd_mixva_t_IrrStartTime]=[pi.p.PID_BASE_SNAP_CHAN_DATA + , 0];
// l2c[lc.eqrtd_t_eqrtd_mixva_t_lasttime]=[pi.p.PID_BASE_SNAP_CHAN_DATA + , 0];
l2c[lc.eqrtd_t_eqrtd_mixva_t_offtime]=[pi.p.PID_BASE_SNAP_CHAN_DATA + lc.SNCD_offTime_mixv, 0];
// l2c[lc.eqrtd_t_eqrtd_mixva_t_protect_percent]=[pi.p.PID_BASE_SNAP_CHAN_DATA + , 0];
l2c[lc.eqrtd_t_eqrtd_mixva_t_curr_va]=[pi.p.PID_BASE_SNAP_CHANNELS + lc.SNC_analogOutput_mixv, 0];
// l2c[lc.eqrtd_t_eqrtd_mixva_t_last_pos]=[pi.p.PID_BASE_SNAP_CHAN_DATA + , 0];

// l2c[lc.eqrtd_t_eqrtd_gen_pid_t_StartTime]=[pi.p.PID_BASE_SNAP_CHAN_DATA + , 0];
// l2c[lc.eqrtd_t_eqrtd_gen_pid_t_DelayTime]=[pi.p.PID_BASE_SNAP_CHAN_DATA + , 0];
// l2c[lc.eqrtd_t_eqrtd_gen_pid_t_lasttime]=[pi.p.PID_BASE_SNAP_CHAN_DATA + , 0];
l2c[lc.eqrtd_t_eqrtd_gen_pid_t_curr_va]=[pi.p.PID_BASE_SNAP_CHANNELS + lc.SNC_analogOutput_generic, 0];
// l2c[lc.eqrtd_t_eqrtd_gen_pid_t_period]=[pi.p.PID_BASE_SNAP_CHAN_DATA + , 0];
l2c[lc.eqrtd_t_eqrtd_gen_pid_t_pulseOn]=[pi.p.PID_BASE_SNAP_CHAN_DATA + lc.SNCD_isPulseOn, 0];

l2c[lc.eqrtd_t_eqrtd_onoff_t_ontime_remaining]=[pi.p.PID_BASE_SNAP_CHAN_DATA + lc.SNCD_cycleOnTimeRemains, 0];
l2c[lc.eqrtd_t_eqrtd_onoff_t_offtime_remaining]=[pi.p.PID_BASE_SNAP_CHAN_DATA + lc.SNCD_cycleOffTimeRemains, 0];
// l2c[lc.eqrtd_t_eqrtd_onoff_t_lsec]=[pi.p.PID_BASE_SNAP_CHAN_DATA + , 0];
l2c[lc.eqrtd_t_eqrtd_onoff_t_state]=[pi.p.PID_BASE_SNAP_CHAN_DATA + lc.SNCD_cycleState, 0]; 

// auxconfig_t
// type 3
/*
 * the aux controls have a bunch of stuff going on. these are the fields in the struct:
 * operand1
 * operand2
 * operand3
 * target
 * functionParameter
 * operandTypes - array of 4
 * conditionTime
 * operators
 * function
 * holdTime
 * reserved
 * 
 * these are the database entries:
 *  operand1Type operandTypes[0]
 *  operand1Value operand1
 *  operand2Type operandTypes[1]
 *  operand2Value operand2
 *  operand3Type operandTypes[2]
 *  operand3Value operand3
 *  operator1 operators bits 4-7
 * operator2 operators bits 0-3 - not used
 *  conditionSeconds conditionTime
 *  action function
 *  targetType operandTypes[3]
 *  targetValue target
 *  actionParameter functionParameter
 *  actionHoldTime holdTime
 * 
 * 
 * These are different, because Bob uses ch as the main index. ix is only used for operandTypes
 * 
 * So, Type6 means that ch 0-127 is used as an index multiplier, X the following number
 * 
 */
l2c[lc.auxcntl_t_operand1]=[pi.p.PID_BASE_CONFIG_AUX_CONTROLS + lc.CAC_operand1Value, 6, 17];
l2c[lc.auxcntl_t_operand2]=[pi.p.PID_BASE_CONFIG_AUX_CONTROLS + lc.CAC_operand2Value, 6, 17];
l2c[lc.auxcntl_t_operand3]=[pi.p.PID_BASE_CONFIG_AUX_CONTROLS + lc.CAC_operand3Value, 6, 17];

l2c[lc.auxcntl_t_target]=[pi.p.PID_BASE_CONFIG_AUX_CONTROLS + lc.CAC_targetValue, 6, 17];

l2c[lc.auxcntl_t_functionParameter]=[pi.p.PID_BASE_CONFIG_AUX_CONTROLS + lc.CAC_actionParameter, 6, 17];

// Type 7, then, uses the ch * mult like type 6, *and* uses ix to index the offset
l2c[lc.auxcntl_t_operandTypes]=[pi.p.PID_BASE_CONFIG_AUX_CONTROLS, 7, 17, []];
l2c[lc.auxcntl_t_operandTypes][3][0]=lc.CAC_operand1Type;
l2c[lc.auxcntl_t_operandTypes][3][1]=lc.CAC_operand2Type;
l2c[lc.auxcntl_t_operandTypes][3][2]=lc.CAC_operand3Type;
l2c[lc.auxcntl_t_operandTypes][3][3]=lc.CAC_targetType;

l2c[lc.auxcntl_t_conditionTime]=[pi.p.PID_BASE_CONFIG_AUX_CONTROLS + lc.CAC_conditionSeconds, 6, 17];

// TODO type 1 with 2 operators
l2c[lc.auxcntl_t_operators]=[pi.p.PID_BASE_CONFIG_AUX_CONTROLS + lc.CAC_operator1, 6, 17];
l2c[lc.auxcntl_t_function]=[pi.p.PID_BASE_CONFIG_AUX_CONTROLS + lc.CAC_action, 6, 17];
l2c[lc.auxcntl_t_holdTime]=[pi.p.PID_BASE_CONFIG_AUX_CONTROLS + lc.CAC_actionHoldTime, 6, 17];

// TODO I see transfer function not assigned in database.php

l2c[lc.auxsnapshot_t_Vars]=[pi.p.PID_BASE_AUX_VARIABLE, 3, 1];
l2c[lc.auxsnapshot_t_PersistentVars]=[pi.p.PID_BASE_AUX_PERSISTENT_VARIABLE, 3, 1];
l2c[lc.auxsnapshot_t_Alarms]=[pi.p.PID_BASE_AUX_ALARMS, 3, 1];

// TODO 
// l2c[lc.auxcntl_rtd_t_timestamp]=[pi.p.PID_BASE_CONFIG_AUX_CONTROLS + lc. , 0];
// l2c[lc.auxcntl_rtd_t_activeTimestamp]=[pi.p.PID_BASE_CONFIG_AUX_CONTROLS + lc. , 0];
// l2c[lc.auxcntl_rtd_t_lastTrueCondition]=[pi.p.PID_BASE_CONFIG_AUX_CONTROLS + lc. , 0];


// TODO I see where these are assigned in database.php but most are array structure (type 1?)

// setpointSet becomes setpointValue, an array of 4 values: heat, cool, hum, dehum
// these map to auxHeatSetpointValue, auxCoolSetpointValue, auxHumidifySetpointValue, auxDehumidifySetpointValue
l2c[lc.auxrtd_t_setpointSet]=[pi.p.PID_BASE_SNAPSHOTS, 1, []];
l2c[lc.auxrtd_t_setpointSet][2][0]=lc.SN_auxHeatSetpointValue;
l2c[lc.auxrtd_t_setpointSet][2][1]=lc.SN_auxCoolSetpointValue;
l2c[lc.auxrtd_t_setpointSet][2][2]=lc.SN_auxHumidifySetpointValue;
l2c[lc.auxrtd_t_setpointSet][2][3]=lc.SN_auxDehumidifySetpointValue;

l2c[lc.auxrtd_t_setpointStatus]=[pi.p.PID_BASE_SNAPSHOTS, 1, []];
l2c[lc.auxrtd_t_setpointStatus][2][0]=lc.SN_auxHeatSetpointStatus;
l2c[lc.auxrtd_t_setpointStatus][2][1]=lc.SN_auxCoolSetpointStatus;
l2c[lc.auxrtd_t_setpointStatus][2][2]=lc.SN_auxHumidifySetpointStatus;
l2c[lc.auxrtd_t_setpointStatus][2][3]=lc.SN_auxDehumidifySetpointStatus;

l2c[lc.auxrtd_t_outputSetPosition]=[pi.p.PID_BASE_SNAP_CHANNELS + lc.SNC_auxOutputValue, 0];// these all have channel addresses
l2c[lc.auxrtd_t_equipmentPosition]=[pi.p.PID_BASE_SNAP_CHANNELS + lc.SNC_auxEquipmentValue, 0];
l2c[lc.auxrtd_t_outputStatus]=[pi.p.PID_BASE_SNAP_CHANNELS + lc.SNC_auxOutputStatus, 0];
l2c[lc.auxrtd_t_equipmentStatus]=[pi.p.PID_BASE_SNAP_CHANNELS + lc.SNC_auxEquipmentStatus, 0];

l2c[lc.auxrtd_t_alarmSetPosition]=[pi.p.PID_BASE_SNAPSHOTS, 1, []];
l2c[lc.auxrtd_t_alarmSetPosition][2][0]=lc.SN_auxLowAlarmValue;
l2c[lc.auxrtd_t_alarmSetPosition][2][1]=lc.SN_auxHighAlarmValue;

l2c[lc.auxrtd_t_alarmStatus]=[pi.p.PID_BASE_SNAPSHOTS, 1, []];
l2c[lc.auxrtd_t_alarmStatus][2][0]=lc.SN_auxLowAlarmStatus;
l2c[lc.auxrtd_t_alarmStatus][2][1]=lc.SN_auxHighAlarmStatus;

// auxrtd_t_alarmStatus:	6013,
// auxrtd_t_alarmSetPosition:	6010,
// 
// SN_auxLowAlarmStatus: 71,
// SN_auxLowAlarmValue: 72,
// SN_auxHighAlarmStatus: 73,
// SN_auxHighAlarmValue: 74,


l2c[lc.auxrtd_t_specialStates]=[pi.p.PID_BASE_SNAPSHOTS, 1, []];
l2c[lc.auxrtd_t_specialStates][2][0]=lc.SN_auxActiveCoolStatus;
l2c[lc.auxrtd_t_specialStates][2][1]=lc.SN_auxPassiveCoolStatus;

l2c[lc.auxrtd_t_specialStatesValue]=[pi.p.PID_BASE_SNAPSHOTS, 1, []];
l2c[lc.auxrtd_t_specialStatesValue][2][0]=lc.SN_auxActiveCoolValue;
l2c[lc.auxrtd_t_specialStatesValue][2][1]=lc.SN_auxPassiveCoolValue;

// auxrtd_t_specialStates:	6015,
// auxrtd_t_specialStatesValue:	6016,
// 
// auxControl.specialStates[S_ST_ACTIVE_COOL],
// auxControl.specialStatesValue[S_ST_ACTIVE_COOL],
// auxControl.specialStates[S_ST_PASSIVE_COOL_OVERRIDE],
// auxControl.specialStatesValue[S_ST_PASSIVE_COOL_OVERRIDE],
// 
// S_ST_ACTIVE_COOL = 0
// S_ST_PASSIVE_COOL_OVERRIDE = 1
// 
// 
// auxControl.specialStates[S_ST_ACTIVE_COOL],	auxActiveCoolStatus=%s
// auxControl.specialStatesValue[S_ST_ACTIVE_COOL],	auxActiveCoolValue=%s
// SN_auxActiveCoolStatus: 75,
// SN_auxActiveCoolValue: 76,
// SN_auxPassiveCoolStatus: 77,
// SN_auxPassiveCoolValue: 78,




// l2c[lc.auxrtd_t_outputSetPosition]=[pi.p.PID_BASE_CONFIG_AUX_CONTROLS + lc. , 0];
// l2c[lc.auxrtd_t_equipmentPosition]=[pi.p.PID_BASE_CONFIG_AUX_CONTROLS + lc. , 0];
// l2c[lc.auxrtd_t_alarmSetPosition]=[pi.p.PID_BASE_CONFIG_AUX_CONTROLS + lc. , 0];
// l2c[lc.auxrtd_t_outputStatus]=[pi.p.PID_BASE_CONFIG_AUX_CONTROLS + lc. , 0];
// l2c[lc.auxrtd_t_equipmentStatus]=[pi.p.PID_BASE_CONFIG_AUX_CONTROLS + lc. , 0];
// l2c[lc.auxrtd_t_alarmStatus]=[pi.p.PID_BASE_CONFIG_AUX_CONTROLS + lc. , 0];
// l2c[lc.auxrtd_t_setpointStatus]=[pi.p.PID_BASE_CONFIG_AUX_CONTROLS + lc. , 0];
// l2c[lc.auxrtd_t_specialStates]=[pi.p.PID_BASE_CONFIG_AUX_CONTROLS + lc. , 0];
// l2c[lc.auxrtd_t_specialStatesValue]=[pi.p.PID_BASE_CONFIG_AUX_CONTROLS + lc. , 0];
// l2c[lc.auxrtd_t_temperatureStageSetPosition]=[pi.p.PID_BASE_CONFIG_AUX_CONTROLS + lc. , 0];
// l2c[lc.auxrtd_t_temperatureStageStatus]=[pi.p.PID_BASE_CONFIG_AUX_CONTROLS + lc. , 0];


/*
 * { "cmd"]=1,
 * "p"]= [
 *   { "id"]=212, "ch"]=2, "ix"]=0, "d"]="1" },
 *  ...
 *   { "id"]=213, "ch"]=2, "ix"]=1, "d"]="2" }
 *  ]
 * } *
 * we enter with values from landru]= id, ch, ix, d
 * these are looked up in the l2c table]=
 * l2c[id] returns an array]= 0 is the base value, 1 is "type"
 * 0 is basic]= the base value is the result
 * 
 * the calculations will be of "types"]=
 * BASIC 
 */

// cl("here" + lc.snapshot_t_hdhtimer_t_ontime_remaining);

// export {cl2c, sl2c, al2c};


var lConfig, lSnapshot, lAuxCtl;
var lNames=[];
var tabOffsets=[];
var cmdTypes=["", "LT_config", "LT_config", "LT_snapshot", "LT_snapshot", "LT_auxctl", "LT_auxctl", "LT_auxctl", "LT_auxctl"];
var tab2tab = {
  snaps: "snapshots",
  snapChans: "snapshot_channels",
  snapEcph: "snapshot_ecphs",
  snapChanData: "snapshot_chan_Snapshot",
  confChan: "channels_configuration",
  confChanData: "config_channels_configuration",
  snapAuxAlarms: "snapshot_aux_alarms",
  snapAuxPVars: "snapshot_aux_persistent_variables",
  snapAuxVars: "snapshot_aux_variables",
  snapXBoards: "snapshot_expansion_boards",
  confAuxAlarms: "config_aux_alarms",
  confAuxConts: "config_aux_controls",
  confAuxPVars: "config_aux_persistent_variables",
  confAuxVars: "config_aux_variables",
  confXBoards: "config_expansion_boards",
  confContSets: "controller_configuration_settings",
  confEcph: "config_ecph",
  confEcphSens: "config_ecph_sensors",
  confSetpoints: "config_setpoints",
  confZoneSets: "zone_configuration_settings",
  confZones: "config_zones",
  confConts: "config_controllers",
  commStat: "config_communication_status",
};
var tab2chan = [
240, // snaps]= "snapshots",
0, // snapChans]= "snapshot_channels",// 0 is taken to mean that it's a real channel, or an ecph channel
0, // snapEcph]= "snapshot_ecphs",
0, // snapChanData]= "snapshot_chan_Snapshot",
0, // confChan]= "channels_configuration",
0, // confChanData]= "config_channels_configuration",
240, // snapAuxAlarms]= "snapshot_aux_alarms",
240, // snapAuxPVars]= "snapshot_aux_persistent_variables",
240, // snapAuxVars]= "snapshot_aux_variables",
240, // snapXBoards]= "snapshot_expansion_boards",
240, // confAuxAlarms]= "config_aux_alarms",
240, // confAuxConts]= "config_aux_controls",
240, // confAuxVars]= "config_aux_variables",
240, // confXBoards]= "config_expansion_boards",
240, // confContSets]= "controller_configuration_settings",
240, // confEcph]= "config_ecph",
240, // confEcphSens]= "config_ecph_sensors",
255, // confSetpoints]= "config_setpoints",
255, // confZoneSets]= "zone_configuration_settings",
255, // confZones]= "config_zones",
240, // confConts]= "config_controllers",
255, // commStat]= "config_communication_status",
240, // confAuxPVars]= "config_aux_persistent_variables",
];

var cmdIds={
  MSG_CMD_NACK: -1,
  MSG_CMD_ACK: 0,
  MSG_CMD_WRITE_SPECIFIC_DEVICE_CONFIG: 1,
  MSG_CMD_READ_SPECIFIC_DEVICE_CONFIG: 2,
  MSG_CMD_REPORT_CHANGED_DEVICE_CONFIG: 3,
  MSG_CMD_READ_SPECIFIC_DEVICE_STATUS: 4,
  MSG_CMD_REPORT_CHANGED_DEVICE_STATUS: 5,
  MSG_CMD_WRITE_SPECIFIC_AUX_CONFIG: 6,
  MSG_CMD_READ_SPECIFIC_AUX_CONFIG: 7,
  MSG_CMD_REPORT_CHANGED_AUX_CONFIG: 8,
  MSG_CMD_READ_SPECIFIC_AUX_STATUS: 9,
  MSG_CMD_REPORT_CHANGED_AUX_STATUS: 10,
}

var lCmdToRange=[];
lCmdToRange[cmdIds.MSG_CMD_WRITE_SPECIFIC_DEVICE_CONFIG] = 0;
lCmdToRange[cmdIds.MSG_CMD_READ_SPECIFIC_DEVICE_CONFIG] = 0;
lCmdToRange[cmdIds.MSG_CMD_REPORT_CHANGED_DEVICE_CONFIG] = 0;
lCmdToRange[cmdIds.MSG_CMD_READ_SPECIFIC_DEVICE_STATUS] = 2000;
lCmdToRange[cmdIds.MSG_CMD_REPORT_CHANGED_DEVICE_STATUS] = 2000;
lCmdToRange[cmdIds.MSG_CMD_WRITE_SPECIFIC_AUX_CONFIG] = 4000;
lCmdToRange[cmdIds.MSG_CMD_READ_SPECIFIC_AUX_CONFIG] = 4000;
lCmdToRange[cmdIds.MSG_CMD_READ_SPECIFIC_AUX_STATUS] = 6000;
lCmdToRange[cmdIds.MSG_CMD_REPORT_CHANGED_AUX_STATUS] = 6000;
var rangeToCmd=[
cmdIds.MSG_CMD_WRITE_SPECIFIC_DEVICE_CONFIG, 
cmdIds.MSG_CMD_REPORT_CHANGED_DEVICE_STATUS, 
cmdIds.MSG_CMD_WRITE_SPECIFIC_AUX_CONFIG, 
cmdIds.MSG_CMD_REPORT_CHANGED_AUX_STATUS];

var lToCmd=(id)=>{
  let cmds=[3, 5, 7, 10];
  return cmds[Math.floor(id/2000)];
}

var cTabFields;
var lName;

/*Landru Commands]=
 */

var makeLNames=()=>{
//   cl("make lNames");
  let keys=Object.keys(lc);
  //   cl(keys);
  for(let i = 0 ; i < keys.length ; i++){
    k=keys[i];
    if(lc[k] < 0) return;
    lNames[lc[k]]=k;
  }
  //   keys.forEach(k=>{
  //     lNames[lc[k]]=k;
  //   });
  
  //   var sect;
  //   keys.forEach(k=>{
  //     if(lc[k] == -100){
  //       sect = k;
  //       lNames[sect] = [];
  //     }else{
  //       lNames[sect][lc[k]]=k;
  //     }
  //   });
  //   cl(lNames);
}

var makeCNames=()=>{
  cTabFields=[];
  (Object.keys(tab2tab)).forEach(k=>{
    let tabName = tab2tab[k]
    //         cl(tabName);
    cTabFields[tabName] = [];
    //     cl(tabName);
    (Object.keys(pi.pi[tabName])).forEach(f=>{
      //       if(tabName == "snapshot_chan_Snapshot"){
      //         cl(f);
      //       }
      //       cl(f);
      cTabFields[tabName][pi.pi[tabName][f]] = f;
    });
  });
  //   cl(cTabFields["snapshot_chan_Snapshot"]);
}

var makeC2LTable=()=>{
  //   cl(lNames);
  /* we have an input vector of all the possible l2c inputs. Generate the c2l table
   * 199 is irrigation mode in landru this gets mapped to 91 in the channel data, which has a base of PID_BASE_CONF_CHAN_DATA
   * 199 is < 2000, so that refers to config
   * 
   * 
   */
  //   cl(lc.L2CInputs);
  
  lObj = {"ch": 0, "ix": 0};
  //   cl(lc.tc.length);// 388
  for(let i=0; i< lc.tc.length ; i++){//lc.tc.length
    //     cl(i);
    l=lc.tc[i];
    //   lc.tc.forEach(l=>{
    //     cl(l);
    let id = (l && l[0]) ? l[0] : l;
    let totalId = id; // this is with the 2000 offsets
    let cmd = lToCmd(id);
    id = id % 2000;
    var ind;
    var trans;
    var keys;
    //     cl(l);
    switch (typeof l){
      case "number":
        //         cl(l);
        //         let cmd = lToCmd(l);
        //         lObj.id = l % 2000;
        lObj.id = id;// I think this should be totalId
        lObj.ch = 0;
        lObj.ix = 0;
        g.mapToC(cmd, lObj);
        c2l[lObj.i]=[totalId, lObj.ch, lObj.ix];//lObj.id;
        //         if(lObj.i == 651){
        //           cl(lObj);
        //         }
        //         if(lObj.i == 129){
        //           cl(lObj);
        //         }
        break;
      case "object":
        //         if(id == 15){
        //           cl(l);
        //         }
        let type = l[1];
        //         cl(type);
        ind = l[0];
        trans = l2c[ind];// this is the forward translation array: base, type, array
        //         cl(trans);
        lObj.id = ind;// ind is the landru id
        switch(type){
          case 1:// an array of ix - an array of specific values
            trans = l2c[ind];// this is the forward translation array: base, type, array
            //             cl(trans);
            let base = trans[0];
            let type2 = trans[1];
            let arr=trans[2];// this is the array of offsets to the base address
            keys = Object.keys(arr);
            keys.forEach(k=>{
              lObj.ix = +k;// for some reason k is a string
              g.mapToC(cmd, lObj);
              c2l[lObj.i]=[totalId, lObj.ch, lObj.ix] ;//lObj.id;
              //               if(lObj.i == 756){
              //                 cl(lObj);
              //               }
            });
            break;
          case 2:// two indexes]= ch and ix
            //               trans = l2c[ind];// this is the forward translation array]= base, type, array
            for (let i = 0 ; i < l[2] ; i++){
              for (let j=0 ; j < l[3] ; j++){
                lObj.ch = i;
                lObj.ix = j;
                g.mapToC(cmd, lObj);
                //                   if(id == lc.setup_t_InfluenceFactor_AC){
                //                     cl([cmd, lObj]);
                //                   }
                c2l[lObj.i]=[totalId, lObj.ch, lObj.ix];//lObj.id;
                //                   if(lObj.id == 15){
                //                     cl(lObj);
                //                     cl(c2l[lObj.i]);
                //                   }
              }
            }
            break;
          case 3:
            for (let i = 0 ; i < l[2] ; i++){
              lObj.ix = i;
              g.mapToC(cmd, lObj);
              //               cl(lObj);
              c2l[lObj.i]=[totalId, lObj.ch, lObj.ix];//lObj.id;
              //               if(lObj.i == 756){
              //                 cl(lObj);
              //               }
            }
            //             if(id == 134){
            //               cl(trans);
            //               cl(lObj);
            //             }
            break;
          case 4:// for ecph, ch is +192
            /* type 4 is more difficult. for each Cloud ParamID, we need to have a ch, ix reference for each
             * Channel* 
             * we're trying to match 24 values for the 3 sensors in 8 tanks 
             * in landru, ix = sensor, ch = tank
             * in cloud, ch is tank, ix selects an add-on to the base id in the lTrans[2] array
             * for the ctol, we look up the id
             */
            //             cl(trans);
            keys = Object.keys(trans[2]);
            lObj.ch = 0;
            for (let j = 0 ; j < keys.length ; j++){// 0, 1, 2 for echp sensors
              lObj.ix = j;// ecph sensor
              g.mapToC(cmd, lObj);
              c2l[lObj.i]=[totalId, lObj.ix];
              //               cl(lObj.i);
              //               cl(c2l[lObj.i]);
            }
            break;
            
          case 5:// ecph alarms
            //             cl(lObj);
            //             cl(l);
            for (let i = 0 ; i < l[2] ; i++){
              for (let j=0 ; j < l[3] ; j++){
                lObj.ch = i;
                lObj.ix = j;
                g.mapToC(cmd, lObj);
                c2l[lObj.i]=[totalId, lObj.ch, lObj.ix];//lObj.id;
                //                 cl(lObj);
              }
            }
            break;
          case 6:// aux control configs
            //             cl("aux controls");
            lObj.ix = 0;
            for (let i = 0 ; i < 128 ; i++){
              lObj.ch = i;
              g.mapToC(cmd, lObj);
              c2l[lObj.i]=[totalId, lObj.ch, lObj.ix];//lObj.id;
            }
            break;
          case 7:// aux control configs operand types
            for (let i = 0 ; i < 128 ; i++){
              lObj.ch = i;
              for (let j = 0 ; j < 4 ; j++){
                lObj.ix = j;
                g.mapToC(cmd, lObj);
                c2l[lObj.i]=[totalId, lObj.ch, lObj.ix];//lObj.id;
              }
            }
            break;
          default:
            break;
        }
        break;
          default:
            break;
    }
    //     cl(typeof l);
    //   });
  }
  //   cl(c2l);
}

var testOne=()=>{
  let inp = //[3,{"ch":0,"ix":"0","id":36,"c":240,"i":4355}]
  //   [3,{"ch":0,"ix":"0","id":37,"c":240,"i":4267}]
  //   [3,{"ch":0,"ix":"0","id":39,"c":240,"i":null}]
  //   [3,{"ch":0,"ix":"0","id":40,"c":240,"i":4334}]
  //   [3,{"ch":0,"ix":"0","id":64,"c":255,"i":4855}]
  //   [3,{"ch":0,"ix":"0","id":460,"c":255,"i":4782}]
  //   [5, {"ch":0,"ix":0,"id":62,"d":0}]
  //   [5, {ch: 0, ix: 0, id: 116, d: 0}]
  //   [5,{"id":58,"z":0,"ch":0,"ix":0,"t":1603284697,"d":"0.000000"}]
  //   [4,{"id":25,"z":0,"ch":12964,"ix":0,"d":"0.000000"}]
  //   [2,{"id":205,"z":0,"ch":7,"ix":0}]
  // [2,{"id":lc.setup_t_InfluenceFactor_AC,"z":0,"ch":1,"ix":1}]// ZC_Cool_Setpoint_Influence_Factor_Lower_Threshold->4800 (84)
  // [2,{"id":lc.setup_t_InfluenceFactor_AC,"z":0,"ch":0,"ix":0}]// ZC_Heat_Setpoint_Influence_Factor_Upper_Threshold->4804 (88)
  [5,{"id":lc.eqrtd_t_eqrtd_varout_t_cal,"z":0,"ch":0,"ix":0}]// ZC_Heat_Setpoint_Influence_Factor_Upper_Threshold->4804 (88)
  
  
  
  let cmd = inp[0];
  let lObj = inp[1];
  g.mapToC(cmd, lObj);
  cl(lObj);
  
}

var testOneToL=()=>{
  //   let inp = {"z":0,"c":0,"i":520,"t":1603383430,"d":"nonea"}
  let inp = {"z":0,"c":0,"i":107};
  g.mapToL(inp);
  cl(inp);
}
// 
var testL2L=()=>{
  //   cl(c2l);
  //   cl(c2l);
  //   cl(lc.tc.length);
  //   let keys = Object.keys(c2l);
  
  //   cl(keys.length);
  var cObj = {c: 0, i: 0, d: 0};
  for (let i = 0 ; i < lc.tc.length ; i++){
    var lObj = {ch: 0, ix: 0, id: 0, d:0};
    let l = lc.tc[i];// this is the test vector entry
    //     cl(l);
    let id = (l && l[0]) ? l[0] : l;
    let cmd = lToCmd(id);
    lObj.id = id % 2000;
    if(l[0]){
      let l2cMap=l2c[l[0]];
      switch(l[1]){
        case 1:// [14,1,64]
          //           cl(l2c[l[0]]);
          //           cl(l2cMap);
          //           let keys = Object.keys(l2cMap[2]);
          for (let j = 0 ; j <  l[2] ; j++){
            if(l2cMap[2][j]){// j is the input ix value
              var lObj = {ch: 0, ix: j, id: l[0] % 2000, d:0};
              g.mapToC(cmd, lObj);
              //               cl(lObj);
              let cObj={c: 0, i: 0, d: 0}; // just one of the p elements
              cObj.c = lObj.c;
              cObj.i = lObj.i;
              g.mapToL(cObj);// this takes the params in the p array
              //               cl(cObj);
              //               cl(lObj);
              //               cl(cObj);
              let eq = (lObj.ch == cObj.ch) && (lObj.ix == cObj.ix) && (lObj.id == cObj.id) ;
              if(!eq){
                cl(i);
                cl([cmd, lObj]);
                cl(cObj);
              }
            }
          }
          //           cl("case one");
          break;
        case 2:
          for (let j = 0 ; j < l[2] ; j++){
            for (let k = 0 ; k < l[3] ; k++){
              //               cl([j, k]);
              var lObj = {ch: j, ix: k, id: l[0] % 2000, d:0};
              g.mapToC(cmd, lObj);
              let cObj={c: 0, i: 0, d: 0}; // just one of the p elements
              cObj.c = lObj.c;
              cObj.i = lObj.i;
              g.mapToL(cObj);// this takes the params in the p array
              let eq = (lObj.ch == cObj.ch) && (lObj.ix == cObj.ix) && (lObj.id == cObj.id) ;
              if(!eq){
                cl(i);
                cl([cmd, lObj]);
                cl(cObj);
              }
            }
          }
          //           cl(l);
          //           cl("case two");
          break;
        case 3:
          for (let j = 0 ; j < l[2] ; j++){
            var lObj = {ch: 0, ix: j, id: l[0] % 2000, d:0};
            g.mapToC(cmd, lObj);
            let cObj={c: 0, i: 0, d: 0}; // just one of the p elements
            cObj.c = lObj.c;
            cObj.i = lObj.i;
            g.mapToL(cObj);// this takes the params in the p array
            let eq = (lObj.ch == cObj.ch) && (lObj.ix == cObj.ix) && (lObj.id == cObj.id) ;
            if(!eq){
              cl(i);
              cl([cmd, lObj]);
              cl(cObj);
            }
          }
          //             cl(l);
          //           cl("case three");
          break;
        case 4:
          //           cl(l);
          //           cl(l[0]);
          //           cl(l2c[l[0]]);
          //           cl(l2c[2062]);
          //           let l2cMap=l2c[l[0]];
          //           cl(l2cMap);
          
          for (let j = 0 ; j < l[2] ; j++){// ch
            for (let k = 0 ; k < l2cMap[2].length ; k++){
              var lObj = {ch: j, ix: k, id: l[0] % 2000, d:0};
              g.mapToC(cmd, lObj);
              let cObj={c: 0, i: 0, d: 0}; // just one of the p elements
              cObj.c = lObj.ch + 192;
              cObj.i = lObj.i;
              g.mapToL(cObj);// this takes the params in the p array
              let eq = (lObj.ch == cObj.ch) && (lObj.ix == cObj.ix) && (lObj.id == cObj.id) ;
              if(!eq){
                cl(i);
                cl([cmd, lObj]);
                cl(cObj);
              }
            }
          }
          //           cl(l);
          //           cl("case four");
          break;
        case 5:// ecph alarms. check 24 values
          for (let j = 0 ; j < l[2] ; j++){// ch
            for (let k = 0 ; k < l[3] ; k++){// ix
              var lObj = {ch: j, ix: k, id: l[0] % 2000, d:0};
              g.mapToC(cmd, lObj);
              let cObj={c: 0, i: 0, d: 0}; // just one of the p elements
              cObj.c = lObj.ch + 192;
              cObj.i = lObj.i;
              g.mapToL(cObj);// this takes the params in the p array
              let eq = (lObj.ch == cObj.ch) && (lObj.ix == cObj.ix) && (lObj.id == cObj.id) ;
              if(!eq){
                cl(i);
                cl([cmd, lObj]);
                cl(cObj);
              }
            }
            
          }
          //           cl(l);
          //           cl(l2cMap);
          break;
        case 6:// aux controls - test 128 ch values
          for (let j = 0 ; j < 128 ; j++){// ch
            lObj.ch = j;
            g.mapToC(cmd, lObj);
            //             cl(lObj);
            let cObj={c: 0, i: 0, d: 0}; // just one of the p elements
            cObj.c = 240; // lObj.ch + 192;
            cObj.i = lObj.i;
            g.mapToL(cObj);// this takes the params in the p array
            let eq = (lObj.ch == cObj.ch) && (lObj.ix == cObj.ix) && (lObj.id == cObj.id) ;
            if(!eq){
              cl(i);
              cl([cmd, lObj]);
              cl(cObj);
            }
          }
          //             cl(l);
          break;
        case 7:
          for (let j = 0 ; j < 128 ; j++){// ch
            lObj.ch=j;
            for (let k = 0 ; k < l[2] ; k++){
              lObj.ix = k;
              g.mapToC(cmd, lObj);
              //             cl(lObj);
              let cObj={c: 0, i: 0, d: 0}; // just one of the p elements
              cObj.c = 240;
              cObj.i = lObj.i;
              g.mapToL(cObj);// this takes the params in the p array
              let eq = (lObj.ch == cObj.ch) && (lObj.ix == cObj.ix) && (lObj.id == cObj.id) ;
              if(!eq){
                cl(i);
                cl([cmd, lObj]);
                cl(cObj);
              }
            }
            
          }
          //             cl(l);
          //           cl("case 7");
          break;
          
      }
    }else{// type0
      g.mapToC(cmd, lObj);
      let cObj={c: 0, i: 0, d: 0}; // just one of the p elements
      cObj.c = lObj.c;
      cObj.i = lObj.i;
      g.mapToL(cObj);// this takes the params in the p array
      let lObj2 = {ch: 0, ix: 0, id: 0, d:0};
      lObj2.ch = cObj.ch;
      lObj2.ix = cObj.ix;
      lObj2.id = cObj.id;
      let eq = (lObj.ch == lObj2.ch) && (lObj.ix == lObj2.ix) && (lObj.id == lObj2.id) ;
      if(!eq){
        cl(i);
        cl([cmd, lObj]);
        cl(lObj2);
      }
    }
  }
}

var mapTV2L=(tv)=>{
  for(let i = 0 ; i < tv.length; i++){// tv.length 
    //     cl(i);
    let v = tv[i];
    let cObj={c: v[0], i: v[1], d: 0};
    g.mapToL(cObj, tv[i]);
    dbl(`, ${v[0]}, ${v[1]}, ${v[2]}, ${v[3]}, `);
    //     cl(tv[i]);
  }
  //   tv.forEach(v=>{
  //     
  //   });
}

// type 0: 0, 6, 7, 8, 9, 10, 11, 12, 13, 14, 21
// type 1: 1, 3, 4, 5
// type 2: 2, 16
// type 3: 17
// type 4: 18, 20
// type 5: 22
// type 6: 19
// type 7: 15

var testFromCloudIds=()=>{
  let keys=Object.keys(pi.pInd);
  tv = []; // test vector: {c: i:}
  for(let i = 0 ; i < keys.length; i++){// keys.length
    k=keys[i];
    let tab = k;
    let ti = pi.pInd[tab];
    let base = ti[0];
    let type = ti[1];
    let p0 = ti[2]; // size / index
    let p1 = ti[3];// # of indexes, this is to use with mapToL to make the ch, ix, and id values
    var keys2, col, id, ind, name, id2;
    //     cl([tab, base, type, p0, p1]);
    switch(type){
      case 0:// zone, unit, c = 0
        keys2=Object.keys(pi.pi[tab]);
        for(let j=0 ; j < keys2.length ; j++){
          k2=keys2[j];
          col=k2
          id = base + pi.pi[tab][k2];
          if(p1){
            for(let l = 0 ; l < p1 ; l++){// k is already used for "key"
              ind = l
              name=`${tab}-${col}-${ind}`;
              id2 = id + p0 * l;
              tv.push([240, id2, name]);
            }
          }else{
            id = base + pi.pi[tab][k2];
            name=`${tab}-${col}`;
            tv.push([240, id, name]);
          }
        }
        //         cl(i);
        //         cl(ti);
        //         cl(keys2);
        //         cl(tab);
        break;
      case 1:// for these the channel is set to actual channel number, just 0 here for test ids
        keys2=Object.keys(pi.pi[tab]);
        for(let j=0 ; j < keys2.length ; j++){
          k2=keys2[j];
          col=k2
          id = base + pi.pi[tab][k2];
          if(p1){
            for(let l = 0 ; l < p1 ; l++){// k is already used for "key"
              ind = l
              name=`${tab}-${col}-${ind}`;
              id2 = id + p0 * l;
              tv.push([0, id2, name]);
            }
          }else{
            name=`${tab}-${col}`;
            id = base + pi.pi[tab][k2];
            tv.push([0, id, name]);
          }
        }
        break;
      case 2:// ecphs, c is 192 - 199, just 192 here. config_ecph_sensors has sensor index
        keys2=Object.keys(pi.pi[tab]);
        for(let j=0 ; j < keys2.length ; j++){
          k2=keys2[j];
          col=k2
          id = base + pi.pi[tab][k2];
          if(p1){
            for(let l = 0 ; l < p1 ; l++){// k is already used for "key"
              ind = l
              name=`${tab}-${col}-${ind}`;
              id2 = id + p0 * l;
              tv.push([192, id2, name]);
            }
          }else{
            name=`${tab}-${col}`;
            id = base + pi.pi[tab][k2];
            tv.push([192, id, name]);
          }
        }
        break;
      case 3:
        keys2=Object.keys(pi.pi[tab]);
        for(let j=0 ; j < keys2.length ; j++){
          k2=keys2[j];
          col=k2
          id = base + pi.pi[tab][k2];
          if(p1){
            for(let l = 0 ; l < p1 ; l++){// k is already used for "key"
              ind = l
              name=`${tab}-${col}-${ind}`;
              id2 = id + p0 * l;
              tv.push([192, id2, name]);
            }
          }else{
            name=`${tab}-${col}`;
            id = base + pi.pi[tab][k2];
            tv.push([192, id, name]);
          }
        }
        break;
      case 4:
        keys2=Object.keys(pi.pi[tab]);
        for(let j=0 ; j < keys2.length ; j++){
          k2=keys2[j];
          col=k2
          id = base + pi.pi[tab][k2];
          if(p1){
            for(let l = 0 ; l < p1 ; l++){// k is already used for "key"
              ind = l
              name=`${tab}-${col}-${ind}`;
              id2 = id + p0 * l;
              tv.push([255, id2, name]);
            }
          }else{
            name=`${tab}-${col}`;
            id = base + pi.pi[tab][k2];
            tv.push([255, id, name]);
          }
        }
        break;
      case 5:// this is actually *site* wide, meaning that z=255, not just c=255
        keys2=Object.keys(pi.pi[tab]);
        for(let j=0 ; j < keys2.length ; j++){
          k2=keys2[j];
          col=k2
          id = base + pi.pi[tab][k2];
          if(p1){
            for(let l = 0 ; l < p1 ; l++){// k is already used for "key"
              ind = l
              name=`${tab}-${col}-${ind}`;
              id2 = id + p0 * l;
              tv.push([255, id2, name]);
            }
          }else{
            name=`${tab}-${col}`;
            id = base + pi.pi[tab][k2];
            tv.push([255, id, name]);
          }
        }
        break;
      case 6:// zone configuration - use the defined indexs in zone_configuration_settings
        // in this case, the index is used to select the entry in the pi.pi[tab] array
        keys2=Object.keys(pi.pi[tab]);
        for(let j=0 ; j < keys2.length ; j++){
          k2=keys2[j];
          col=k2
          id = base + pi.pi[tab][k2];// + pi.pi[tab][k2]; // p0 * 
          name=`${tab}-${col}`;
          tv.push([255, id, name]);
        }
        break;
      case 7:// controller configuration
        keys2=Object.keys(pi.pi[tab]);
        for(let j=0 ; j < keys2.length ; j++){
          k2=keys2[j];
          col=k2
          id = base + pi.pi[tab][k2];// + pi.pi[tab][k2]; // p0 * 
          name=`${tab}-${col}`;
          tv.push([240, id, name]);
        }
        break;
    }
  }
  mapTV2L(tv);
  //   tv.forEach(v=>{
  //     dbl(`, ${v[0]}, ${v[1]}, ${v[2]}`);
  //   });
}

var initTabs=()=>{
  makeLNames();
  //   cl(lNames);
  makeCNames();
  //   testOne();
  //   cl("make");
  makeC2LTable();
  testFromCloudIds();
  //   testOneToL();
  //   testL2L();
  //   cl(c2l);
  //   makeTabOffsets();
}

var doL2C=(l)=>{
  //   cl(l);
  let type=cmdTypes[l.cmd];
  //   cl(type);
  //   cl(lNames);
  lName=lNames[l.id];
  g.mapToC(l.cmd, l);
  //   cl(lName);
  //   getLName(l);
  
}

var idToTabInd=(id)=>{
  //   cl(tableBases2);
  //   console.log("table bases 2");
  //   console.log(pi.tableBases2);
  //   cl(id);
  for (let i = 0 ; i < pi.tableBases2.length ; i++){
    //         cl(pi.tableBases2[i][0]);
    if(pi.tableBases2[i][0] > id) return i - 1;
    //     cl(tableBases2[i][0]);
  }
  //     cl("not found");
}

var g={
  //   test00: ()=>{
  //     var tests = [
  //     {cmd: 1, ch: 0, ix: 0, id: 3},
  //     {cmd: 1, ch: 0, ix: lc.setup_t_InputMap_INPUTIDX_WINSPD, id:lc.setup_t_InputMap},
  //     {cmd: 1, ch: 0, ix: 2, id:lc.setup_t_CalBHeat},
  //     {cmd: 1, ch: 1, ix: 0, id:lc.setup_t_Accumulator_t_id},
  //     {cmd: 1, ch: 0, ix: 3, id:lc.setpoint_t_startmode},
  //     {cmd: 3, ch: 2, ix: 2, id:lc.snapshot_t_ecph_sensors_t_phx100},
  //     
  //     ];
  //     initTabs();
  //     doL2C(tests[5]);
  //   },
  //   
  makeLNames: makeLNames,
  lNames: lNames,
  mapToC: (cmd, l)=>{
    //     if((cmd == 5) && (l.id == 174)){
    //       cl([cmd, l]);
    //     }
    let sl=false ; // "showlines", means output to dbl
    let id=lCmdToRange[cmd] + (l.id % 2000);// adjusts Bob's input ids to our 2000-range values
    //     cl(id);
    let lName=lNames[id];// get the name of Bob's id
    var vals;
    var lcm, lch, lix, lid, cc, ci
    lcm = cmd;
    lch = l.ch;
    lix = l.ix;
    lid = l.id % 2000;
    //     cl(l.ch);
    let mt = l2c;// the l2c table to use
    //         cl(id);
    //     cl(id);
    let l2cMap = mt[id];// [4762, 0]// gets the tranform for this id of Bob's
    if(!l2cMap){
      cl("Error in mapToC");
      cl([cmd, l]);
      return;
    }
    //     if((cmd == 5) && (l.id == 174)){
    //       cl(l2cMap);
    //     }
    //     cl(mt[id]);
    //         cl(mt[id]);
    //     cl(l2cMap);
    let tabInd=idToTabInd(l2cMap[0]);// which of the LinkConn tables this relates to
    //     cl(tabInd);
    l.c = tab2chan[tabInd];// puts in the special 240 and 255 channels
    if(!l.c)l.c=l.ch;// 0 means that it's a channel, or ecph channel
    let tabShortName=pi.tableIds[tabInd];// the name of the table to show
    //     cl(tabShortName);
    let tabName=tab2tab[tabShortName];// the full name of the table
    //     cl(tabName);
    //     cl(tabInd);
    let tabInfo = pi.tableBases2[tabInd];// [4716, 6, 2, 153]// the values that the cloud uses to determine an id
    //     if(id == lc.eqrtd_t_eqrtd_irr_t_inWindow){
    //       cl(tabShortName);
    //       cl(tabInd);
    //       cl(tabInfo);
    //       cl(tabName);
    //     }
    //     if(tabName == "snapshot_chan_Snapshot"){
    //       
    //     }
    l.i = l2cMap[0];// our ID to use
    //     cl(cTabFields[tabName]);
    let fieldName=cTabFields[tabName][l.i - tabInfo[0]];// get the name of our ID
    var id2, ch;
    //     cl(l2cMap[1]);
    switch(l2cMap[1]){
      case 0:// one to one
        if(!fieldName){
          cl(tabName);
          cl(cTabFields[tabName]);
          cl(l.i - tabInfo[0]);
          cl(id2);
          cl(l);
          cl("type 0: " + lName + " => "+ tabName + "-" + fieldName);
        }
        cc = l.c;
        ci = l.i;
        vals = `\t${lcm}\t${lch}\t${lix}\t${lid}\t${cc}\t${ci}\t`;
        //         if(sl)dbl(vals + "type 0: " + lName + "	"+ tabName + "-" + fieldName);
        // //         cl(l);
        break;
      case 1:// a defined array
        id2=l2cMap[2][l.ix];
        l.i += id2;
        fieldName=cTabFields[tabName][id2];
        if(!fieldName){
          //           cl(tabName);
          //           cl(cTabFields[tabName]);
          //           cl(l.i - tabInfo[0]);
          //           cl(id2);
          //           cl(l);
          cl("type 1 ix=" + l.ix + ": " + lName + " => "+ tabName + "-" + fieldName);
        }
        cc = l.c;
        ci = l.i;
        vals = `\t${lcm}\t${lch}\t${lix}\t${lid}\t${cc}\t${ci}\t`;
        if(sl)dbl(vals + "type 1: ix=" + l.ix + ": " + lName + "	"+ tabName + "-" + fieldName);
        break;
      case 2: //a 2x2 array defined
        //       id2=l2cMap[2][l.ix];
        //         cl(l);
        
        l.i += l2cMap[2][l.ch][l.ix];
        //       cl(c.i);
        fieldName=cTabFields[tabName][l.i - tabInfo[0]];
        //         cl(l.i - tabInfo[0]);
        if(!fieldName){
          cl(tabName);
          cl(l);
          cl("type 2 ch=" + l.ch + " ix=" + l.ix + ": " + lName + " => "+ tabName + "-" + fieldName);
        }
        cc = l.c;
        ci = l.i;
        vals = `\t${lcm}\t${lch}\t${lix}\t${lid}\t${cc}\t${ci}\t`;
        if(sl)dbl(vals + "type 2: ch=" + l.ch + " ix=" + l.ix + ": " + lName + "	"+ tabName + "-" + fieldName);
        break;
      case 3:// 4640, 3, 10] // [4636, 4, 10, 8] // {cmd: 1, ch: 0, ix: 3, id: 135}
        //         cl(l.i);
        //         cl([l.id, l2cMap[2], l.i]);
        l.i += l.ix * l2cMap[2];// multiply index by size of entries
        //         cl(l);
        //         cl(l2cMap);
        //       c.i += l2cMap[1] * l2cMap[2];
        //       cl(c.i);
        if(!fieldName){
          cl("type 3 ix=" + l.ix + ": " + lName + " => "+ tabName + "-" + fieldName + " + " + l2cMap[1] + " * " + l2cMap[2]);
        }
        cc = l.c;
        ci = l.i;
        vals = `\t${lcm}\t${lch}\t${lix}\t${lid}\t${cc}\t${ci}\t`;
        if(sl)dbl(vals + "type 3: ix=" + l.ix + ": " + lName + "	"+ tabName + "-" + fieldName + " + " + l2cMap[1] + " * " + l2cMap[2]);
        break;
      case 4:// like snapshot_t_ecph_sensors_t_phx100
        l.i += l2cMap[2][l.ix];
        fieldName=cTabFields[tabName][l.i - tabInfo[0]];
        l.c = 192 + l.ch;
        //         cl(l);
        if(!fieldName){
          cl(tabName);
          cl(l);
          cl("type 4 ch=" + l.ch + " ix=" + l.ix + ": " + lName + " => "+ tabName + "-" + fieldName + " ch=" + l.c);
        }
        cc = l.c;
        ci = l.i;
        vals = `\t${lcm}\t${lch}\t${lix}\t${lid}\t${cc}\t${ci}\t`;
        if(sl)dbl(vals + "type 4: ch=" + l.ch + " ix=" + l.ix + ": " + lName + "	"+ tabName + "-" + fieldName + " ch=" + l.c);
        break;
      case 5:  // type5: add 192 to ch and store as c, then lookup ix, and add to base, like snapshot_t_ecphAlarms
        // was type5: add 192 to ch and store as ch, then multiply ix by coef and add to base
        l.c=192 + l.ch;
        id2=l2cMap[2][l.ix];
        l.i += id2;
        fieldName=cTabFields[tabName][id2];
        //         cl("type 1 ix=" + l.ix + ": " + lName + " => "+ tabName + "-" + fieldName);
        
        if(!fieldName){
          cl("type 5 ch=" + l.ch + " ix=" + l.ix + ": " + lName + "	"+ tabName + "-" + fieldName + " ch=" + l.c);
        }
        cc = l.c;
        ci = l.i;
        vals = `\t${lcm}\t${lch}\t${lix}\t${lid}\t${cc}\t${ci}\t`;
        if(sl)dbl(vals + "type 5: ch=" + l.ch + " ix=" + l.ix + ": " + lName + "	"+ tabName + "-" + fieldName + " ch=" + l.c);
        break;
        //       case 6:// type6, no indexing, but ch gets copied to cloud channel
        //         l.c = l.ch;
        //         //         cl("type 6 ch=" + l.ch + " ix=" + l.ix + ": " + lName + " => "+ tabName + "-" + fieldName + " ch=" + l.c);
        //         break
      case 6:// aux controls
        //         let ofs = l.i;
        //         ofs -= tabInfo[0];
        fieldName=cTabFields[tabName][l.i - tabInfo[0]];
        l.i += l.ch*l2cMap[2];
        //         cl(l2cMap);
        if(!fieldName){
          cl("type 6 ch=" + l.ch + " ix=" + l.ix + ": " + lName + " => "+ tabName + "-" + fieldName + " ch=" + l.c);
        }
        cc = l.c;
        ci = l.i;
        vals = `\t${lcm}\t${lch}\t${lix}\t${lid}\t${cc}\t${ci}\t`;
        if(sl)dbl(vals + "type 6: ch=" + l.ch + " ix=" + l.ix + ": " + lName + "	"+ tabName + "-" + fieldName + " ch=" + l.c);
        break;
      case 7:// like auxcntl_t_operandTypes
        //         cl(l2cMap);
        ofs = l.i + l2cMap[3][l.ix];
        l.i += l.ch*l2cMap[2] + l2cMap[3][l.ix];
        //         ofs -= ;
        fieldName=cTabFields[tabName][ofs - tabInfo[0]];
        if(!fieldName){
          cl(l);
          cl("type 7 ch=" + l.ch + " ix=" + l.ix + ": " + lName + "	"+ tabName + "-" + fieldName + " ch=" + l.c);
        }
        cc = l.c;
        ci = l.i;
        vals = `\t${lcm}\t${lch}\t${lix}\t${lid}\t${cc}\t${ci}\t`;
        if(sl)dbl(vals + "type 7: ch=" + l.ch + " ix=" + l.ix + ": " + lName + "	"+ tabName + "-" + fieldName + " ch=" + l.c);
        break;
      case 8:// ecph sensors: ch is tank, ix is sensor - taken from type 3, like ecphConfig_t_ecphSensor_t_ecMapping
        l.i += l.ix * l2cMap[2];// multiply index by size of entries
        l.c=192 + l.ch;
        if(!fieldName){
          cl("type 8 ix=" + l.ix + ": " + lName + " => "+ tabName + "-" + fieldName + " + " + l2cMap[1] + " * " + l2cMap[2]);
        }
        cc = l.c;
        ci = l.i;
        vals = `\t${lcm}\t${lch}\t${lix}\t${lid}\t${cc}\t${ci}\t`;
        if(sl)dbl(vals + "type 3: ix=" + l.ix + ": " + lName + "	"+ tabName + "-" + fieldName + " + " + l2cMap[1] + " * " + l2cMap[2]);
        break;
      case 9:// one to one, for echp_config, add 192
        cc = 192 + l.c;
        ci = l.i;
        vals = `\t${lcm}\t${lch}\t${lix}\t${lid}\t${cc}\t${ci}\t`;
        break;
      default:
        break;
    }
  },
  
  //   mapToC: (cmd, l)=>{
  mapToL: (c, tv=[])=>{
    /* I'm unclear if this should do 1 or an array of Cloud values.
     * for the moment, I'm going back to 1.
     * so this is just one of the elements in the p array
     * I have not had a proper treatment for type1, where there's an input ix and an output id offset.
     * this has to work in reverse, too, so that I can look up the final parameter id, and find the id, ch, and ix that produced it.
     * I think we're just going to change c2l so that each entry is an array: [id, ch, ix]
     */
    // cl(c);
    let cId = c.i;// this is the first id in the p array
    let c2lArr = g.c2l[cId];// this is the cloud id
    //     cl(c2lArr);// this is id, ch, and ix
    if(!c2lArr){
      //         cl(cId);
      //         cl("error in mapToL!!!");
      //         cl(c);
      tv.push("Error");
      return false;
    }
    let lId = c2lArr[0];
    let forceChan=c2lArr[1];
    tv.push(lNames[lId]);
    //     cl(lId);
    c.cmd=rangeToCmd[Math.floor(lId/2000)];// this is the command to send, with all 
    let lTrans=l2c[lId];// this is the landru (l2c) translation array
    //     cl(lTrans);
    let lType=lTrans[1];
    //     cl(lTrans);
    //     c.ch = 0; // c.c;// 0 is the default. c is used when necessary
    let ch = (c.c < 40) ? c.c : 0;// if c represents a real channel
    c.ch=(forceChan) ? forceChan : ch;// this is supposed to fill in the channels for equipment correctly
    c.ix = 0;
    c.id = lId % 2000;
    switch (lType){
      case 0:
        break;
      case 1:// 
        //         c.ch = c2lArr[1];
        c.ix = c2lArr[2];
        //         cl(c);
        break;
      case 2:
        //         c.ch = c2lArr[1];
        c.ix = c2lArr[2];
        break;
      case 3:
        //         c.ch = c2lArr[1];
        c.ix = c2lArr[2];
        break;
      case 4: // ecph sensors 
        //         cl(c2lArr);
        c.ch = c.c - 192;
        //         c.ch = c2lArr[1];
        c.ix = c2lArr[1];
        break;
      case 5:
        c.ch = c.c - 192;
        c.ix = c2lArr[2];
        break;
      case 6:// equipment types, put c in ch, ix is 0
        //         c.ch = c2lArr[1];
        c.ix = c2lArr[2];
        //         cl(c2lArr);
        //         cl("type 6");
        //         cl(c);
        //         lObj.ch = p.c;
        //         c.p[i]=lObj;
        break;
      case 7:
        //         c.ch = c2lArr[1];
        c.ix = c2lArr[2];
        //         cl(c2lArr);
        break;
      default:
        break;
    }
    return true;
  },
  c2l: c2l,
  
  
};

// cl(lc.c2l);

init();
initTabs();
// cl("initted");

/*
 * { "cmd":1,
 * "p": [
 *   { "id":212, "ch":2, "ix":0, "d":"1" },
 *  ...
 *   { "id":213, "ch":2, "ix":1, "d":"2" }
 *  ]
 * } *
 * we enter with values from landru: id, ch, ix, d
 * these are looked up in the l2c table:
 * l2c[id] returns an array: 0 is the base value, 1 is "type"
 * 0 is basic: the base value is the result
 * 
 * the calculations will be of "types":
 * BASIC 
 */

// cl("here" + lc.snapshot_t_hdhtimer_t_ontime_remaining);

// export {l2c};
module.exports=g;
