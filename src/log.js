var utils = require('./utils')

var cl = (m)=>{
  let pos = 1 + __filename.lastIndexOf('/');
  let ln = utils.callStack()[1].getLineNumber()
  console.log(__filename.substr(pos) + " " + ln + ": " + m);
}

function getTime(){
  return Math.floor ((new Date()).getTime() / 1000);
}

var addLog = (req, res)=>{
//   cl("addLog");
  let msg = req.params.msg; // decodeURIComponent(req.params.msg);
  let parts = req.connection.remoteAddress.split(":");
  let ip = parts[parts.length-1];
  let ti = utils.makeLogTime(new Date());
  let msg2 = ti + " " + ip + ": " + msg
//   cl(ti);
//   cl(req.params.msg)
  if (g.log != null){
    g.log.insertOne({
      i: ip,
      t: ti,
      m: msg,
    })
  }
  if (g.ws !== null){
    g.ws.send(msg2);
  }
  res.status(200).send("ok");
}

var setRoutes = (app)=>{
  app.get('/log/:msg', addLog);
  app.get('/logshow', (req, res)=>{
    res.sendFile(__dirname + '/showLog.html'); 
  });
}

var onListen = ()=>{
    console.log('Example app listening on port 3003!')
}

var onWsClose = ()=>{
  g.ws = null;
}

var onWsConnect = (ws)=>{
  g.ws = ws;
  g.ws.on('close', onWsClose);
}

var g = {
  app: null,
  log: null,
  ws: null,
  
  init: ()=>{
    cl("log init");
    utils.openMongoDB('ngdb').then(db=>{
      g.log = utils.openMongoColl(db, 'log0');
    });
    utils.openExpressServer(3373, g.onListen).then(r=>{
      g.app = r;
      setRoutes(r);
      cl("3373 open");
    });
    utils.openWebSocket(3377, onWsConnect).then(ws=>{
      g.websock = ws;
      cl("3377 open");
    });
  }
}

g.init();

module.exports = g;
