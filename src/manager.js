 
var utils = require('./utils')
var cl = (m)=>{
  let pos = 1 + __filename.lastIndexOf('/');
  let ln = utils.callStack()[1].getLineNumber()
  if (typeof m === "object"){ m = JSON.stringify(m); }
  console.log(__filename.substr(pos) + " " + ln + ": " + m);
}


var dbl = (m)=>{
  let pos = 1 + __filename.lastIndexOf('/');
  let ln = utils.callStack()[1].getLineNumber()
  if (typeof m === "object"){ m = JSON.stringify(m); }
  utils.dbl(__filename.substr(pos) + " " + ln + ": " + m);
}

var siteinit00 = (o)=>{
//   cl("site init");
//   cl(o);
  g.connectionMQTTCollection.updateOne({s: o.site}, {$set: {s: o.site, mqtt: o.queue, }}, {upsert: true});
//   cl(o);
}

var sendLoginToMQTT = (o)=>{
//   cl("send login");
  let query = {s: o.site};
  cl(query);
  g.connectionMQTTCollection.findOne(query).then(r=>{
    cl("matched new ws Client with mqtt queue for " + r.s + ": " + r.mqtt);
    dbl("matched new ws Client with mqtt queue for " + r.s + ": " + r.mqtt);
//     cl(r);
    if (r !== null){
      g.chanManager.publish("mqtt", r.mqtt, Buffer.from(JSON.stringify(o)));
    }
  });
  
}

var wsinit00 = (o)=>{
//   cl("ws init");
//   cl(o);
  g.connectionWSCollection.updateOne({s: o.site}, {$set: {s: o.site, ws: o.queue, sid: o.socketId}}, {upsert: true});
  o.command = "addWS";
  sendLoginToMQTT(o);
//   g.connectionMQTTCollection.updateOne({s: o.site}, {$set: {s: o.site, mqtt: o.queue, }}, {upsert: true});
//   cl(o);
}

var getmqttqueue00 = (o)=>{
//   cl(o);
  let query = {s: o.site};
//   cl(query);
  g.connectionMQTTCollection.findOne(query).then(r=>{
//     cl(r);
    let resp = {
      command: "gotmqttqueue00",
      site: o.site,
      queue: r.mqtt,
    }
    g.chanManager.publish("ws", o.returnQueue, Buffer.from(JSON.stringify(resp)));
  })
  
}

var msgManager = (msg)=>{
//   cl("got manager message");
  cmds = {siteinit00: siteinit00, wsinit00: wsinit00, getmqttqueue00: getmqttqueue00}
  let o = JSON.parse(msg.content);
//   cl(o);
  cmds[o.command](o);
//   cl(msg.content.toString());
}

var g = {
  managerExchange: 'manager',
  chanManager: null,
  connectionMQTTCollection: null,
  
  init: ()=>{
    utils.openMongoDB('manage', utils.managerUrl).then(db=>{
      g.connectionMQTTCollection = utils.openMongoColl(db, 'connectionsMQTT');
      g.connectionWSCollection = utils.openMongoColl(db, 'connectionsWS');
      cl("mongo open for manage: connecttionsMQTT, connectionsWS on " + utils.managerUrl);
    });
    utils.openAMQP(utils.amqpUrl).then(conn=>{
      utils.openAMQPChannel00(conn, g.managerExchange, 'fanout', msgManager).then((r)=>{
        cl("manager connected to AMQP " + g.managerExchange + " on " + utils.amqpUrl);
        g.chanManager = r.chan;
      });
    })
    
  }
  
}

g.init();

module.exports = g;
