// var lc=require('./landruConstants');
// var pi=require('./paramIds');
var l2c=require('./l2c2l');

/* this is the mqtt processor
 assume that the controller client has already been to the authorization / service discovery, and gotten a JWT
 mosquitto will require a username / password, and we will require the JWT, to post to the PodStream
 so, this accepts mqtt packets, adds the "data00" command, and sends them out the PodSiteStream
 this will also make the PodSecond packs, to send to Petra
 and this handles the data going back *down* to the controller
 Now, this will be in the d0/j/(username) stream, so we need to know what username is associated with each username
 There should be a kind of "login" packet, that begins the process, so that we can associate the site / username at that time
 This will monitor the PodClientStream channel, looking for data01 packets that are bound for the sites. If it references
 a site that we have, then we send it down.
 Every Site has exactly one username / password for MQTT
 The Posts to u0/j/(username), but the MQTT Server subscribes to all: u0/j/#

 written on Basecamp:
 MQTT Server: Runs Mosquitto MQTT broker, using TLS connections. Uses Username/Password authentication for each Site, which has unique 12 character Base64 (72 bit) Username, and  (72 bit) Password. This authorizes the Site to Publish and Subscribe to topics like "u0/j/(username)", and "d0/j/(username)", only, which carry the data from (up) and to (down) the site.

The MQTT Server verifies that the SiteId of the packages from the Site match the Username, and then publish the data to the PodSiteStream. Also, it monitors the PodClientStream for packets destined for one of the Sites that it serves. If found, it publishes the data to the MQTT stream with the appropriate username.

The MQTT Server also handles the Failsafe functions. As each packet is received from a Site, it is added to the current PodSecond Pack. Every 10 seconds, this is sent to Petra, and only when that transfer is acknowledged, is the MQTT QOS 1 Acknowledge sent. These packets are also sent over the Control Stream, to be saved by the Manager.

Each Site will have a Client Secret. This is presented to the Authorization Server, to get a JWT, which includes the SiteID.
This is presented to the MQTT Server with each packet, and this is where the SiteID comes from, *not* the site id in the packet.

For now:
use the site in the packet.
save the site / username association with each packet

so, subscribe to u0/j/#, pass *all* packets on to PodSiteStream, and keep track of username / site associations
when we receive a packet to a site, use that username: d0/j/(username), which the site is subscribed to

 */
var utils = require('./utils');
// var parid=require('./paramIds');
// var l2c=require('./l2c2l');

// console.log(l2c.l2c);
var cl = (m)=>{
  let dt = new Date();
  let ti = " " + utils.az(dt.getMinutes(), 2) + ":" + utils.az(dt.getSeconds(), 2);
  let pos = 1 + __filename.lastIndexOf('/');
  let ln = utils.callStack()[1].getLineNumber()
  if (typeof m === "object"){ m = JSON.stringify(m); }
  console.log(__filename.substr(pos) + " " + ln + ti +": " + m);
}

var dbl = (m)=>{
  let pos = 1 + __filename.lastIndexOf('/');
  let ln = utils.callStack()[1].getLineNumber()
  if (typeof m === "object"){ m = JSON.stringify(m); }
  utils.dbl(__filename.substr(pos) + " " + ln + ": " + m);
}

// cl(utils);
var siteInit= (site)=>{
/* each time data is received from a site, check that we've sent the site infor to the Manager*/
  g.siteInitted[site] = 1;
//   cl(site);
//   cl("send site init");
  let pack = {
    command: "siteinit00",
    site: site,
    queue: g.queueMQTT,
  };
//   cl("init");
  cl("site " + site + " init. queue: " + g.queueMQTT);
  try{
    g.chanMQTT.publish("manager", '', Buffer.from(JSON.stringify(pack)));
  }catch{}
}

var addWS = (o)=>{
  cl("added a websocket for " + o.site + " on queue " + o.queue);
  g.ws[o.site] = {socketId: o.socketId, queue: o.queue};
//   cl(g.ws);

}

var data01 = (o)=>{
  // l2c2l.js 1635: {"z":0,"c":240,"i":23,"d":"0","cmd":5,"ch":0,"ix":0,"id":37}
  
//   cl(o);
//   cl("received data01 for site " + o.site + " d: " + o.params[0].d);
  var mqttDown;
  if(o.command == "data01"){// "demo_get_status" || //  || "demo_get_config"
    l2c.mapToL(o.params[0]);
    let p0=o.params[0];
    let p2Arr = [];
    o.params.forEach(p=>{
      l2c.mapToL(p);
      let p2 = {
        id: p.id,
        z: 0,
        ch: p.ch,
        ix: p.ix,
        t: 102,
        d: p.d.toString(),
      }
      p2Arr.push(p2);
    });
    mqttDown={
      v: 257,
      s: "SdWgD0C2ncIv5e6u",
      z: 0,
      p: p2Arr,
    }
    let landruCommands={
      demo_get_status: 4, // read status
      demo_get_config: 2, // read config
      data01: 1,// write config
    }
    mqttDown.cmd = landruCommands[o.command];
  }
  
  
//   if(o.command=="data02"){
//     o.params = o.p;
//   }
// mqtt.js 63: {"command":"data01","site":0,"user":0,"zone":0,"token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IkREbWNHcUUxd3FLa3A1cjlPcFAzc29RS1RNVnhuMkhQIiwicHJpdnMiOnsicyI6MH0sImlhdCI6MTU3MDE4NTg5NywiZXhwIjoxNTcwMjcyMjk3fQ.x9BmMeL-23A7jdjo3Fy3SL82cSe_Do69LPVOy7dtfzo","params":[{"c":255,"i":4643,"t":1570189200,"d":22.2}]}

/*
sending packet to controller:
{v: version, s: site, z: zone, p: (params)[{c: channel, i: id, t: time, d: data}]}

need to publish to d0/j/site0
 */
//   let mqttDown = {
//     cmd: o.cmd,
//     v: 257,
//     s: o.site,
//     z: o.zone,
//     p: o.params,
//   }
//   cl(mqttDown);
//   dbl(mqttDown);
//   if(mqttDown.s == "SdWgD0C2ncIv5e6u") l2c.mapToL(mqttDown);
  let topic = "d0/j/" + g.siteToUsername[o.site];
  let msg = JSON.stringify(mqttDown);
//   cl(topic);
  g.mqttClient.publish(topic, msg);
  dbl(topic + ": " + msg);
  cl(topic + ": " + msg);
//   cl(g.siteToUsername);
}

var msgMQTT = (msg)=>{
//   cl("msgmqtt")
//   dbl(msg);
  let cmds = {addWS: addWS, data01: data01, data02: data01, 
    demo_get_status: data01,
    demo_get_config: data01,
  };
  obj = JSON.parse(msg.content);
//   dbl(obj.command);
//   cl(obj);
//   cl(msg.content);

//   cl("got mqtt message");
//   cl(obj)


  cmds[obj.command](obj);
//   cl(msg.content.toString());

}

var g = {
  url: utils.mqttUrl, // 'mqtt://dev.l4ol.com',
  topic: 'u0/j/#',
  siteToUsername: {},// associative array
  PodSiteExchange: 'PodSiteStream',
  siteInitted: {},
  chanMQTT: null,
  queueMQTT: null,
  mqttClient: null,
  ws: {},

  getTopicUsername: (topic)=>{
    let pos = 1 + topic.lastIndexOf('/');
    return topic.substr(pos);
  },

  checkSetpoint: (o)=>{
//     cl(o.p[0]);
    o.p.forEach(v=>{
      if (v.i === 4642){cl("Setpoint Change from Site: " + v.d)}
    });
  },
  
//   fixOneL2C: (p)=>{
//     map
//   }
  
  fixLandruToCloud: (o)=>{
//     cl(o.cmd);
    if(o.cmd){
      o.p.forEach(p=>{
//         dbl(p);
        l2c.mapToC(o.cmd, p);
        dbl(p);
      });
//       cl(o);
    }
  },

  mqttConsume: (topic, msg)=>{
/* the site initting has to change a bit, since we're using clientIds now.
 */
//     dbl("testing");
//     cl("got mqtt from site");
//     cl(msg);
//     cl("mqtt consume");
//     cl(msg);
    var obj = {};
    let clientId = g.getTopicUsername(topic);// actually, the clientid, now
//     if((clientId != "landru1_Bobs") &&(clientId != "landru1_Genes") && (clientId != "landru1_Serhiis")) return;
    let username=clientId;// the old name
//     if((clientId == "landru1_Genes") || (clientId == "landru1_329de32a")) return;
//     cl([clientId, msg]);
    try{
      obj = JSON.parse(msg);
    } catch{ 
      cl("json error");
      return 
      
    }
//     utils.getJwtToken(obj.token).then(r=>{
//       if(r.exp != 1603445073){return};
//     });
//     cl(clientId);
//     cl(obj);
    if(!obj.p) return;
    let msg2 = {clientId: clientId, msg: msg};
//     cl(clientId);
//     if(clientId == g.bobClient){
//       g.bobReceived += msg;// msg is text
//       cl(g.bobReceived);
      g.chanMQTT.publish('bob', '', Buffer.from(JSON.stringify(msg2)));
//     }
//     return;
//     cl(obj);
// {"v":257,"s":0,"z":0,"k":"01234567890123456789012345678901","p":[{"c":240,"i":1,"t":1572709632,"d":"250977","h":0},{"c":240,"i":4,"t":1572709632,"d":"1572709632","h":0},{"c":240,"i":5,"t":1572709632,"d":"1415000863","h":0},{"c":240,"i":79,"t":1572709632,"d":"1237.49731","h":0}],"token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NzI3MDgwNTksImV4cCI6MTU3Mjc5NDQ1OX0.U9T0ayzlqpiNXy2sOjqXLUrAMyozJxHprTZz4ylEV8Q"}

    utils.getJwtToken(obj.token).then(r=>{
//       if(r.exp != 1603445073){return};
//       cl(clientId);
//       cl(obj);
//       cl(r);
//       dbl(r);
      obj.s = r.siteid; // set site id from token
//       cl("mqtt from site: " + obj.s);
//       dbl("mqtt from site: " + obj.s);
      if (!(obj.s in g.siteInitted)){siteInit(obj.s, r.flags)}
      g.siteToUsername[obj.s] = username;
      if(obj.p.length) cl("MQTT from " + username);
      dbl("MQTT from " + username);
      if(obj.p.length) cl(obj);
//       cl(g.siteToUsername);
      obj.command = "data00";
      obj.params = obj.p;
      obj.site = obj.s;
      obj.zone = obj.z;
      obj.token = null;
//       cl(r.flags);
      if((obj.p.length) && (r.flags & utils.SITE_FLAG_LANDRU)){
//         cl(obj);
        g.fixLandruToCloud(obj);
//         cl(obj);
      } 
//       g.checkSetpoint(obj);
//       cl(obj);
      g.chanMQTT.publish('CurrentHistory', '', Buffer.from(JSON.stringify(obj)));
      dbl(obj);
//       dbl(g.ws);
      if (obj.s in g.ws){
  //       cl(g.ws[obj.s]);
        obj.socketId = g.ws[obj.s].socketId;
        dbl(obj.socketId);
        g.chanMQTT.publish('ws', g.ws[obj.s].queue, Buffer.from(JSON.stringify(obj)));

      }
  //     g.chanPodSiteStream.publish('ws',

    });
  },
  
  testBob: ()=>{
    let obj={msg: "this is the one"};
    g.chanMQTT.publish('bob', '', Buffer.from(JSON.stringify(obj)));
    cl("sent bob");
    
    
  },
  
  bobClient: null,
  bobReceived: "",
  
  msgBob: (msg)=>{// this handles the messages coming from the debug interface
    obj = JSON.parse(msg.content);
//     cl(obj.mqttMsg);
    
    let topic = "d0/j/" + obj.clientId;
    g.bobClient = obj.clientId;
//     cl(topic);
//     let mqttMsg = JSON.stringify(obj.mqttMsg);
    //   cl(topic);
    cl([topic, obj.mqttMsg]);
    g.mqttClient.publish(topic, obj.mqttMsg);
    
  },
  
  chanBobMQTT: null,

  init: ()=>{
//     cl("mqtt");
//     cl(this);
//     cl(g.url)
//     cl(g.topic)
//     setInterval(g.testBob, 10000);
    dbl("starting up");
//     setTimeout(f=>{
// //       cl("doing siteinit");
// //       siteInit("SdWgD0C2ncIv5e6u");
//       
//     }, 1000);
    
//     l2c.test00();

    utils.openMQTTConsumer(g.url, g.topic, g.mqttConsume).then((client)=>{
      g.mqttClient = client;
      cl("mqtt connected to " + g.url + ", subscribed: " + g.topic);
    });
//     cl("trying " + utils.amqpUrl);
    utils.openAMQP(utils.amqpUrl).then(conn=>{
      cl("mqtt connected to AMQP at " + utils.amqpUrl);
      utils.openAMQPChannel00(conn, "mqtt", 'topic', msgMQTT).then((ret)=>{
        cl("mqtt listening on " + ret.queue);
        g.chanMQTT = ret.chan;
        g.queueMQTT = ret.queue;
      });
      utils.openAMQPChannel00(conn, "mqttBob", 'fanout', g.msgBob).then((ret)=>{
//         cl("mqtt listening on " + ret.queue);
        g.chanBobMQTT = ret.chan;
//         g.queueMQTT = ret.queue;
      });
    })

  }


};

g.init();

module.exports = g;
