var utils = require('./utils')

/*this will handle both service discovery and new sites
 when a new site appears, it will publish its license, username, and password to the NewSite topic
{"shortName": "(short site id)", "license": "some", "username": "sample", "password": "data"}. at this time, this is just stored
the server responds with a valid short name: {"shortName": (permanent site id)}, which becomes a part of the site's permanent id
then, the site will contact with the "ServiceDiscovery" request, with its short id, and will receive the url
to use for mqtt.
The "shortName" is in fact the first 8 bytes of the full 32 byte siteid*/

var mqttNewSite=(topic, msg)=>{
  cl(msg);
  
}

var mqttServiceDiscovery=(topic, msg)=>{
  cl(msg);
  
}

var g = {
  url: utils.mqttUrl, // 'mqtt://dev.l4ol.com',
  nsTopic: "NewSite",
  sdTopic: "ServiceDiscovery",
  
  
  init: ()=>{
//     utils.openMQTTConsumer(g.url, g.nsTopic, mqttNewSite).then((client)=>{
//       g.mqttClient = client;
//     });
//     utils.openMQTTConsumer(g.url, g.sdTopic, mqttServiceDiscovery).then((client)=>{
//       g.mqttClient = client;
//     });
    utils.openMongoDB('serviceDiscovery').then(db=>{
      g.siteCollection = utils.openMongoColl(db, 'sites');
    });
    
    
  }
}



module.exports = g;
