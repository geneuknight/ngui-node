var utils = require('./utils')
var dbl = (m)=>{
  let pos = 1 + __filename.lastIndexOf('/');
  let ln = utils.callStack()[1].getLineNumber()
  if (typeof m === "object"){ m = JSON.stringify(m); }
  utils.dbl(__filename.substr(pos) + " " + ln + ": " + m);
}

var cl = (m)=>{
  let pos = 1 + __filename.lastIndexOf('/');
  let ln = utils.callStack()[1].getLineNumber()
  if (typeof m === "object"){ m = JSON.stringify(m); }
  console.log(__filename.substr(pos) + " " + ln + ": " + m);
}

var makeRandomString

var getNewSiteId=()=>{
  // would like to guarantee here that the siteId is unique, and keep searching until we find one
// for now, just assuming that it's unique
  return new Promise((res, rej)=>{
    utils.createRandomString(16).then(r=>{
      res(r)
    });
  });
}

var newSiteAck=(clientId, msg)=>{
  obj = {
    msg: msg,
  };
  let msgStr = JSON.stringify(obj);
  let topic = "newsiteAck/" + clientId;
//   cl(msgStr);
//   cl(topic);
  g.mqttClient.publish(topic, msgStr);
}

var newSite2=(clientId, msg)=>{
// { "_id" : ObjectId("5daefb9bf022be0401f618e8"), "username" : "iv-0hHh2", "password" : "UQmH4qAE", "address" : 0, "siteid" : 0 }

  let o = JSON.parse(msg)
//   cl(o)
  let username = o.uid.substr(0, 32);
  let password = o.uid.substr(32, 32);
//   cl(clientId);
//   cl(username);
//   cl(password);
      cl("new registration");
  getNewSiteId().then(r=>{
    let siteId = r;
//         siteId = "mgdL6pwcijJjMhJK";
    let ins = {
      serial: o.serial,
      username: username,
      password: password,
      siteid: siteId,
      created: utils.getTime(),
    }
//         cl(ins)
    g.sites.insertOne(ins);// created new site
//     g.users.findOne({siteid: siteId}).then(r=>{
//       if (r=== null){
//         g.users.findOne({siteid: 0}).then(r=>{// getting the MQTT user/pass
//           cl(r);
//           g.users.updateOne({"_id": utils.makeObjectID(r._id)}, {$set: {siteid: siteId}}, {upsert: false}, (e, r)=>{// {"_id": new ObjectId(r._id)}
//             cl(e)
//             cl(r)
//             newSiteAck(clientId, "siteRegistered");
//           });
//         })
//       }else{
//         newSiteAck(clientId, "siteIdError");
//       }
//     });
  })
}

// var loginSite

var loginAck=(clientId, msg)=>{
//   obj = {
//     msg: msg,
//   };
  let msgStr = JSON.stringify(msg); // {msg: msg}
  let topic = "loginAck/" + clientId;
  // cl(topic)
  cl(msgStr);
  g.mqttClient.publish(topic, msgStr);
}

var makeLogAck=(user, pass, addr, site, flags)=>{
  // cl(site);
  return {
    username: user,
    password: pass,
    address: addr, // utils.mqttUrl, // "dev.l4ol.com",
    token: makeToken(site, flags),
  };
}
var newLogin=(clientId, siteId)=>{// add to users table
  cl("new login");
  g.users.findOne({siteid: 0}).then(ur=>{// find unused
    g.users.updateOne({"_id": utils.makeObjectID(ur._id)}, {$set: {siteid: siteId}}, {upsert: false}, (e, r2)=>{// add siteId
      loginAck(clientId, makeLogAck(ur.username, ur.password, siteId));
    });
  })
}

var makeToken=(siteId, flags)=>{
  // cl(siteId);
  let payload = {siteid: siteId, flags: flags};
  cl(payload);
  return utils.makeJwtToken(payload, 86400);
}

var doLogin=(clientId, siteId, flags)=>{// got site, find mqtt user
  g.users.findOne({siteid: siteId}).then(r=>{// look for existing mqtt user for this site
//     cl(r);
    if(r===null){
      newLogin(clientId, siteId);// we don't have the mqtt user asigned, yet
    }else{
      // cl("existing login");
      let mqttAdd = (flags & utils.SITE_FLAG_USE_STGGRN) ? "mqtt://dev.c2.link4cloud.com" : "mqtt://dev.main.li4.pw"
      loginAck(clientId, makeLogAck(r.username, r.password, mqttAdd, r.siteid, flags));
      cl("loginAck: " + r.username + " / " + r.password + ", to " + mqttAdd);
    }
  });
  // cl(siteId);
}

var login=(clientId, msg)=>{
/* this is still using a fixed address for the mqtt servers. -
 I think the mqttusers table has two copies of all the user names
 */
  // cl("login, clientid: " + clientId);
  var o;
  try{
	cl(msg)
	o = JSON.parse(msg) 
	cl(o);
  }catch(e){
	  cl("JSON Error: " + e.message);
	  return;
  }
  let username = o.uid.substr(0, 32);
  let password = o.uid.substr(32, 32);
//   cl(o);// uid, serial. sites: serial, username, password, siteid, created
  g.sites.findOne({username: username}).then(r=>{
    if (r === null){// first time for site
      newSite2(clientId, msg);
      dbl("new site");
//       cl("new site");
    }else{ // got site
      if(password === r.password){ // valid user
        cl(r);
        doLogin(clientId, r.siteid, r.flags);
//         loginAck(clientId, "ok");
      }else{
        loginAck(clientId, "badUsernamePassword")
      }
//       cl(r);


    }
  })
}

var mqttConsume=(topic, message, packet)=>{
  cl("mqtt consume");
  let cmds = {"newsite": newSite, "login": login}
//   cl("topic: " + topic + " msg: " + message);
  let topicParts = topic.split('/');
  cmds[topicParts[0]](topicParts[1], message);
}

var mqttLoginConsume=(topic, message, packet)=>{
  cl("login consume")
  let topicParts = topic.split('/');
  login(topicParts[1], message);
//   dbl(message);
//   let cmds = {"newsite": newSite, "login": login}
// //   cl("topic: " + topic + " msg: " + message);
//   let topicParts = topic.split('/');
//   cmds[topicParts[0]](topicParts[1], message);
}

var testMqttPublish=()=>{
  cl("test pub");
  g.mqttClient.publish('newsite/and', "hello");
}

g = {
  mqttClient: null,
  init: ()=>{
    cl("init newsite");
    utils.openMongoDB('authorize', utils.siteAuthorizeUrl).then(db=>{
      // cl("open authorize")
      g.db = db;
      g.users = utils.openMongoColl(db, 'mqttusers');
      g.sites = utils.openMongoColl(db, 'sites');
      // cl(g.sites)
      cl("mongo open for authorize: mqttusers, sites on " + utils.siteAuthorizeUrl)
    });

//     utils.openMQTTConsumer(utils.mqttUrl, "newsite/#", mqttConsume).then((client)=>{
//       g.mqttClient = client;
//     });
    utils.openMQTTConsumer(utils.mqttUrl, "login/#", mqttLoginConsume).then((client)=>{
      g.mqttClient = client;
      cl("siteAuthorize connected to " + utils.mqttUrl + ", subscribed: login/#")
      dbl("siteAuthorize connected to " + utils.mqttUrl + ", subscribed: login/#")
      
    });
//     setInterval(testMqttPublish, 1000);

  },
}

g.init();

module.exports = g
