var utils = require('./utils')
var cl = (m)=>{
  let pos = 1 + __filename.lastIndexOf('/');
  let ln = utils.callStack()[1].getLineNumber()
  console.log(__filename.substr(pos) + " " + ln + ": " + m);
}

var WebSocket = require('ws');

/****************** Test WebSocket *********************/

var wsOpen = e=>{
  cl("wsOpen");
  g.ws.send("here");
  g.ws.close();
//   cl(e);
}

var wsClose = e=>{
  cl("wsClose");
//   cl(e);
}

var wsMessage = e=>{
  cl("wsMessage");
//   cl(e);
}

var wsError = e=>{
  cl("wsError");
//   cl(e);
}

var openWS = (uri)=>{
  let ws = new WebSocket(uri);
  ws.onopen = wsOpen;
  ws.onclose = wsClose;
  ws.onmessage = wsMessage;
  ws.onerror = wsError;
  g.ws = ws;
  return ws;
}


/****************** Basic Test Code *********************/

var showMsg = (req, res)=>{
  cl(req.params)
  res.status(200).send("ok");
  utils.httpLog("this[]is/ a bit\ of a test");
//   openWS("ws://l4ol.com:3378");
//   sendToMQTT(req.params.msg);
}

var sendFile = (req, res)=>{
  try{
    res.sendFile(__dirname + '/' + req.params.file); 
  } catch {cl("err")}
}

var setRoutes = (app)=>{
  app.get('/', function (req, res) {
      res.sendFile(__dirname + '/index.html'); 
  })
  app.get('/msg/:msg', showMsg);
  app.get('/:file', sendFile);
}


var g = {
  app: null,
  
  onListen: ()=>{
//     utils.httpLog('Example app listening on port 3002!')
  },
  
  init: ()=>{
//     cl("init test");
    utils.openExpressServer(3002, g.onListen).then(r=>{
      g.app = r;
      setRoutes(r);
    });
  }
}

g.init();

module.exports = g;