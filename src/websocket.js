var utils = require('./utils')
var cl = (m)=>{
  let pos = 1 + __filename.lastIndexOf('/');
  let ln = utils.callStack()[1].getLineNumber()
  if (typeof m === "object"){ m = JSON.stringify(m); }
  console.log(__filename.substr(pos) + " " + ln + ": " + m);
}

var dbl = (m)=>{
  let pos = 1 + __filename.lastIndexOf('/');
  let ln = utils.callStack()[1].getLineNumber()
  if (typeof m === "object"){ m = JSON.stringify(m); }
  utils.dbl(__filename.substr(pos) + " " + ln + ": " + m);
}

/*
current error:
events.js:174
      throw er; // Unhandled 'error' event
      ^

Error: read ECONNRESET
    at TCP.onStreamRead (internal/stream_base_commons.js:111:27)
Emitted 'error' event at:
    at WebSocket.finalize (/opt/l4c2/re/no00/current/node_modules/ws/lib/WebSocket.js:182:41)
    at Socket.emit (events.js:198:13)
    at emitErrorNT (internal/streams/destroy.js:91:8)
    at emitErrorAndCloseNT (internal/streams/destroy.js:59:3)
    at process._tickCallback (internal/process/next_tick.js:63:19)
[nodemon] app crashed - waiting for file changes before starting...
*/

var globWs = null;

// var getMQTTQueue = (site)=>{
//   let cmd = {
//     command: "getSiteMQTTQueue",
//     site: site
//
//   }
//   g.chanCurrent.publish("manager", '', Buffer.from(JSON.stringify(cmd)));
//
// }

var makePrivsDict=(privs)=>{
  ret = {};
  privs.forEach(p=>{
    ret[p.s] = p.p;
  });
  return ret;
}


var login00 = (o, ws)=>{
/* what does login mean?
it means that we will send the site stream for this site here, so this needs
to go into a table accessed by site
we need to get the queue of the MQTT server for this site
token: {"id":"DDmcGqE1wqKkp5r9OpP3soQKTMVxn2HP","privs":[{"s":0,"p":1}
,{"s":1,"p":1}],"iat":1570899046,"exp":1570985446}

*/
//   cl("login00");
//   cl(o);
  o.command = "wsinit00";
  o.socketId = ws.socketId;
  o.queue = g.queueWS;
//   cl("ws connect, queue: " + g.queueWS);
  g.chanCurrent.publish("manager", '', Buffer.from(JSON.stringify(o)));
  dbl("send wsinit00 to manager");
  utils.getJwtToken(o.token).then(r=>{
//     cl(r);
    g.sites[o.site] = {ws: ws, };
    ws.userId = r.id;
    ws.privs = makePrivsDict(r.privs);
//     cl(ws.privs);
    let ret = {command: o.command, key: o.key, result: "ok"};
    try{
      ws.send(JSON.stringify(ret))
    }catch(e){cl(e)}
    let queueReq = {
      command: "getmqttqueue00",
      site: o.site,
      returnQueue: g.queueWS,
    };
//     cl(queueReq);
    dbl("getmqttqueue00");
    g.chanCurrent.publish("manager", '', Buffer.from(JSON.stringify(queueReq)));
//     cl(r);
  }, e=>{"jwt expired error"});
//   cl(o);
}

var getcursite00 = (o, ws)=>{
/* one of the hard parts. This has to send the message off to Current, *with* the key. Then, when the message comes back,
 use the key in the response
 { command: 'getcursite00',
  token:
   'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IkREbWNHcUUxd3FLa3A1cjlPcFAzc29RS1RNVnhuMkhQIiwicHJpdnMiOnsicyI6MH0sImlhdCI6MTU3MDAyMzA4OSwiZXhwIjoxNTcwMTA5NDg5fQ.Ic_0DjmKEK6aC3xSoshzISiCEb3zx9CrkxnBw7cZG88',
  site: 0,
  key: 1 }
*/
//   cl("getcursite00");
//   cl(o);
//   cl(ws.privs[o.site]);
//   cl(ws.privs);
//   cl(o.site);
  let priv = ws.privs[o.site];
//   cl(ws.privs);
//   cl(priv)
//   cl(o.site);
  if ((priv === 0) || (priv === 1)){
//     cl("authorized");
  } else {
//     cl("not");
    try{
      ws.send(JSON.stringify({"result": "not authorized", "key": o.key}))
    }catch(e){cl(e)}
    return;
  }
//   console.log(o);
//   globWs = ws;
  cl(utils.start("ws get current"));
  utils.getJwtToken(o.token).then(r=>{
//     cl(o);
    o.returnQueue = g.queueWS;
    o.socketId = ws.socketId;
    g.chanCurrent.publish(g.currentExchange, '', Buffer.from(JSON.stringify(o)));
//     let ret = {command: o.command, key: o.key, result: "ok"};
//     ws.send(JSON.stringify(ret))
  });
}

var data01 = (o, ws)=>{
// this needs to be sent to Current, History, and MQTT
//   cl(o);
//   dbl(o);
//   g.testObject=o;
  let buf = Buffer.from(JSON.stringify(o)) ;
  g.chanCurrent.publish("CurrentHistory", '', buf);
//   cl("sending to queue: " + g.siteToQueue[o.site]);
  g.chanCurrent.publish("mqtt", g.siteToQueue[o.site], buf);
//   dbl(g.siteToQueue[o.site]);
//   dbl("published to mqtt");
//   g.siteToQueue [o.site] = o.queue;

//   console.log(o);

}

var getPrivs=(obj, token)=>{
  for (let i = 0 ; i < token.privs.length ; i++){
    let p = token.privs[i];
    if(p.s === obj.site){
      return p.p;
    }
  }
  return 0;
}

// var checkJwt=(o)=>{
//   utils.getJwtToken(o.token).then(r=>{
//     let p = getPrivs(o, r);
//   });
//   return 0;
// }

var expiredToken=(o, ws)=>{
  let ret = {command: "expiredToken", key: o.key, result: "error"};
  try{
    // cl(ret);
    // cl(JSON.stringify(ret))
    ws.send(JSON.stringify(ret))
  }catch(e){cl(e.message)}

}

var onMessage = (m, ws)=>{// from WebSocket
  cl("msg");
  // cl(m);
  let cmds = {getcursite00: getcursite00, data01: data01}
  let o = JSON.parse(m)
  g.testObject = o;
  g.testWs = ws;
//   cl(o);
  if(o.command == "login00") return login00(o, ws);
  utils.getJwtToken(o.token).then(r=>{
//       cl(r);
      if (getPrivs(o, r) !== 0){
        cmds[o.command](o, ws);
      }
    }, e=>{
      cl("expired token");
      expiredToken(o, ws)
    }
  );

}

var onClose = (e, ws)=>{
  cl("on Close of socket: " + ws.socketId);
//   cl(g.sockets);
//   cl(e);
//   cl(ws.socketId);
  delete g.sockets[ws.socketId];
}

var onError = (e, ws)=>{
  cl("error");
  cl(e);
}
// var onOpen = ws=>{
//   cl("onOpen");
//   cl(ws)
// }

var onConnect = (ws)=>{// when a new client connects
//     cl("connect");
    ws.on('message', (r)=>onMessage(r, ws));
    ws.on('close', (r)=>onClose(r, ws));// should disconnect with Manager
    ws.on('error', (r)=>onError(r, ws));// should disconnect with Manager
//     ws.lcid = "mine";
    g.sockets[g.socketId] = ws;
    ws.socketId = g.socketId;
    g.socketId += 1;
//     cl(ws);
}

var gotCurSite = (o)=>{
//   cl("received cur site");
  let id = o.socketId;
  delete o.socketId;
  try{
    g.sockets[id].send(JSON.stringify(o));
  }catch(e){cl(e)}

  cl(utils.stop("ws get current"));
//   console.log(o);
//   cl(o.cursite.length);
//   cl(o.packs.length);

}

var sendData = (o)=>{
//   cl("data00 to websocket");
//   cl(o.toString());
//   cl("socket id: " + o.socketId);
//   console.log(o);
  try{
    g.sockets[o.socketId].send(JSON.stringify(o));
  }catch(e){
    cl("trying to send on sockeId: " + o.socketId.toString())
    cl("exception on sending data " + e.toString())
  }

//   globWs.send(JSON.stringify(o));

}

var gotmqttqueue00 = (o)=>{
  dbl("gotmqttqueue00: " + o.queue);
  g.siteToQueue [o.site] = o.queue;
//   cl(g.siteToQueue)
}

var msgWS = (msg)=>{// coming back from current, probably
//   cl("WS message");
  cmds = {gotcursite00: gotCurSite, data00: sendData, gotmqttqueue00: gotmqttqueue00}
  let obj = JSON.parse(msg.content);
//   console.log(obj);
//   cl(obj.command);
  try{
    cmds[obj.command](obj);
  } catch{
    cl("missing command in websocket: " + obj.command);
  }
//   cl(obj.command);
//   if (obj.cursite !== undefined){
//     let cnt = obj.cursite.length;
// //     cl("returned packs: " + cnt);
//   }
//   cl("returned done");
//   cl(JSON.parse(msg.content));
}

var g = {
  websock: null,
  socketId: 0,
  sites: {},
  sockets: {},
  currentExchange: "Current",
//   currentReturnExchange: "CurrentReturn",
  queueCurrentReturn: null,
  siteToQueue: {},


  chanWS: null,
  queueWS: null,
  
  testObject: null,
  
/*{"command":"data01","site":"BiL4o4pAcbaW9FmN","user":0,"token":"eyJhbGciOiJIU","params":[
{"z":0,"c":0,"i":782,"t":1602699210,"d":1},
{"z":0,"c":0,"i":784,"t":1602699210,"d":1},
{"z":0,"c":0,"i":786,"t":1602699210,"d":1}],
"key":2}
*/

/* request params for cyclic irrigation
i: 755, 756, 650, 651, 716
cloud 755 = 
CD_start_irr: 155,
i":755,"d":"0"}
l2c2l.js 1635: {"z":0,"c":0,"i":755,"d":"0","cmd":1,"ch":0,"ix":0,"id":206}
mqtt.js 116: 
{"v":257,"s":"SdWgD0C2ncIv5e6u","z":0,"p":[{"id":206,"z":0,"ch":0,"ix":0,"t":102,"d":"0"}],"cmd":2}
mqtt.js 144: d0/j/landru1_Bobs: {"v":257,"

p.PID_BASE_CONF_CHANNELS = 500;
pi.channels_configuration["channelName"] = 7
pi.channels_configuration["channelType"] = 8

l2c2l.js 1585: {"z":0,"c":0,"i":507,"t":1603375610,"d":"abcdef"}
mqtt.js 116: {"v":257,"s":"SdWgD0C2ncIv5e6u","z":0,"p":[{"id":144,"z":0,"ch":0,"ix":0,"t":102,"d":"abcdef"}],"cmd":1}

bob's expire time
1603445073


 
 */

  sendOurTestPacket:()=>{
    let p0={z: 0, c: 0, i: 507, d: "0"};
    let p1={z: 0, c: 0, i: 755, d: "0"};
    let p2={z: 0, c: 0, i: 756, d: "0"};
    let p3={z: 0, c: 0, i: 650, d: "0"};
    let p4={z: 0, c: 0, i: 651, d: "0"};
    let p5={z: 0, c: 0, i: 716, d: "0"};
    let p6={z: 0, c: 0, i: 712, d: "0"};
    
//     let p1={z: 0, c: 0, i: 755, d: "0"};
//     let p1={z: 0, c: 0, i: 755, d: "0"};
//     let p1={z: 0, c: 0, i: 755, d: "0"};
//     let p1={z: 0, c: 0, i: 755, d: "0"};
//     let p1={z: 0, c: 0, i: 755, d: "0"};
    let packet = {
      command: "demo_get_config", 
      site: "SdWgD0C2ncIv5e6u",
      params: [p0, p1, p2, p3, p4, p5, p6]
    };
    data01(packet, g.testWs);
    packet.site = "utd5xp3vvdoSsbg0";
    data01(packet, g.testWs);
  },
  
  
  
  sendTestPacket: ()=>{
    cl("send test");
    let p0=[{id: 203, ix: 0, ch: 0, d: "540", z: 0, t: 101}];
    let p1=    [
    {"id":37 },
    {"id":25,  "ix":0 },
    {"id":25,  "ix":1 }
    ];
    if(g.testObject){
      g.testObject.command="data02";
      g.testObject.cmd=4;// read status
      g.testObject.params=[];
      g.testObject.p=p1;
      data01(g.testObject, g.testWs);
      
      
      
      dbl(g.testObject);
      
      
    }
  },


  init: ()=>{
//     setInterval(g.sendOurTestPacket, 30000);
//     cl("init websocket");
    cl("websocket exposed socket at " + utils.wsUrl + " on port " + utils.wsPort.toString());
//     cl(utils.wsPort);
    utils.openWebSocket(utils.wsPort, onConnect).then(ws=>{
      g.websock = ws;
      cl("websocket connection accepted");
//       cl(ws)
    });
    utils.openAMQP(utils.amqpUrl).then(conn=>{
      utils.openAMQPChannel00(conn, g.currentExchange, 'fanout', null).then((r)=>{
        g.chanCurrent = r.chan;
        cl("ws connected to AMQP at " + utils.amqpUrl + " on " + g.currentExchange);
      });
//       cl("get queue");
      utils.openAMQPChannel00(conn, "ws", 'topic', msgWS).then((ret)=>{
        g.chanWS = ret.chan;
        g.queueWS = ret.queue;
        cl("ws connected to AMQP, listening on " + g.queueWS);
//         cl("ws queue: " + g.queueWS);
      });
    })

  }
}

g.init();

module.exports = g;
